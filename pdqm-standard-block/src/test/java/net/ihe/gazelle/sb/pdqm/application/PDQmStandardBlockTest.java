package net.ihe.gazelle.sb.pdqm.application;

import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.sb.api.business.DecodingException;
import net.ihe.gazelle.sb.fhir.business.FhirResources;
import net.ihe.gazelle.sb.fhir.business.FhirSearchParameters;
import org.hl7.fhir.r4.model.Enumerations;
import org.hl7.fhir.r4.model.SearchParameter;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * test for pdqm standard block
 */
class PDQmStandardBlockTest {

    private PDQmStandardBlock block = new PDQmStandardBlock();

    /**
     * test decoding
     * @throws DecodingException if error during decoding
     */
    @Test
    @Covers(requirements = {"ITI78-011", "ITI78-013", "SB-3"})
    void decodeValidParameter() throws DecodingException {
        FhirSearchParameters parameters= new FhirSearchParameters();
        parameters.addSearchParameter(org.hl7.fhir.instance.model.api.IAnyResource.SP_RES_ID, Enumerations.SearchParamType.STRING, SearchParameter.SearchModifierCode.EXACT, "");

        FhirSearchParameters decodedParameters= block.decode(parameters);

       assertEquals(parameters, decodedParameters);
    }

    /**
     * test decoding with no parameters. Should throw error
     */
    @Test
    void decodeNoParameter() {
        FhirSearchParameters parameters= new FhirSearchParameters();

        assertThrows(DecodingException.class, () -> block.decode(parameters));
    }

    /**
     * test decoding with invalid parameters. Should throw error
     */
    @Test
    @Covers(requirements = {"ITI78-011","SB-3"})
    void decodeInvalidParameter() {
        FhirSearchParameters parameters= new FhirSearchParameters();
        parameters.addSearchParameter("invalid", Enumerations.SearchParamType.URI, SearchParameter.SearchModifierCode.EXACT, "");

        assertThrows(DecodingException.class, () -> block.decode(parameters));
    }

    /**
     * test encoding
     */
    @Test
    @Covers(requirements ="SB-4")
    void encode() {
        FhirResources resources = new FhirResources();

        FhirResources encodedResources = block.encode(resources);

        assertEquals(resources, encodedResources);
    }
}