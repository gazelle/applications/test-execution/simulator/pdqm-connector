Feature: Retrieve Patient information

  Background:
    Given the following patients exist
      | tmpuuid  | gender | active | dateOfBirth                  | dateOfDeath                  | multipleBirthOrder |
      | tmpuuid0 | MALE   | true   | 1984-03-12T17:25:04.017+0100 | 2020-03-12T17:25:04.017+0100 | 134                |
      | tmpuuid1 | FEMALE | true   | 1984-03-12T17:25:04.017+0100 | 2020-03-12T17:25:04.017+0100 | 134                |
    And patient is fed with provisional uuid "tmpuuid0"
    And patient is fed with provisional uuid "tmpuuid1"

  Scenario: Nominal Patient retrieve
    When retrieve is done on uuid "tmpuuid0"
    Then response is "OK"
    And retrieved patient id is "tmpuuid0"
