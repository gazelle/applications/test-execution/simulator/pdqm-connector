Feature: Translate FHIR request parameters to Gazelle SearchCriteria

  Scenario: Request with exact id
    Given search criteria "uuid" "is" "123"
    When search is done
    Then forwarded parameter contains "uuid" "is" "123"

  Scenario: Patient registry returns patient with uuid but not mappable on FHIR
    Given the following patients exist
      | tmpuuid  | gender | active | dateOfBirth                  | dateOfDeath                  | multipleBirthOrder |
      | tmpuuid0 | MALE   | true   | 1984-03-12T17:25:04.017+0100 | 2020-03-12T17:25:04.017+0100 | 134                |
    And patients have the following identifiers
      | tmpuuid  | systemIdentifier | value       | systemName | type |
      | tmpuuid0 | urn:oid:1.2.3.4  | identifier0 | system1    | PI   |
      | tmpuuid0 | urn:oid:5.6.7.8  | identifier0 | system2    | PI   |
    And patients have the following names
      | tmpuuid  | family | given1 | given2 | given3 |
      | tmpuuid0 | Bars   | Alain  | Hilary | Didier |
    And patients have the following addresses
      | tmpuuid  | line1                | city               | countryIso3 | postalCode | state | use   |
      | tmpuuid0 | 4 Rue Hélène-Boucher | Thorigné-Fouillard | FRA         | 35325      | test  | HOME  |
      | tmpuuid0 | 1 Rue des vignes     | Thorigné-Fouillard | FRA         | 35325      | etat  | LEGAL |
    And patients have the following contact points
      | tmpuuid  | use   | value           |
      | tmpuuid0 | HOME  | 0167200001      |
      | tmpuuid0 | OTHER | ftg@kereval.com |
    And patients have the following observations
      | tmpuuid  | type        | value |
      | tmpuuid0 | BLOOD_GROUP | O+    |
      | tmpuuid0 | WEIGHT      | 62    |
    And search criteria "uuid" "is" "tmpuuid0"
    When search is done
    Then response is "OK"
    And received 0 patients

  Scenario: Patient registry returns patient with uuid
    Given the following patients exist
      | tmpuuid  | gender | active | dateOfBirth                  | dateOfDeath                  | multipleBirthOrder |
      | tmpuuid0 | MALE   | true   | 1984-03-12T17:25:04.017+0100 | 2020-03-12T17:25:04.017+0100 | 134                |
    And patients have the following identifiers
      | tmpuuid  | systemIdentifier | value       | systemName | type |
      | tmpuuid0 | urn:oid:1.2.3.4  | identifier0 | system1    | PI   |
      | tmpuuid0 | urn:oid:5.6.7.8  | identifier0 | system2    | PI   |
    And patients have the following names
      | tmpuuid  | family | given1 | given2 | given3 |
      | tmpuuid0 | Bars   | Alain  | Hilary | Didier |
    And patients have the following addresses
      | tmpuuid  | line1                | city               | countryIso3 | postalCode | state | use  |
      | tmpuuid0 | 4 Rue Hélène-Boucher | Thorigné-Fouillard | FRA         | 35325      | test  | HOME |
      | tmpuuid0 | 1 Rue des vignes     | Thorigné-Fouillard | FRA         | 35325      | etat  | BAD  |
    And patients have the following contact points
      | tmpuuid  | use  | value           |
      | tmpuuid0 | HOME | 0167200001      |
      | tmpuuid0 | WORK | ftg@kereval.com |
    And patients have the following observations
      | tmpuuid  | type        | value |
      | tmpuuid0 | BLOOD_GROUP | O+    |
      | tmpuuid0 | WEIGHT      | 62    |
    And search criteria "uuid" "is" "tmpuuid0"
    When search is done
    Then response is "OK"
    And received 1 patients
    And received patient is received with uuid "tmpuuid0"