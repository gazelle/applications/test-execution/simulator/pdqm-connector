Feature: Search Patient information in a single domain

  Scenario: Request with exact id
    Given search criteria "uuid" "is" "123"
    When search is done
    Then forwarded parameter contains "uuid" "is" "123"

  Scenario: Request with startsWith LastName
    Given search criteria "LastName" "startsWith" "Ba"
    When search is done
    Then forwarded parameter contains "LastName" "startsWith" "Ba*"

  Scenario: Request with contains LastName
    Given search criteria "LastName" "contains" "ar"
    When search is done
    Then forwarded parameter contains "LastName" "contains" "*ar*"

  Scenario: Request with exact LastName
    Given search criteria "LastName" "is" "Bars"
    When search is done
    Then forwarded parameter contains "LastName" "is" "Bars"

  Scenario: Request with contains FirstName
    Given search criteria "FirstName" "contains" "ai"
    When search is done
    Then forwarded parameter contains "FirstName" "contains" "*ai*"

  Scenario: Request with exact FirstName
    Given search criteria "FirstName" "is" "Alain"
    When search is done
    Then forwarded parameter contains "FirstName" "is" "Alain"

  Scenario: Request with exact Gender
    Given search criteria "Gender" "is" "female"
    When search is done
    Then forwarded parameter contains "Gender" "is" "female"

  Scenario: Request with exact Identifier
    Given search criteria "Identifier" "is" "urn:oid:1.2.3.4|identifier0"
    When search is done
    Then forwarded parameter contains "Identifier" "is" "urn:oid:1.2.3.4|identifier0"

  Scenario: Request with exact Identifier as Domain restriction
    Given search criteria "Identifier" "is" "urn:oid:1.2.3.4|"
    And search criteria "FirstName" "is" "Alain"
    When search is done
    Then forwarded parameter contains "Identifier" "is" "urn:oid:1.2.3.4|"

  Scenario: Request with multiple Identifier as Domain restriction
    Given search criteria "Identifier" "is" "urn:oid:1.2.3.4|,urn:oid:9.8.7.6|"
    And search criteria "FirstName" "is" "Alain"
    When search is done
    Then forwarded parameter contains "Identifier" "is" "urn:oid:1.2.3.4|"
    And forwarded parameter contains "Identifier" "is" "urn:oid:9.8.7.6|"

  Scenario: Request with exact Identifier without domain nor pipe
    Given search criteria "Identifier" "is" "identifier0"
    When search is done
    Then forwarded parameter contains "Identifier" "is" "identifier0"

  Scenario: Request with exact Identifier without domain
    Given search criteria "Identifier" "is" "|identifier0"
    When search is done
    Then forwarded parameter contains "Identifier" "is" "|identifier0"

  Scenario: Request with contains Address
    Given search criteria "Address" "contains" "Rennes"
    When search is done
    Then forwarded parameter contains "Address" "contains" "Rennes"

  Scenario: Request with exact city
    Given search criteria "City" "is" "Thorigné-Fouillard"
    When search is done
    Then forwarded parameter contains "City" "is" "Thorigné-Fouillard"

  Scenario: Request with startWith postalCode
    Given search criteria "PostalCode" "startsWith" "35"
    When search is done
    Then forwarded parameter contains "PostalCode" "startsWith" "35*"

  Scenario: Request with exact Country
    Given search criteria "Country" "is" "GBR"
    When search is done
    Then forwarded parameter contains "Country" "is" "GBR"

  Scenario: Request with exact State
    Given search criteria "State" "is" "England"
    When search is done
    Then forwarded parameter contains "State" "is" "England"

  Scenario: Request with BirthDate
    Given search criteria "BirthDate" "is" "eq2019-01-19"
    When search is done
    Then forwarded parameter contains "BirthDate" "is" "eq2019-01-19"

  Scenario: Request with Telecom
    Given search criteria "Telecom" "is" "alain@testpdqm.com"
    When search is done
    Then forwarded parameter contains "Telecom" "is" "alain@testpdqm.com"

  Scenario: Request with Telecom
    Given search criteria "Telecom" "is" "555444666"
    When search is done
    Then forwarded parameter contains "Telecom" "is" "555444666"

# PR -> PDQm

  Scenario: Patient registry returns patient with uuid
    Given the following patients exist:
      | uuid  |
      | uuid0 |
    And patients have the following identifiers:
      | uuid  | domain          | identifier  |
      | uuid0 | 1.2.3.4 | identifier0 |
    And search criteria "LastName" "startsWith" "Ba"
    And PatientRegistry search returns "Patient List"
    When search is done
    Then received HTTP status code 200
    And received 1 patients
    And all received patients "uuid" "is" "uuid0"

  Scenario: Patient registry returns patient with LastName
    Given the following patients exist:
      | uuid  | LastName|
      | uuid0 | Bars    |
    And patients have the following identifiers:
      | uuid  | domain          | identifier  |
      | uuid0 | 1.2.3.4 | identifier0 |
    And search criteria "LastName" "startsWith" "Ba"
    And PatientRegistry search returns "Patient List"
    When search is done
    Then received HTTP status code 200
    And received 1 patients
    And all received patients "LastName" "is" "Bars"


  Scenario: Patient registry returns patient with FirstName
    Given the following patients exist:
      | uuid  | LastName | FirstName |
      | uuid0 | Bars     | Alain     |
    And patients have the following identifiers:
      | uuid  | domain          | identifier  |
      | uuid0 | 1.2.3.4 | identifier0 |
    And search criteria "LastName" "startsWith" "Ba"
    And PatientRegistry search returns "Patient List"
    When search is done
    Then received HTTP status code 200
    And received 1 patients
    And all received patients "FirstName" "is" "Alain"

  Scenario: Patient registry returns patient with Gender
    Given the following patients exist:
      | uuid  | LastName | FirstName | Gender |
      | uuid0 | Bars     | Alain     | male   |
    And patients have the following identifiers:
      | uuid  | domain          | identifier  |
      | uuid0 | 1.2.3.4 | identifier0 |
    And search criteria "LastName" "startsWith" "Ba"
    And PatientRegistry search returns "Patient List"
    When search is done
    Then received HTTP status code 200
    And received 1 patients
    And all received patients "Gender" "is" "male"

  Scenario: Patient registry returns patient with Identifier
    Given the following patients exist:
      | uuid  | LastName | FirstName | Gender |
      | uuid0 | Bars     | Alain     | male   |
    And patients have the following identifiers:
      | uuid  | domain          | identifier  |
      | uuid0 | 1.2.3.4 | identifier0 |
      | uuid0 | 5.6.7.8 | identifier1 |
    And search criteria "LastName" "startsWith" "Ba"
    And PatientRegistry search returns "Patient List"
    When search is done
    Then received HTTP status code 200
    And received 1 patients
    And all received patients "Identifier" "is" "urn:oid:1.2.3.4|identifier0"
    And all received patients "Identifier" "is" "urn:oid:5.6.7.8|identifier1"

  Scenario: Patient registry returns patient with AddressLine
    Given the following patients exist:
      | uuid  | LastName | FirstName | Gender |
      | uuid0 | Bars     | Alain     | male   |
    And patients have the following identifiers:
      | uuid  | domain          | identifier  |
      | uuid0 | 1.2.3.4 | identifier0 |
    And patients have the following addresses:
      | uuid  | line1                | line2         |
      | uuid0 | 4 Rue Hélène-Boucher | Z.A. Bellevue |
    And search criteria "LastName" "startsWith" "Ba"
    And PatientRegistry search returns "Patient List"
    When search is done
    Then received HTTP status code 200
    And received 1 patients
    And all received patients "Address" "is" "4 Rue Hélène-Boucher"
    And all received patients "Address" "is" "Z.A. Bellevue"

  Scenario: Patient registry returns patient with AddressCity
    Given the following patients exist:
      | uuid  | LastName | FirstName | Gender |
      | uuid0 | Bars     | Alain     | male   |
    And patients have the following identifiers:
      | uuid  | domain          | identifier  |
      | uuid0 | 1.2.3.4 | identifier0 |
    And patients have the following addresses:
      | uuid  | city               |
      | uuid0 | Thorigné-Fouillard |
    And search criteria "LastName" "startsWith" "Ba"
    And PatientRegistry search returns "Patient List"
    When search is done
    Then received HTTP status code 200
    And received 1 patients
    And all received patients "City" "is" "Thorigné-Fouillard"

  Scenario: Patient registry returns patient with AddressCountry
    Given the following patients exist:
      | uuid  | LastName | FirstName | Gender |
      | uuid0 | Bars     | Alain     | male   |
    And patients have the following identifiers:
      | uuid  | domain          | identifier  |
      | uuid0 | 1.2.3.4 | identifier0 |
    And patients have the following addresses:
      | uuid  | countryIso3  |
      | uuid0 | FRA          |
    And search criteria "LastName" "startsWith" "Ba"
    And PatientRegistry search returns "Patient List"
    When search is done
    Then received HTTP status code 200
    And received 1 patients
    And all received patients "Country" "is" "FRA"

  Scenario: Patient registry returns patient with AddressPostalCode
    Given the following patients exist:
      | uuid  | LastName | FirstName | Gender |
      | uuid0 | Bars     | Alain     | male   |
    And patients have the following identifiers:
      | uuid  | domain          | identifier  |
      | uuid0 | 1.2.3.4 | identifier0 |
    And patients have the following addresses:
      | uuid  | postalCode   |
      | uuid0 | 35325        |
    And search criteria "LastName" "startsWith" "Ba"
    And PatientRegistry search returns "Patient List"
    When search is done
    Then received HTTP status code 200
    And received 1 patients
    And all received patients "PostalCode" "is" "35325"

  Scenario: Patient registry returns patient with State
    Given the following patients exist:
      | uuid  | LastName | FirstName | Gender |
      | uuid0 | Bars     | Alain     | male   |
    And patients have the following identifiers:
      | uuid  | domain          | identifier  |
      | uuid0 | 1.2.3.4 | identifier0 |
    And patients have the following addresses:
      | uuid  | state   |
      | uuid0 | England |
    And search criteria "LastName" "startsWith" "Ba"
    And PatientRegistry search returns "Patient List"
    When search is done
    Then received HTTP status code 200
    And received 1 patients
    And all received patients "State" "is" "England"

  Scenario: Patient registry returns patient with Telecom
    Given the following patients exist:
      | uuid  | LastName | FirstName | Gender | BirthDate  |
      | uuid0 | Bars     | Alain     | male   | 2019-01-25 |
    And patients have the following identifiers:
      | uuid  | domain          | identifier  |
      | uuid0 | 1.2.3.4 | identifier0 |
    And patients have the following contact points:
      | uuid  | value     |
      | uuid0 | 555444666 |
    And search criteria "LastName" "startsWith" "Ba"
    And PatientRegistry search returns "Patient List"
    When search is done
    Then received HTTP status code 200
    And received 1 patients
    And all received patients "Telecom" "is" "555444666"
