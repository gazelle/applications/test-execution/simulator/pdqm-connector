Feature: Test PDQm Search Params

  Background: init patient registry mock
    Given PatientRegistry search returns "Patient List"

  Scenario:
    Given search criteria "LastName" "startsWith" "Ba"

    When search is done
    Then received HTTP status code 200

  Scenario: no valid param
    When search is done
    Then received HTTP status code 400

