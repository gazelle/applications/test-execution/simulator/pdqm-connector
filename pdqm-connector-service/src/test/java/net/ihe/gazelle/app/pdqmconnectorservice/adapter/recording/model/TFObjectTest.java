package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link TFObject}
 */
public class TFObjectTest {

    /**
     * Test for getter/Setter keyword
     */
    @Test
    public void testKeyword() {
        String keyword = "keyword";
        TFObject transaction = new Transaction();

        assertNull(transaction.getKeyword());

        transaction.setKeyword(keyword);

        assertEquals(keyword, transaction.getKeyword());

        transaction.setKeyword(null);

        assertNull(transaction.getKeyword());
    }

    /**
     * Test for getter/Setter description
     */
    @Test
    public void testDescription() {
        String description = "description";
        TFObject transaction = new Transaction();

        assertNull(transaction.getDescription());

        transaction.setDescription(description);

        assertEquals(description, transaction.getDescription());

        transaction.setDescription(null);

        assertNull(transaction.getDescription());
    }

    /**
     * Test for getter/Setter name
     */
    @Test
    public void testName() {
        String name = "name";
        TFObject transaction = new Transaction();

        assertNull(transaction.getName());

        transaction.setName(name);

        assertEquals(name, transaction.getName());

        transaction.setName(null);

        assertNull(transaction.getName());
    }

    /**
     * Test for hashCode
     */
    @Test
    public void testHashCode() {
        TFObject tfObject = new TFObject() {
            @Override
            public String toString() {
                return super.toString();
            }
        };
        assertNotNull(tfObject.hashCode());
    }

    /**
     * Tests for equals method
     */
    @Test
    public void testEquals() {
        TFObject domain = new Domain();
        assertNotEquals(domain, null);
        assertEquals(domain, new Domain());
        assertEquals(domain, domain);
        assertNotEquals(12, domain);

        TFObject domain0 = new Domain();
        domain0.setKeyword("Test");
        assertNotEquals(domain, domain0);

        domain.setName("Name");
        assertNotEquals(domain, new Domain());
        assertEquals(domain, domain);

        TFObject domain1 = new Domain();
        domain1.setName("Name");
        domain.setDescription("Description");
        assertNotEquals(domain, domain1);

        domain1.setDescription("Description");
        assertEquals(domain, domain1);
    }
}
