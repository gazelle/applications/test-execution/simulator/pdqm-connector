package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link Actor}
 */
public class ActorTest {

    /**
     * Test for the ID getter
     */
    @Test
    public void testGetId() {
        Actor actor = new Actor();
        assertNull(actor.getId());
    }

    /**
     * Test for the actAsResponder Property
     */
    @Test
    public void testActAsResponder() {
        Actor actor = new Actor();
        assertNull(actor.getCanActAsResponder());

        actor.setCanActAsResponder(true);

        assertTrue(actor.getCanActAsResponder());
    }

    /**
     * Test for hashCode method
     */
    @Test
    public void testHashCode() {
        Actor actor = new Actor();
        assertNotNull(actor.hashCode());
    }

    /**
     * Test for equals method
     */
    @Test
    public void testEquals() {
        Actor actor = new Actor();
        assertEquals(actor, actor);
        assertNotEquals(0, actor);
        assertNotEquals(actor, null);
        Actor actor1 = new Actor();
        assertEquals(actor, actor1);
    }
}
