package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.database;

import net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model.Domain;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DomainDatabaseDAOTest {

   private static final String PERSISTENCE_UNIT_NAME_CONST = "PersistenceUnitTest";

   private static EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST);
   private EntityManager entityManager;
   private DomainDatabaseDAO domainDatabaseDAO;

   @BeforeAll
   public static void setupDatabase() {
      factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST);
   }

   @AfterAll
   public static void closeAndDropDatabase() {
      // Drop database at sessionFactory closure if hbm2ddl.auto is set to "create-drop"
      factory.close();
   }

   @BeforeEach
   public void initializeEntityManager() {
      entityManager = factory.createEntityManager();
      entityManager.getTransaction().begin();
      domainDatabaseDAO = new DomainDatabaseDAO(entityManager);
   }

   @AfterEach
   public void closeEntityManager() {
      entityManager.getTransaction().commit();
      entityManager.close();
   }


   @Test
   public void testDomainCreationAndGet() {
      String keyWord = "CreationKeyword";
      domainDatabaseDAO.createDomain(keyWord);
      Domain domain = domainDatabaseDAO.getDomainByKeyword(keyWord);
      assertNotNull(domain);
      assertEquals(keyWord, domain.getKeyword());
   }

   @Test
   public void testDomainCreateAndIsExisting() {
      String keyWord = "CreationAndExistKeyword";
      domainDatabaseDAO.createDomain(keyWord);
      assertTrue(domainDatabaseDAO.isDomainExisting(keyWord));
   }

   @Test
   public void testDomainIsExistingFalse() {
      String keyWord = "NotExistKeyword";
      assertFalse(domainDatabaseDAO.isDomainExisting(keyWord));
   }


   @Test
   public void testDomainNumber() {
      Long domainNumberPrev = domainDatabaseDAO.getNumberOfRegisteredDomains();
      String keyWord = "NumberCreationKeyword";
      domainDatabaseDAO.createDomain(keyWord);

      Long domainNumberNext = domainDatabaseDAO.getNumberOfRegisteredDomains();
      assertEquals(1, domainNumberNext.compareTo(domainNumberPrev));
   }

}