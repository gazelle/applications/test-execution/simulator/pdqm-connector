package net.ihe.gazelle.app.pdqmconnectorservice.adapter.preferences;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertTrue;

/**
 * Test class for {@link OperationalPreferencesPDQm}
 */
public class OperationalPreferencesPDQmTest {

    /**
     * Test that the Mandatory Preferences for PDQm are well returned by the Operational Preferences Service.
     */
    @Test
    public void testGetMandatoryPreferences(){
        OperationalPreferencesPDQm operationalPreferencesPDQm = new OperationalPreferencesPDQm();

        Map<String, List<String>> mandatoryPreferences = operationalPreferencesPDQm.wantedMandatoryPreferences();

        assertTrue("DEPLOYEMENT namespace shall be present in the Mandatory Preferences map !",
                mandatoryPreferences.containsKey(Namespaces.DEPLOYMENT.getValue()));
        assertEquals("DEPLOYEMENT Namespace shall define a single Preference !",1,
                mandatoryPreferences.get(Namespaces.DEPLOYMENT.getValue()).size());
        assertEquals("Preference defined in DEPLOYEMENT Namespace shall be Patient Registry URL !", Preferences.PATIENT_REGISTRY_URL.getKey(),
                mandatoryPreferences.get(Namespaces.DEPLOYMENT.getValue()).get(0));
    }
}
