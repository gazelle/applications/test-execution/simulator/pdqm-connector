package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.database;

import net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model.Transaction;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TransactionDatabaseDAOTest {

   private static final String PERSISTENCE_UNIT_NAME_CONST = "PersistenceUnitTest";


   private static EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST);
   private EntityManager entityManager;
   TransactionDatabaseDAO transactionDAO;

   @BeforeAll
   public static void setupDatabase() {
      factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST);
   }

   @AfterAll
   public static void closeAndDropDatabase() {
      // Drop database at sessionFactory closure if hbm2ddl.auto is set to "create-drop"
      factory.close();
   }

   @BeforeEach
   public void initializeEntityManager() {
      entityManager = factory.createEntityManager();
      entityManager.getTransaction().begin();
      transactionDAO = new TransactionDatabaseDAO(entityManager);
   }

   @AfterEach
   public void closeEntityManager() {
      entityManager.getTransaction().commit();
      entityManager.close();
   }


   @Test
   public void testTransactionCreationAndGet() {
      String keyWord = "CreationKeyword";
      transactionDAO.createTransaction(keyWord);

      Transaction transaction = transactionDAO.getTransactionByKeyword(keyWord);
      assertNotNull(transaction);
      assertEquals(keyWord, transaction.getKeyword());
   }

   @Test
   public void testTransactionCreationAndIsExisting() {
      String keyWord = "CreationAndExistKeyword";
      transactionDAO.createTransaction(keyWord);

      assertTrue(transactionDAO.isTransactionExisting(keyWord));
   }

   @Test
   public void testTransactionIsExistingFalse() {
      String keyWord = "NotExistsKeyword";
      assertFalse(transactionDAO.isTransactionExisting(keyWord));
   }

   @Test
   public void testTransactionNumber() {
      Long transactionNumberPrev = transactionDAO.getNumberOfRegisteredTransactions();
      String keyWord = "NumberCreationKeyword";
      transactionDAO.createTransaction(keyWord);

      Long transactionNumberNext = transactionDAO.getNumberOfRegisteredTransactions();
      assertEquals(1, transactionNumberNext.compareTo(transactionNumberPrev));
   }

}