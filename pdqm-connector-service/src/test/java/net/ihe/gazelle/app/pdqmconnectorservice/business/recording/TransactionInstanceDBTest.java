package net.ihe.gazelle.app.pdqmconnectorservice.business.recording;

import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class TransactionInstanceDBTest {


    @Test
    public void MessageInstanceMetadataBusinessDefaultConstructorTest() {
        TransactionInstance transactionInstance = new TransactionInstance();
        assertNotNull(transactionInstance);
    }


    @Test
    public void MessageInstanceMetadataBusinessCompleteConstructorTest() {
        Date timestamp = new Date();
        Boolean visible = true;
        String domainKeyword = "domainKeyword";
        String simulatedActorKeyword = "simulatedActorKeyword";
        String transactionKeyword = "transactionKeyword";
        EStandardBusiness standard = EStandardBusiness.FHIR_JSON;
        String companyKeyword = "companyKeyword";
        MessageInstance request = new MessageInstance();
        MessageInstance response = new MessageInstance();

        TransactionInstance transactionInstance = new TransactionInstance(timestamp, visible,
                domainKeyword, simulatedActorKeyword, transactionKeyword, standard, companyKeyword, request, response);

        assertEquals(timestamp, transactionInstance.getTimestamp());
        assertEquals(visible, transactionInstance.getVisible());
        assertEquals(domainKeyword, transactionInstance.getDomainKeyword());
        assertEquals(simulatedActorKeyword, transactionInstance.getSimulatedActorKeyword());
        assertEquals(transactionKeyword, transactionInstance.getTransactionKeyword());
        assertEquals(standard, transactionInstance.getStandard());
        assertEquals(companyKeyword, transactionInstance.getCompanyKeyword());
        assertEquals(request, transactionInstance.getRequest());
        assertEquals(response, transactionInstance.getResponse());
    }

}