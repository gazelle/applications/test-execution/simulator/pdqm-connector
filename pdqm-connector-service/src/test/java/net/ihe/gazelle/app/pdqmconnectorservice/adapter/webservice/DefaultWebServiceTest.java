package net.ihe.gazelle.app.pdqmconnectorservice.adapter.webservice;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests for {@link DefaultWebService}
 */
public class DefaultWebServiceTest {

    /**
     * Test get method
     */
    @Test
    public void testGet() {
        DefaultWebService defaultWebService = new DefaultWebService();
        assertEquals("get", defaultWebService.get());
    }

    /**
     * Test post method
     */
    @Test
    public void testPost() {
        DefaultWebService defaultWebService = new DefaultWebService();
        assertEquals("post", defaultWebService.post());
    }

    /**
     * Test put method
     */
    @Test
    public void testPut() {
        DefaultWebService defaultWebService = new DefaultWebService();
        assertEquals("put", defaultWebService.put());
    }

    /**
     * Test patch method
     */
    @Test
    public void testPatch() {
        DefaultWebService defaultWebService = new DefaultWebService();
        assertEquals("patch", defaultWebService.patch());
    }

    /**
     * Test delete method
     */
    @Test
    public void testDelete() {
        DefaultWebService defaultWebService = new DefaultWebService();
        assertEquals("delete", defaultWebService.delete());
    }

    /**
     * Test head method
     */
    @Test
    public void testHead() {
        DefaultWebService defaultWebService = new DefaultWebService();
        assertEquals("head", defaultWebService.head());
    }
}
