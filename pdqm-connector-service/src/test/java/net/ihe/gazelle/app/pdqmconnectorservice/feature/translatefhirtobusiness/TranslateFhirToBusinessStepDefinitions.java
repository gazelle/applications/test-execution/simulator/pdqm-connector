package net.ihe.gazelle.app.pdqmconnectorservice.feature.translatefhirtobusiness;

import com.github.tomakehurst.wiremock.verification.LoggedRequest;
import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.BeforeStep;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.ihe.gazelle.app.patientregistryapi.business.Address;
import net.ihe.gazelle.app.patientregistryapi.business.AddressUse;
import net.ihe.gazelle.app.patientregistryapi.business.ContactPoint;
import net.ihe.gazelle.app.patientregistryapi.business.ContactPointUse;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.Observation;
import net.ihe.gazelle.app.patientregistryapi.business.ObservationType;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryapi.business.PersonName;
import net.ihe.gazelle.app.pdqmconnectorservice.adapter.connector.Chain;
import net.ihe.gazelle.app.pdqmconnectorservice.adapter.webservice.PatientSimulatorWebService;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.constants.SearchCriteriaName;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.constants.SearchCriteriaVerb;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.fhir.CriteriaBuilder;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.fhir.HapiFhirResourceParser;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.fhir.ResponseBundle;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.gitb.ProcessRequestWrapper;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.utils.cucumber.CucumberMockManager;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.utils.patient.PatientServiceManager;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.utils.xml.ResponseBodyConverter;
import net.ihe.gazelle.lib.gitbutils.adapter.MappingException;
import org.junit.Assert;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletConfig;

import javax.ws.rs.core.Response;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import static com.github.tomakehurst.wiremock.client.WireMock.anyUrl;
import static com.github.tomakehurst.wiremock.client.WireMock.exactly;
import static com.github.tomakehurst.wiremock.client.WireMock.findAll;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Step definitions for Translation between FHIR and Business model tests.
 */
public class TranslateFhirToBusinessStepDefinitions {

    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    private CucumberMockManager cucumberMockManager;
    private PatientServiceManager patientServiceManager;

    private String stringCriteria = "";

    private Response response;

    /**
     * Set up the in-memory Database as well as services that will be tested.
     *
     * @throws MalformedURLException if the Mock URL is not well formed
     */
    @Before
    public void setUpInMemoryDBAndFeedClient() throws MalformedURLException {
        this.cucumberMockManager = new CucumberMockManager();
        this.patientServiceManager = new PatientServiceManager(cucumberMockManager.getMockProcessingServiceURL());
        this.cucumberMockManager.setPatientServiceManager(patientServiceManager);
    }

    /**
     * Clear the database and close the connection. Also turn down the Mock server.
     */
    @After
    public void tearDownInMemoryDBAndMock() {
        this.cucumberMockManager.stopMock();
        this.patientServiceManager.tearDown();
    }

    /**
     * Start transaction for step.
     */
    @BeforeStep
    public void startTransaction() {
        patientServiceManager.startTransaction();
    }

    /**
     * End transaction after step.
     */
    @AfterStep
    public void endTransaction() {
        patientServiceManager.endTransaction();
    }

    /**
     * Initialize a cache of existing patients to be used in tests.
     *
     * @param patientDataList list of data to initialize patients from
     * @throws ParseException if dates cannot be parsed.
     */
    @Given("the following patients exist")
    public void the_following_patients_exist(List<Map<String, String>> patientDataList) throws ParseException {
        patientServiceManager.addExistingPatients(patientDataList);
    }

    /**
     * Precondition that will add identifiers to existing patients.
     *
     * @param patientIdentifierDataList list on information on Identifiers to create
     */
    @Given("patients have the following identifiers")
    public void patients_have_the_following_identifiers(List<Map<String, String>> patientIdentifierDataList) {
        for (Map<String, String> identifierData : patientIdentifierDataList) {

            EntityIdentifier identifier = new EntityIdentifier(identifierData.get("systemIdentifier"),
                    identifierData.get("systemName"), identifierData.get("type"), identifierData.get("value"));

            Patient patient = patientServiceManager.getExistingPatientWithUUID(identifierData.get("tmpuuid"));
            if (patient == null) {
                throw new IllegalArgumentException(String.format("No patient corresponding to UUID %s !", identifierData.get("tmpuuid")));
            } else {
                patient.addIdentifier(identifier);
            }
        }
    }

    /**
     * Precondition that will add names to existing patients.
     *
     * @param nameDataList list on information on names to create
     */
    @Given("patients have the following names")
    public void patients_have_the_following_names(List<Map<String, String>> nameDataList) {
        for (Map<String, String> nameData : nameDataList) {

            PersonName personName = new PersonName(nameData.get("family"), null, null, null);
            if (nameData.get("given1") != null) {
                personName.addGiven(nameData.get("given1"));
            }
            if (nameData.get("given2") != null) {
                personName.addGiven(nameData.get("given2"));
            }
            if (nameData.get("given3") != null) {
                personName.addGiven(nameData.get("given3"));
            }

            Patient patient = patientServiceManager.getExistingPatientWithUUID(nameData.get("tmpuuid"));
            if (patient == null) {
                throw new IllegalArgumentException(String.format("No patient corresponding to UUID %s !", nameData.get("tmpuuid")));
            } else {
                patient.addName(personName);
            }
        }
    }

    /**
     * Precondition that will add addresses to existing patients.
     *
     * @param addressDataList list on information on addresses to create
     */
    @Given("patients have the following addresses")
    public void patients_have_the_following_addresses(List<Map<String, String>> addressDataList) {
        for (Map<String, String> addressData : addressDataList) {

            Address address = new Address(addressData.get("city"), addressData.get("countryIso3"), addressData.get("postalCode"), addressData.get(
                    "state")
                    , AddressUse.valueOf(addressData.get("use")));
            if (addressData.get("line1") != null && !addressData.get("line1").isEmpty()) {
                address.addLine(addressData.get("line1"));
            }
            Patient patient = patientServiceManager.getExistingPatientWithUUID(addressData.get("tmpuuid"));
            if (patient == null) {
                throw new IllegalArgumentException(String.format("No patient corresponding to UUID %s !", addressData.get("tmpuuid")));
            } else {
                patient.addAddress(address);
            }
        }
    }

    /**
     * Precondition that will add contact points to existing patients.
     *
     * @param contactPointDataList list on information on contact points to create
     */
    @Given("patients have the following contact points")
    public void patients_have_the_following_contact_points(List<Map<String, String>> contactPointDataList) {
        for (Map<String, String> contactPointData : contactPointDataList) {

            ContactPoint contactPoint = new ContactPoint(null, ContactPointUse.valueOf(contactPointData.get("use")),
                    contactPointData.get("value"));

            Patient patient = patientServiceManager.getExistingPatientWithUUID(contactPointData.get("tmpuuid"));
            if (patient == null) {
                throw new IllegalArgumentException(String.format("No patient corresponding to UUID %s !", contactPointData.get("tmpuuid")));
            } else {
                patient.addContactPoint(contactPoint);
            }
        }
    }

    /**
     * Precondition that will add observations to existing patients.
     *
     * @param observationDataList list on information on observations to create
     */
    @Given("patients have the following observations")
    public void patients_have_the_following_observations(List<Map<String, String>> observationDataList) {
        for (Map<String, String> observationData : observationDataList) {

            Observation observation = new Observation(ObservationType.valueOf(observationData.get("type")), observationData.get("value"));

            Patient patient = patientServiceManager.getExistingPatientWithUUID(observationData.get("tmpuuid"));
            if (patient == null) {
                throw new IllegalArgumentException(String.format("No patient corresponding to UUID %s !", observationData.get("tmpuuid")));
            } else {
                patient.addObservation(observation);
            }
        }
    }

    /**
     * Create a search criterion for the test Search Request
     *
     * @param searchCriteriaNameString literal value of the criterion name
     * @param searchCriteriaVerbString literal value of the criterion verb
     * @param searchCriteriaValue      literal value of the criterion value
     */
    @Given("search criteria {string} {string} {string}")
    public void searchCriteriaDefinitionStep(String searchCriteriaNameString, String searchCriteriaVerbString, String searchCriteriaValue) {
        SearchCriteriaName name = SearchCriteriaName.fromString(searchCriteriaNameString);
        SearchCriteriaVerb verb = SearchCriteriaVerb.fromString(searchCriteriaVerbString);
        if (name == null && verb == null && searchCriteriaValue == null) {
            return;
        } else {
            CriteriaBuilder.checkCriterion(name, verb, searchCriteriaValue);
        }
        if (SearchCriteriaName.TMP_UUID.equals(name) && patientServiceManager.patientWasFedWithTmpUUID(searchCriteriaValue)) {
            searchCriteriaValue = patientServiceManager.getAssignedUUID(searchCriteriaValue);
        } else if (!SearchCriteriaName.UUID.equals(name)) {
            fail(String.format("Unsupported Criterion : %s", name));
        }

        String requestParameter = CriteriaBuilder.buildCriterion(name, verb, searchCriteriaValue);
        assertNotNull(requestParameter);
        if (stringCriteria.isEmpty()) {
            stringCriteria = "?";
        } else {
            stringCriteria = stringCriteria + "&";
        }
        stringCriteria = stringCriteria + requestParameter;
    }

    /**
     * Perform the search request
     */
    @When("search is done")
    public void search_is_done() throws MappingException {
        this.cucumberMockManager.updateMock();
        MockHttpServletRequest servletRequest = new MockHttpServletRequest(null);
        servletRequest.setMethod("GET");
        servletRequest.setServerName(patientServiceManager.getBaseUrl());
        servletRequest.setScheme("http");
        servletRequest.setRequestURI("/Patient");
        servletRequest.setQueryString(stringCriteria);

        MockHttpServletResponse servletResponse = new MockHttpServletResponse();
        PatientSimulatorWebService webService = new PatientSimulatorWebService(new Chain(patientServiceManager.getPatientProcessingService()));
        webService.setConfig(new MockServletConfig());

        this.response = webService.getPatientList(servletRequest, servletResponse);
    }

    /**
     * Checks that SearchCriteria received by the Mock server contains the specified criterion.
     *
     * @param searchCriteriaNameString literal value of the criterion name
     * @param searchCriteriaVerbString literal value of the criterion verb
     * @param searchCriteriaValue      literal value of the criterion value
     * @throws MappingException if received Request cannot be mapped to SearchCriteria.
     */
    @Then("forwarded parameter contains {string} {string} {string}")
    public void receivedParametersContains(String searchCriteriaNameString, String searchCriteriaVerbString, String searchCriteriaValue) throws MappingException {

        SearchCriteriaName name = SearchCriteriaName.fromString(searchCriteriaNameString);
        SearchCriteriaVerb verb = SearchCriteriaVerb.fromString(searchCriteriaVerbString);
        if (name == null && verb == null && searchCriteriaValue == null) {
            return;
        } else {
            CriteriaBuilder.checkCriterion(name, verb, searchCriteriaValue);
        }
        verify(exactly(1), postRequestedFor(anyUrl()));


        LoggedRequest request = findAll(postRequestedFor(anyUrl())).get(0);

        ProcessRequestWrapper requestWrapper = new ProcessRequestWrapper(ResponseBodyConverter.xmlToProcessRequest(request.getBodyAsString()));
        requestWrapper.checkAttributeValueForRequest(name, verb, searchCriteriaValue);
    }

    /**
     * Checks Response status.
     *
     * @param responseStatus Excpected status of the response
     */
    @Then("response is {string}")
    public void response_is(String responseStatus) {
        if ("OK".equals(responseStatus)) {
            assertNotNull("Response shall not be null !", response);
            Assert.assertEquals(200, this.response.getStatus());
        } else {
            fail(String.format("Unexpected response status to check : %s", responseStatus));
        }
    }

    /**
     * Check the number of received patients.
     *
     * @param expectedNumberOfPatients expected number of received patients.
     */
    @Then("received {int} patients")
    public void checkNumberOfReceivedPatients(int expectedNumberOfPatients) {
        ResponseBundle responseBundle = new ResponseBundle(new HapiFhirResourceParser()
                .parseBundle(this.response.getEntity().toString(), HapiFhirResourceParser.FhirResourceFormat.XML));

        Assert.assertEquals(expectedNumberOfPatients, responseBundle.getNumberOfPatientsInBundle());
    }

    /**
     * Check that the received patient is the one expected.
     *
     * @param provisionalUUID literal UUID value to uniquely identify the patient.
     */
    @Then("received patient is received with uuid {string}")
    public void received_patient_is_received_with_uuid(String provisionalUUID) {
        ResponseBundle responseBundle = new ResponseBundle(new HapiFhirResourceParser()
                .parseBundle(this.response.getEntity().toString(), HapiFhirResourceParser.FhirResourceFormat.XML));
        assertNotNull("ProcessResponse shall not be null !", responseBundle);

        org.hl7.fhir.r4.model.Patient retrievedPatient = responseBundle.getPatientFromIndexInBundle(0);

        assertEquals(patientServiceManager.getFullURL() + "/Patient/" + provisionalUUID, retrievedPatient.getId());
    }
}
