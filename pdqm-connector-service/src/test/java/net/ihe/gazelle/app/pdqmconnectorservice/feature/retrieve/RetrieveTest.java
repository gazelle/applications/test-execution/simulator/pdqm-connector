package net.ihe.gazelle.app.pdqmconnectorservice.feature.retrieve;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

/**
 * Launcher for Retrieve Tests
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:feature/retrieve" , glue ={"net.ihe.gazelle.app.pdqmconnectorservice.feature.retrieve"})
public class RetrieveTest {

}
