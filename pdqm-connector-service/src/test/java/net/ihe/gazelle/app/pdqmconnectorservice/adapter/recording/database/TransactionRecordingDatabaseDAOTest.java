package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.database;

import net.ihe.gazelle.app.pdqmconnectorservice.business.recording.RecordingException;
import net.ihe.gazelle.app.pdqmconnectorservice.business.recording.TransactionInstance;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TransactionRecordingDatabaseDAOTest {

   private static final String PERSISTENCE_UNIT_NAME_CONST = "PersistenceUnitTest";

   private static EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST);
   private EntityManager entityManager;
   TransactionRecordingDatabaseDAO transactionInstanceDAO;

   @BeforeAll
   public static void setupDatabase() {
      factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST);
   }

   @AfterAll
   public static void closeAndDropDatabase() {
      factory.close();
   }

   @BeforeEach
   public void initializeEntityManager() {
      entityManager = factory.createEntityManager();
      entityManager.getTransaction().begin();
      transactionInstanceDAO = new TransactionRecordingDatabaseDAO(entityManager);
   }

   @AfterEach
   public void closeEntityManager() {
      entityManager.getTransaction().commit();
      entityManager.close();
   }

   @Test
   public void testIsDomainExistingFalse() throws RecordingException {
      assertFalse(transactionInstanceDAO.isDomainExisting("anyDomainKeyword"));
   }

   @Test
   public void testIsTransactionExistingFalse() throws RecordingException {
      assertFalse(transactionInstanceDAO.isTransactionExisting("anyTransactionKeyword"));
   }

   @Test
   public void testIsActorExistingFalse() throws RecordingException {
      assertFalse(transactionInstanceDAO.isActorExisting("anyActorKeyword"));
   }

   @Test
   public void createDomainTestAndIsExisting() throws RecordingException {
      String keyword = "domainKeyword";
      transactionInstanceDAO.createDomain(keyword);
      assertTrue(transactionInstanceDAO.isDomainExisting(keyword));
   }

   @Test
   public void createTransactionTestAndIsExisting() throws RecordingException {
      String keyword = "transactionKeyword";
      transactionInstanceDAO.createTransaction(keyword);
      assertTrue(transactionInstanceDAO.isTransactionExisting(keyword));
   }

   @Test
   public void createActorTestAndIsExisting() throws RecordingException {
      String keyword = "actorKeyword";
      transactionInstanceDAO.createActor(keyword);
      assertTrue(transactionInstanceDAO.isActorExisting(keyword));
   }

   @Test
   public void testTransactionCreation() {
      Long transactionNumberPrev = transactionInstanceDAO.getNumberOfRegisteredTransactionInstances();
      TransactionInstance instanceBusiness = new TransactionInstance();
      transactionInstanceDAO.saveTransaction(instanceBusiness);
      Long transactionNumberNext = transactionInstanceDAO.getNumberOfRegisteredTransactionInstances();
      assertEquals(1, transactionNumberNext.compareTo(transactionNumberPrev));
   }

}