package net.ihe.gazelle.app.pdqmconnectorservice.feature.utils.xml;

import com.gitb.ps.ProcessRequest;
import com.gitb.ps.ProcessResponse;
import org.w3c.dom.Document;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.fail;

/**
 * Transform ProcessRequest/ProcessResponse from/to an XML string for SOAP Webservices.
 */
public class ResponseBodyConverter {

    /**
     * Transform an XML String into a {@link ProcessRequest} object.
     *
     * @param xmlString String to transform
     * @return the parsed {@link ProcessRequest}
     */
    public static ProcessRequest xmlToProcessRequest(String xmlString) {
        byte[] xml = xmlString.getBytes();
        try {
            InputStream inputStream = new ByteArrayInputStream(xml);
            SOAPMessage request = MessageFactory.newInstance().createMessage(null, inputStream);
            Document body = request.getSOAPBody().extractContentAsDocument();

            JAXBContext jax = JAXBContext.newInstance(ProcessRequest.class);

            Unmarshaller unmarshaller = jax.createUnmarshaller();

            JAXBElement<ProcessRequest> jaxbElement = unmarshaller
                    .unmarshal(new StreamSource(new ByteArrayInputStream(documentToString(body).getBytes(StandardCharsets.UTF_8))),
                            ProcessRequest.class);
            return jaxbElement.getValue();
        } catch (JAXBException | IOException | SOAPException e) {
            fail(String.format("Cannot unmarshal XML String as %s", ProcessRequest.class.getSimpleName()));
        }
        return null;
    }

    /**
     * Transform a {@link ProcessResponse} to an XML String
     *
     * @param object {@link ProcessResponse} object to transform
     * @return XML representation of the object.
     */
    public static String objectToXml(ProcessResponse object) {
        try {
            JAXBContext jax = JAXBContext.newInstance(object.getClass());
            JAXBElement jaxbElement = new JAXBElement(new QName("http://www.gitb.com/ps/v1/", object.getClass().getSimpleName()), object.getClass()
                    , object);

            Marshaller marshaller = jax.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            StringWriter stringWriter = new StringWriter();
            marshaller.marshal(jaxbElement, stringWriter);

            return addSOAPEnvelope(stringWriter.toString());
        } catch (JAXBException e) {
            fail(String.format("Cannot unmarshal XML String as %s", object.getClass().getSimpleName()));
        }
        return null;
    }

    /**
     * Add SOAP envelope to the inputted String.
     *
     * @param bodyValue literal value of the SOAP body
     * @return literal representation of the full SOAP Envelope
     */
    private static String addSOAPEnvelope(String bodyValue) {
        String headFreeMessageFragment = bodyValue.substring(56);
        String top = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:v1=\"http://www.gitb.com/ps/v1/\" " +
                "xmlns:v11=\"http://www.gitb.com/core/v1/\">\n" +
                "    <soapenv:Header/>\n" +
                "    <soapenv:Body>\n";
        String bottom = "    </soapenv:Body>\n" +
                "</soapenv:Envelope>";
        String wholeMessage = top + headFreeMessageFragment + bottom;
        return wholeMessage;
    }

    /**
     * Transform a {@link Document} to String
     *
     * @param document the document to transform
     * @return the literal value of the document.
     */
    private static String documentToString(Document document) {
        try {
            StringWriter sw = new StringWriter();
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

            transformer.transform(new DOMSource(document), new StreamResult(sw));
            return sw.toString();
        } catch (Exception ex) {
            throw new RuntimeException("Error converting to String", ex);
        }
    }


}
