package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.interceptor;

import net.ihe.gazelle.app.pdqmconnectorservice.adapter.webservice.ContainerRequestContextTestMock;
import net.ihe.gazelle.app.pdqmconnectorservice.adapter.webservice.ContainerResponseContextTestMock;
import net.ihe.gazelle.app.pdqmconnectorservice.adapter.webservice.HttpServletRequestTestMock;
import net.ihe.gazelle.app.pdqmconnectorservice.business.PDQm;
import net.ihe.gazelle.app.pdqmconnectorservice.business.recording.EStandardBusiness;
import net.ihe.gazelle.app.pdqmconnectorservice.business.recording.TransactionInstance;
import net.ihe.gazelle.fhir.constants.FhirConstants;
import net.ihe.gazelle.lib.annotations.Package;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

@Package
class PDQmTransactionInstanceMapperTest {

   /**
    * tested class
    */
   private PDQmTransactionInstanceMapper PDQmTransactionInstanceMapper;

   private HttpServletRequestTestMock servletRequest;
   private ContainerRequestContextTestMock containerRequestContext;
   private ContainerResponseContextTestMock containerResponseContext;
   private static final String localhostAddress = "127.0.0.1";
   private static final String localhostHost = "localhost";

   @BeforeEach
   private void setUp() throws URISyntaxException {
      PDQmTransactionInstanceMapper = new PDQmTransactionInstanceMapper();
      servletRequest = new HttpServletRequestTestMock(localhostHost, localhostAddress, "application/x-turtle,application/json+fhir",
            "application/json+fhir;q=0.9,*/*;q=0.8");
      containerRequestContext = new ContainerRequestContextTestMock("REQUEST");
      containerRequestContext.setMethod("GET");
      containerRequestContext.setRequestUri(new URI("http://test/Patient"));
      containerResponseContext = new ContainerResponseContextTestMock("RESPONSE");
   }

   @Test
   void saveTransactionBaseIP() {
      TransactionInstance transactionInstance = PDQmTransactionInstanceMapper
            .getTransactionInstanceFromRequestResponseContext(containerRequestContext, containerResponseContext, servletRequest);
      assertNotNull(transactionInstance);
      Assertions.assertEquals(localhostHost, transactionInstance.getRequest().getIssuer());
      Assertions.assertEquals(localhostAddress, transactionInstance.getRequest().getIssuerIpAddress());
      Assertions.assertEquals(PDQm.Actors.PDC.getKeyword(), transactionInstance.getRequest().getIssuingActorKeyword());
   }

   @Test
   void saveTransactionNewIP() {
      String newIp = "192.12.251.3";
      servletRequest.setRemoteAddr(newIp);
      TransactionInstance transactionInstance = PDQmTransactionInstanceMapper
            .getTransactionInstanceFromRequestResponseContext(containerRequestContext, containerResponseContext, servletRequest);
      assertNotNull(transactionInstance);
      Assertions.assertEquals(newIp, transactionInstance.getRequest().getIssuerIpAddress());
      Assertions.assertEquals(PatientManagerConstants.ISSUER_DEFAULT_NAME, transactionInstance.getResponse().getIssuer());
      Assertions.assertEquals(PDQm.Actors.PDC.getKeyword(), transactionInstance.getRequest().getIssuingActorKeyword());
   }

   @Test
   void saveTransactionJSONFormat() {
      TransactionInstance transactionInstance = PDQmTransactionInstanceMapper
            .getTransactionInstanceFromRequestResponseContext(containerRequestContext, containerResponseContext, servletRequest);

      assertNotNull(transactionInstance);
      Assertions.assertEquals(EStandardBusiness.FHIR_JSON, transactionInstance.getStandard());
   }

   @Test
   void saveTransactionXMLFormat() {
      String newFormat = "application/x-turtle,application/xml+fhir";
      servletRequest.setFormatParameterValues(newFormat);

      TransactionInstance transactionInstance = PDQmTransactionInstanceMapper
            .getTransactionInstanceFromRequestResponseContext(containerRequestContext, containerResponseContext, servletRequest);

      assertNotNull(transactionInstance);
      Assertions.assertEquals(EStandardBusiness.FHIR_XML, transactionInstance.getStandard());
   }

   @Test
   void saveTransactionJSONFormatHeader() {
      servletRequest.setFormatParameterValues(null);

      TransactionInstance transactionInstance = PDQmTransactionInstanceMapper
            .getTransactionInstanceFromRequestResponseContext(containerRequestContext, containerResponseContext, servletRequest);
      assertNotNull(transactionInstance);

      Assertions.assertEquals(EStandardBusiness.FHIR_JSON, transactionInstance.getStandard());
   }

   @Test
   void saveTransactionXMLFormatHeader() {
      String newHeader = "application/fhir+xml;q=0.9,*/*;q=0.8";
      servletRequest.setFormatParameterValues(null);
      servletRequest.setAcceptHeaderValues(newHeader);

      TransactionInstance transactionInstance = PDQmTransactionInstanceMapper
            .getTransactionInstanceFromRequestResponseContext(containerRequestContext, containerResponseContext, servletRequest);

      assertNotNull(transactionInstance);
      Assertions.assertEquals(EStandardBusiness.FHIR_XML, transactionInstance.getStandard());
   }

   @Test
   void saveTransactionXMLDefaultFormat() {
      String newHeader = "application/x-turtle;q=0.9,*/*;q=0.8";
      servletRequest.setFormatParameterValues(null);
      servletRequest.setAcceptHeaderValues(newHeader);

      TransactionInstance transactionInstance = PDQmTransactionInstanceMapper
            .getTransactionInstanceFromRequestResponseContext(containerRequestContext, containerResponseContext, servletRequest);

      assertNotNull(transactionInstance);
       Assertions.assertEquals(EStandardBusiness.FHIR_XML, transactionInstance.getStandard());
   }

   @Test
   void saveTransactionDate() {

       TransactionInstance transactionInstance = PDQmTransactionInstanceMapper
             .getTransactionInstanceFromRequestResponseContext(containerRequestContext, containerResponseContext, servletRequest);

      assertNotNull(transactionInstance);
      assertNotNull(transactionInstance.getTimestamp());
   }

   @Test
   void saveTransactionVisible() {

       TransactionInstance transactionInstance = PDQmTransactionInstanceMapper
             .getTransactionInstanceFromRequestResponseContext(containerRequestContext, containerResponseContext, servletRequest);

      assertNotNull(transactionInstance);
      Assertions.assertTrue(transactionInstance.getVisible());
   }

   @Test
   void saveTransactionDomain() {

       TransactionInstance transactionInstance = PDQmTransactionInstanceMapper
             .getTransactionInstanceFromRequestResponseContext(containerRequestContext, containerResponseContext, servletRequest);

      assertNotNull(transactionInstance);
      assertNotNull(transactionInstance.getDomainKeyword());
      Assertions.assertEquals(PDQm.DOMAIN.getKeyword(), transactionInstance.getDomainKeyword());
   }

   @Test
   void saveTransactionActor() {

       TransactionInstance transactionInstance = PDQmTransactionInstanceMapper
             .getTransactionInstanceFromRequestResponseContext(containerRequestContext, containerResponseContext, servletRequest);

      assertNotNull(transactionInstance);
      assertNotNull(transactionInstance.getDomainKeyword());
      Assertions.assertEquals(PDQm.Actors.PDS.getKeyword(), transactionInstance.getSimulatedActorKeyword());
   }

   @Test
   void saveTransactionTransaction() {

       TransactionInstance transactionInstance = PDQmTransactionInstanceMapper
             .getTransactionInstanceFromRequestResponseContext(containerRequestContext, containerResponseContext, servletRequest);

      assertNotNull(transactionInstance);
      assertNotNull(transactionInstance.getDomainKeyword());
      Assertions.assertEquals(PDQm.Transactions.ITI_78.getKeyword(), transactionInstance.getTransactionKeyword());
   }

   @Test
   void saveTransactionCompanyKeyword() {
      TransactionInstance transactionInstance = PDQmTransactionInstanceMapper
             .getTransactionInstanceFromRequestResponseContext(containerRequestContext, containerResponseContext, servletRequest);
      assertNotNull(transactionInstance);
      assertNull(transactionInstance.getCompanyKeyword());
   }

   @Test
   void saveTransactionResponseContent() {
      TransactionInstance transactionInstance = PDQmTransactionInstanceMapper
             .getTransactionInstanceFromRequestResponseContext(containerRequestContext, containerResponseContext, servletRequest);
      assertNotNull(transactionInstance);
      assertNotNull(transactionInstance.getResponse());
      Assertions.assertEquals(PatientManagerConstants.ISSUER_DEFAULT_NAME, transactionInstance.getResponse().getIssuer());
      Assertions.assertEquals(PDQm.Actors.PDS.getKeyword(), transactionInstance.getResponse().getIssuingActorKeyword());
      assertEquals("RESPONSE", new String(transactionInstance.getResponse().getContent(), StandardCharsets.UTF_8));
   }

   @Test
   void saveTransactionRequestContent() throws URISyntaxException {
      String requestURI = "requestURI";
      containerRequestContext.setRequestUri(new URI(requestURI));
      TransactionInstance transactionInstance = PDQmTransactionInstanceMapper
             .getTransactionInstanceFromRequestResponseContext(containerRequestContext, containerResponseContext, servletRequest);
      assertNotNull(transactionInstance);
      assertNotNull(transactionInstance.getRequest());
      assertEquals(requestURI, new String(transactionInstance.getRequest().getContent(), StandardCharsets.UTF_8));
   }

   @Test
   void saveTransactionRetrieveRequestType() throws URISyntaxException {
      containerRequestContext.setRequestUri(new URI("http://test/Patient/123"));
      TransactionInstance transactionInstance = PDQmTransactionInstanceMapper
             .getTransactionInstanceFromRequestResponseContext(containerRequestContext, containerResponseContext, servletRequest);
      assertNotNull(transactionInstance);
      assertNotNull(transactionInstance.getRequest());
      Assertions.assertEquals(FhirConstants.PDQM_RETRIEVE_REQUEST_TYPE, transactionInstance.getRequest().getType());
   }

   @Test
   void saveTransactionSearchRequestType() throws URISyntaxException {
      containerRequestContext.setRequestUri(new URI("http://test/Patient"));
      TransactionInstance transactionInstance = PDQmTransactionInstanceMapper
             .getTransactionInstanceFromRequestResponseContext(containerRequestContext, containerResponseContext, servletRequest);
      assertNotNull(transactionInstance);
      assertNotNull(transactionInstance.getRequest());
      Assertions.assertEquals(FhirConstants.PDQM_REQUEST_TYPE, transactionInstance.getRequest().getType());
   }

   @Test
   void saveTransactionUnknownType() throws URISyntaxException {
      containerRequestContext.setRequestUri(new URI("http://test.com/ihe-pix"));
      TransactionInstance transactionInstance = PDQmTransactionInstanceMapper
             .getTransactionInstanceFromRequestResponseContext(containerRequestContext, containerResponseContext, servletRequest);
      assertNotNull(transactionInstance);
      assertNotNull(transactionInstance.getRequest());
      Assertions.assertEquals("unknown", transactionInstance.getRequest().getType());
   }
}