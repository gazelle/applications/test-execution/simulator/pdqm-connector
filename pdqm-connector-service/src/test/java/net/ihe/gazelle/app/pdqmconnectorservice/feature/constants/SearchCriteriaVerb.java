package net.ihe.gazelle.app.pdqmconnectorservice.feature.constants;

/**
 * Search Criteria verbs to be used in .feature tests.
 *
 * @author wbars
 */
public enum SearchCriteriaVerb {
    EQUALS("is", ":exact=");

    private String nameInStep;
    private String requestParameterModifier;

    /**
     * Default constructor for the class.
     * @param nameInStep                    name in .feature step.
     * @param requestParameterModifier      modifier in FHIR request.
     */
    SearchCriteriaVerb(String nameInStep, String requestParameterModifier) {
        this.nameInStep = nameInStep;
        this.requestParameterModifier = requestParameterModifier;
    }

    /**
     * Getter for the requestParameterModifier property.
     * @return the value of the requestParameterModifier property.
     */
    public String getRequestParameterModifier() {
        return requestParameterModifier;
    }

    /**
     * Get the element from the enumeration based on its nameInStep property.
     * @param text      text to match the nameInStep property value.
     * @return corresponding {@link SearchCriteriaVerb}
     */
    public static SearchCriteriaVerb fromString(String text) {
        for (SearchCriteriaVerb verb : SearchCriteriaVerb.values()) {
            if (verb.nameInStep.equalsIgnoreCase(text)) {
                return verb;
            }
        }
        return null;
    }
}