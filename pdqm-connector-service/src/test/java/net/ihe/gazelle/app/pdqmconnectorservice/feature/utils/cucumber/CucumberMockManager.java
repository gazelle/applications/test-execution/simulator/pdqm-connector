package net.ihe.gazelle.app.pdqmconnectorservice.feature.utils.cucumber;

import com.gitb.ps.ProcessResponse;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.utils.patient.PatientServiceManager;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.utils.xml.ResponseBodyConverter;
import net.ihe.gazelle.lib.gitbutils.adapter.MapperObjectToAnyContent;
import net.ihe.gazelle.lib.gitbutils.adapter.MappingException;

import javax.ws.rs.core.MediaType;
import java.net.MalformedURLException;
import java.net.URL;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Manager class for the setup of Wiremock mock server
 *
 * @author wbars
 */
public class CucumberMockManager {

    private static final String SERVICE_PATH = "ProcessingServiceService";
    private static final int port = 9999;
    private String mockUrl;
    private WireMockServer server;
    private PatientServiceManager patientServiceManager;

    /**
     * Default constructor for the class.
     * @throws MalformedURLException if the defined url for the mock server is not well formed.
     */
    public CucumberMockManager() throws MalformedURLException{
        initiateMock();
    }

    /**
     * Setter for the patientServiceManager property. This is used to allow the Mock server to return the list of existing patients.
     * @param patientServiceManager value to set to the property.
     */
    public void setPatientServiceManager(PatientServiceManager patientServiceManager) {
        this.patientServiceManager = patientServiceManager;
    }

    /**
     * Get the URL of the Mocked Processing Service.
     * @return URL of the Mocked Processing Service.
     */
    public URL getMockProcessingServiceURL(){
        try {
            return new URL(this.getMockUrl() + "/" + SERVICE_PATH);
        } catch (MalformedURLException e){
            fail(String.format("Malformed URL : %s", this.getMockUrl() + "/" + SERVICE_PATH));
            return null;
        }
    }

    /**
     * Get base URL of the mock server.
     * @return the base URL of the Mock server.
     */
    private String getMockUrl() {
        if (this.mockUrl == null) {
            this.mockUrl = System.getenv().get("PAM_URL");
            if (this.mockUrl == null) {
                this.mockUrl = "http://localhost:" + port;
            }
        }
        return this.mockUrl;
    }

    /**
     * Initialize mock with Processing Service WSDL.
     * @throws MalformedURLException if the URL for the Mocked server is not well formed.
     */
    private void initiateMock() throws MalformedURLException {

        server = new WireMockServer(wireMockConfig().port(port).withRootDirectory("./src/test/resources/wiremock_resources"));
        URL mockUrl = new URL(getMockUrl());
        WireMock.configureFor(mockUrl.getHost(), mockUrl.getPort());
        server.start();

        server.stubFor(get(urlPathEqualTo("/" + SERVICE_PATH))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBodyFile("patreg_gitb_ps.wsdl")));
    }

    /**
     * Update the Mock to return the current list of existing patient known by its {@link PatientServiceManager}.
     * @throws MappingException if existing patients cannot be mapped to AnyContent.
     */
    public void updateMock() throws MappingException{
        server.stubFor(post(urlPathEqualTo("/" + SERVICE_PATH))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
//                        .withBodyFile("gitb_dummy_process_response.xml")));
                        .withBody(getResponseBody())));
    }

    /**
     * Stop the Mock Server.
     */
    public void stopMock() {
        server.stop();
    }

    /**
     * Get the literal value of the ProcessResponse to be returned by the Mock Server.
     * @return the literal value of the response.
     * @throws MappingException if existing patients cannot be mapped to AnyContent.
     */
    private String getResponseBody() throws MappingException {
        ProcessResponse processResponse = new ProcessResponse();

        TAR report = new TAR();
        report.setResult(TestResultType.SUCCESS);

        processResponse.setReport(report);
        processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent("PatientList", patientServiceManager.getExistingPatients()));

        return ResponseBodyConverter.objectToXml(processResponse);
    }
}
