package net.ihe.gazelle.app.pdqmconnectorservice.business.recording;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class MessageInstanceMetadataTest {

    @Test
    public void MessageInstanceMetadataBusinessDefaultConstructorTest() {
        MessageInstanceMetadata messageInstanceMetadata = new MessageInstanceMetadata();
        assertNotNull(messageInstanceMetadata);
    }


    @Test
    public void MessageInstanceMetadataBusinessCompleteConstructorTest() {
        String label = "label";
        String value = "value";

        MessageInstanceMetadata messageInstanceMetadata = new MessageInstanceMetadata(
                label, value);

        assertEquals(label, messageInstanceMetadata.getLabel());
        assertEquals(value, messageInstanceMetadata.getValue());
    }

}