package net.ihe.gazelle.app.pdqmconnectorservice.feature.utils.patient;

import com.gitb.ps.ProcessingService;
import net.ihe.gazelle.app.patientregistryapi.application.PatientFeedException;
import net.ihe.gazelle.app.patientregistryapi.application.PatientFeedService;
import net.ihe.gazelle.app.patientregistryapi.application.PatientSearchService;
import net.ihe.gazelle.app.patientregistryapi.business.GenderCode;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryfeedclient.adapter.PatientFeedClient;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.DomainDAOImpl;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.IdentifierDAOImpl;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.PatientDAOImpl;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.search.PatientSearchCriterionJPAMappingService;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.search.PatientSearchDAO;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.search.PatientSearchResultJPAMappingService;
import net.ihe.gazelle.app.patientregistryservice.adapter.ws.PatientFeedProcessingService;
import net.ihe.gazelle.app.patientregistryservice.adapter.ws.PatientProcessingService;
import net.ihe.gazelle.app.patientregistryservice.adapter.ws.PatientSearchProcessingService;
import net.ihe.gazelle.app.patientregistryservice.application.PatientFeedApplication;
import net.ihe.gazelle.app.patientregistryservice.application.PatientSearchApplication;
import net.ihe.gazelle.lib.gitbprocessingclient.adapter.GITBClientProcessImpl;
import org.junit.jupiter.api.Assertions;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Manager to the Patient Service. Allow to cache existing patients defined in tests as well as feeding them to the in-memory database and tracking
 * the assigned UUID.
 *
 * @author wbars
 */
public class PatientServiceManager {

   private static final String PERSISTENCE_UNIT_NAME = "PersistenceUnitTest";
   private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

   private EntityManagerFactory factory;
   private EntityManager entityManager;
   private PatientDAOImpl patientDAO;
   private PatientFeedClient patientFeedClient;
   private ProcessingService patientProcessingService;

   private HashMap<String, String> uuidOfFedPatients;
   private List<Patient> existingPatients;

   private String baseUrl;

   /**
    * Default constructor for the class. Instantiate a service with a java implementation of the {@link PatientProcessingService}.
    */
   public PatientServiceManager() {
      setupServices();
      instantiateResources();
   }

   /**
    * Constructor that will instantiate a {@link PatientProcessingService} from an URL. This can be a mock or a real deployed service.
    *
    * @param url
    */
   public PatientServiceManager(URL url) {
      setupServices(url);
      instantiateResources();
   }

   /**
    * Gets the {@link EntityManager} to access in-memory Database.
    *
    * @return the entityManager property value
    */
   public EntityManager getEntityManager() {
      return entityManager;
   }

   /**
    * Getter for the Patient DAO property.
    *
    * @return the value of the PatientDAO property.
    */
   public PatientDAOImpl getPatientDAO() {
      return patientDAO;
   }

   /**
    * Getter for the patientProcessingService property.
    *
    * @return the value of the patientProcessingService.
    */
   public ProcessingService getPatientProcessingService() {
      return patientProcessingService;
   }

   /**
    * Returns the map of UUID assigned to existing patients that were fed to the service.
    *
    * @return the value of the uuidOfFedPatients property
    */
   public HashMap<String, String> getUuidOfFedPatients() {
      return uuidOfFedPatients;
   }

   /**
    * Returns the list of existing patients.
    *
    * @return the value of the existingPatients property
    */
   public List<Patient> getExistingPatients() {
      return existingPatients;
   }

   /**
    * Add existing Patients from .feature datas
    *
    * @param patientDataList list of data from .feature.
    *
    * @throws ParseException if dates cannot be parsed.
    */
   public void addExistingPatients(List<Map<String, String>> patientDataList) throws ParseException {
      for (Map<String, String> patientData : patientDataList) {
         net.ihe.gazelle.app.patientregistryapi.business.Patient patient = new net.ihe.gazelle.app.patientregistryapi.business.Patient();
         patient.setUuid(patientData.get("tmpuuid"));
         patient.setGender(GenderCode.valueOf(patientData.get("gender")));
         patient.setActive(Boolean.valueOf(patientData.get("active")));
         patient.setDateOfBirth(dateFormat.parse(patientData.get("dateOfBirth")));
         patient.setDateOfDeath(dateFormat.parse(patientData.get("dateOfDeath")));
         patient.setMultipleBirthOrder(Integer.valueOf(patientData.get("multipleBirthOrder")));

         this.existingPatients.add(patient);
      }
   }

   /**
    * Start transaction.
    */
   public void startTransaction() {
      entityManager.getTransaction().begin();
   }

   /**
    * End transaction
    */
   public void endTransaction() {
      entityManager.getTransaction().commit();
   }

   /**
    * Feed a patient to the in-memory database
    *
    * @param uuid uuid of an existing patient
    *
    * @throws PatientFeedException if the feed cannot be performed
    */
   public void feedPatient(String uuid) throws PatientFeedException {
      Patient existingPatient = getExistingPatientWithUUID(uuid);
      if (existingPatient != null) {
         uuidOfFedPatients.put(uuid, patientFeedClient.feedPatient(existingPatient));
      } else {
         throw new IllegalArgumentException(String.format("No patient existing with UUID %s !", uuid));
      }
   }

   /**
    * Get an existing patient by its UUID.
    *
    * @param uuid uuid of the existing patient.
    *
    * @return the Patient with requested uuid.
    */
   public net.ihe.gazelle.app.patientregistryapi.business.Patient getExistingPatientWithUUID(String uuid) {
      if (uuid != null) {
         Patient existingPatient = existingPatients.stream()
               .filter(patient -> uuid.equals(patient.getUuid()))
               .findAny()
               .orElse(null);
         Assertions.assertNotNull(existingPatient, String.format("No existing patient with UUID %s !", uuid));
         return existingPatient;
      } else {
         throw new IllegalArgumentException("UUID shall not be null !");
      }
   }

   /**
    * Get base url for the Patient Service.
    *
    * @return base URL without scheme.
    */
   public String getBaseUrl() {
      if (this.baseUrl == null) {
         this.baseUrl = System.getenv().get("APP_URL");
         if (this.baseUrl == null) {
            this.baseUrl = "localhost:8999";
         }
      }
      return this.baseUrl;
   }

   /**
    * Get full url for the Patient Service.
    *
    * @return full URL with scheme.
    */
   public String getFullURL() {
      return "http://" + baseUrl;
   }

   /**
    * Check if an existing patient has been fed to the DB.
    *
    * @param tmpUUID temporary UUID of the existing patient.
    *
    * @return true if the patient was fed false otherwise.
    */
   public boolean patientWasFedWithTmpUUID(String tmpUUID) {
      return uuidOfFedPatients.containsKey(tmpUUID);
   }

   /**
    * Get UUID assigned to an existing patient after the feed operation.
    *
    * @param tmpUUID temporary UUID of an existing patient.
    *
    * @return the literal value of the assigned UUID.
    */
   public String getAssignedUUID(String tmpUUID) {
      return uuidOfFedPatients.get(tmpUUID);
   }

   /**
    * Close database access, EntityManager transaction must be commited before with a call to {@link PatientServiceManager#endTransaction()}
    */
   public void tearDown() {
      entityManager.close();
      factory.close();
   }

   /**
    * Setup the service with a java implementation.
    */
   private void setupServices() {
      this.factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
      this.entityManager = factory.createEntityManager();

      DomainDAOImpl domainDAO = new DomainDAOImpl(entityManager);
       IdentifierDAOImpl identifierDAO = new IdentifierDAOImpl(entityManager);
      this.patientDAO = new PatientDAOImpl(this.entityManager, new PatientSearchDAO(this.entityManager,
              new PatientSearchCriterionJPAMappingService(this.entityManager), new PatientSearchResultJPAMappingService()), domainDAO, identifierDAO);

      PatientFeedService patientFeedService = new PatientFeedApplication(this.patientDAO, domainDAO);
      PatientSearchService patientSearchService = new PatientSearchApplication(this.patientDAO);

      patientProcessingService = new PatientProcessingService(new PatientFeedProcessingService(patientFeedService),
            new PatientSearchProcessingService(patientSearchService));

      this.patientFeedClient = new PatientFeedClient(patientProcessingService);
   }

   /**
    * Setup the service using a remote server's URL.
    *
    * @param url url of the remote patient service.
    */
   private void setupServices(URL url) {
      this.factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
      this.entityManager = factory.createEntityManager();

      DomainDAOImpl domainDAO = new DomainDAOImpl(entityManager);
       IdentifierDAOImpl identifierDAO = new IdentifierDAOImpl(entityManager);
      this.patientDAO = new PatientDAOImpl(this.entityManager, new PatientSearchDAO(this.entityManager,
              new PatientSearchCriterionJPAMappingService(this.entityManager), new PatientSearchResultJPAMappingService()), domainDAO, identifierDAO);

      this.patientProcessingService = new GITBClientProcessImpl(url, "PatientProcessingServiceService", "PatientProcessingServicePort");
      this.patientFeedClient = new PatientFeedClient(patientProcessingService);
   }

   /**
    * Refresh existing and fed patients cache.
    */
   private void instantiateResources() {
      this.uuidOfFedPatients = new HashMap<>();
      this.existingPatients = new ArrayList<>();
   }
}
