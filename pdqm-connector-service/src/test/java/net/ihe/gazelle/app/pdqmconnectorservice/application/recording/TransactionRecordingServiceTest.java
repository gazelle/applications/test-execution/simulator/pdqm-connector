package net.ihe.gazelle.app.pdqmconnectorservice.application.recording;

import net.ihe.gazelle.app.pdqmconnectorservice.business.PDQm;
import net.ihe.gazelle.app.pdqmconnectorservice.business.recording.EStandardBusiness;
import net.ihe.gazelle.app.pdqmconnectorservice.business.recording.MessageInstance;
import net.ihe.gazelle.app.pdqmconnectorservice.business.recording.RecordingException;
import net.ihe.gazelle.app.pdqmconnectorservice.business.recording.TransactionInstance;
import net.ihe.gazelle.app.pdqmconnectorservice.business.recording.TransactionRecordingService;
import net.ihe.gazelle.fhir.constants.FhirConstants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class TransactionRecordingServiceTest {

   private static final String REQUEST_CONTENT = "request";
   private static final String RESPONSE_CONTENT = "response";

   /**
    * mocked class
    */
   private TransactionRecordingDAOTestMock dao;

   /**
    * tested class
    */
   private TransactionRecordingService recordingService;

   /**
    * SetUp Transaction recording DAO and service.
    */
   @BeforeEach
   public void setUp() {
      dao = new TransactionRecordingDAOTestMock();
      recordingService = new TransactionRecordingServiceImpl(dao);
   }

   /**
    * Test saving a transaction
    * @throws RecordingException if transaction cannot be saved.
    */
   @Test
   void testSaveTransactionNominal() throws RecordingException {

      // Given the following TF is registered
      dao.createDomain(PDQm.DOMAIN.getKeyword());
      dao.createTransaction(PDQm.Transactions.ITI_78.getKeyword());
      dao.createActor(PDQm.Actors.PDC.getKeyword());
      dao.createActor(PDQm.Actors.PDS.getKeyword());

      // When recording
      TransactionInstance transactionInstance = createTransactionInstanceForTest();
      recordingService.recordTransaction(transactionInstance);

      // Then the dao must have been called to save the transaction instance
      assertTrue(dao.isTransactionInstanceExisting(transactionInstance));
   }

   @Test
   void testSaveTransactionNewDomain() throws RecordingException {

      // Given the following TF is registered
      dao.createTransaction(PDQm.Transactions.ITI_78.getKeyword());
      dao.createActor(PDQm.Actors.PDC.getKeyword());
      dao.createActor(PDQm.Actors.PDS.getKeyword());

      // When recording
      TransactionInstance transactionInstance = createTransactionInstanceForTest();
      recordingService.recordTransaction(transactionInstance);

      // Then the dao must have been called to create the domain and save the transaction instance
      assertTrue(dao.isDomainExisting(PDQm.DOMAIN.getKeyword()));
      assertTrue(dao.isTransactionInstanceExisting(transactionInstance));
   }

   @Test
   void testSaveTransactionNewTransaction() throws RecordingException {

      // Given the following TF is registered
      dao.createDomain(PDQm.DOMAIN.getKeyword());
      dao.createActor(PDQm.Actors.PDC.getKeyword());
      dao.createActor(PDQm.Actors.PDS.getKeyword());

      // When recording
      TransactionInstance transactionInstance = createTransactionInstanceForTest();
      recordingService.recordTransaction(transactionInstance);

      // Then the dao must have been called to create the transaction and save the transaction instance
      assertTrue(dao.isTransactionExisting(PDQm.Transactions.ITI_78.getKeyword()));
      assertTrue(dao.isTransactionInstanceExisting(transactionInstance));
   }

   @Test
   void testSaveTransactionNewActor() throws RecordingException {

      // Given the following TF is registered
      dao.createDomain(PDQm.DOMAIN.getKeyword());
      dao.createTransaction(PDQm.Transactions.ITI_78.getKeyword());

      // When recording
      TransactionInstance transactionInstance = createTransactionInstanceForTest();
      recordingService.recordTransaction(transactionInstance);

      // Then the dao must have been called an provided the transaction instance and the
      assertTrue(dao.isActorExisting(PDQm.Actors.PDC.getKeyword()));
      assertTrue(dao.isActorExisting(PDQm.Actors.PDS.getKeyword()));
      assertTrue(dao.isTransactionInstanceExisting(transactionInstance));
   }

   private TransactionInstance createTransactionInstanceForTest() {
      MessageInstance request = new MessageInstance(PDQm.Actors.PDC.getKeyword(), REQUEST_CONTENT.getBytes(), FhirConstants.PDQM_REQUEST_TYPE, "test.host",
            "192.168.0.1");
      MessageInstance response = new MessageInstance(PDQm.Actors.PDS.getKeyword(), RESPONSE_CONTENT.getBytes(), FhirConstants.PDQM_RESPONSE, "localhost",
            "127.0.0.1");
      return new TransactionInstance(new Date(), true, PDQm.DOMAIN.getKeyword(), PDQm.Actors.PDS.getKeyword(),
            PDQm.Transactions.ITI_78.getKeyword(), EStandardBusiness.FHIR_XML, null, request, response);
   }


}