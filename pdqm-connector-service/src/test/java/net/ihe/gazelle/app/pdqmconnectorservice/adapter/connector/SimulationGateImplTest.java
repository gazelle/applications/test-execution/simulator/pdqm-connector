package net.ihe.gazelle.app.pdqmconnectorservice.adapter.connector;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;
import net.ihe.gazelle.sb.fhir.business.FhirResources;
import net.ihe.gazelle.sb.fhir.business.FhirSearchParameters;
import org.hl7.fhir.r4.model.Enumerations;
import org.hl7.fhir.r4.model.SearchParameter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.MediaType;
import java.net.MalformedURLException;
import java.net.URL;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class SimulationGateImplTest {

   SimulationGateImpl simulationGate;

   /**
    * mock server
    */
   private WireMockServer server = new WireMockServer(
         WireMockConfiguration.wireMockConfig().port(9999).withRootDirectory("src/test/resources/wiremock_resources"));

   private static final String RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE = "response should not be null";

   private static final String SERVICE_PATH = "ProcessingServiceService";

   /**
    * init the mock server and tested class
    *
    * @throws MalformedURLException if the url of the target is invlaid
    */
   @BeforeEach
   void init() throws MalformedURLException {
      server.start();
      server.stubFor(get(urlPathEqualTo("/" + SERVICE_PATH))
            .willReturn(aResponse()
                  .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                  .withBodyFile("patreg_gitb_ps.wsdl")));
      server.stubFor(WireMock.get(WireMock.urlPathEqualTo("/gitb_core.xsd"))
            .willReturn(WireMock.aResponse()
                  .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                  .withBodyFile("gitb_core.xsd")));
      server.stubFor(WireMock.get(WireMock.urlPathEqualTo("/gitb_ps.xsd"))
            .willReturn(WireMock.aResponse()
                  .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                  .withBodyFile("gitb_ps.xsd")));
      server.stubFor(WireMock.get(WireMock.urlPathEqualTo("/gitb_tr.xsd"))
            .willReturn(WireMock.aResponse()
                  .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                  .withBodyFile("gitb_tr.xsd")));
      simulationGate = new SimulationGateImpl(new URL("http://localhost:9999/" + SERVICE_PATH + "?wsdl"));
   }

   /**
    * reset the server
    */
   @AfterEach
   void stopServer() {
      server.resetAll();
      server.stop();
   }

   /**
    * test process function
    */
   @Test
   void processTest() {
      stubSoapFromFile("gitb_dummy_process_response.xml");

      assertThrows(SearchException.class, () -> simulationGate.process(new FhirSearchParameters()));
   }

   /**
    * test process function
    */
   @Test
   void processIdSearchParameterTest() {
      stubSoapFromFile("gitb_dummy_process_response.xml");
      FhirSearchParameters fhirSearchParameters = new FhirSearchParameters();
      fhirSearchParameters.addSearchParameter(org.hl7.fhir.instance.model.api.IAnyResource.SP_RES_ID, Enumerations.SearchParamType.STRING,
            SearchParameter.SearchModifierCode.EXACT, "");

      assertThrows(SearchException.class, () -> simulationGate.process(fhirSearchParameters));
   }

   /**
    * test process function
    */
   @Test
   void processResponseTest() {
      simulationGate = new SimulationGateImpl(new TestProcessingService());
      stubSoapFromFile("gitb_dummy_process_response.xml");
      FhirSearchParameters fhirSearchParameters = new FhirSearchParameters();
      fhirSearchParameters.addSearchParameter(org.hl7.fhir.instance.model.api.IAnyResource.SP_RES_ID, Enumerations.SearchParamType.STRING,
            SearchParameter.SearchModifierCode.EXACT, "");

      FhirResources actualResponse = null;
      try {
         actualResponse = simulationGate.process(fhirSearchParameters);
      } catch (Exception e) {
         e.printStackTrace();
      }

      assertNotNull(actualResponse, RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE);
   }

   /**
    * test process function with some patients that cannot be mapped to FHIR
    */
   @Test
   void processResponseTestPatientNotMapped() throws SearchException {
      stubSoapFromFile("gitb_mapping_process_response.xml");
      FhirSearchParameters fhirSearchParameters = new FhirSearchParameters();
      fhirSearchParameters.addSearchParameter(org.hl7.fhir.instance.model.api.IAnyResource.SP_RES_ID, Enumerations.SearchParamType.STRING,
            SearchParameter.SearchModifierCode.EXACT, "");

      assertNotNull(simulationGate.process(fhirSearchParameters));
   }


   /**
    * stub the server from file
    *
    * @param path the file path
    */
   private void stubSoapFromFile(String path) {
      server.stubFor(WireMock.post(WireMock.urlPathEqualTo("/" + SERVICE_PATH))
            .willReturn(WireMock.aResponse()
                  .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                  .withBodyFile(path)));
   }

}