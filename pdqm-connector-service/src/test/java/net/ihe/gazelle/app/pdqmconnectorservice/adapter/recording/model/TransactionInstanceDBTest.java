package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model;


import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class TransactionInstanceDBTest {


    @Test
    public void MessageInstanceMetadataBusinessDefaultConstructorTest() {
        TransactionInstanceDB transactionInstance = new TransactionInstanceDB();
        assertNotNull(transactionInstance);
    }


    @Test
    public void MessageInstanceMetadataBusinessCompleteConstructorTest() {
        Date timestamp = new Date();
        Boolean visible = true;
        String domainKeyword = "domainKeyword";
        String simulatedActorKeyword = "simulatedActorKeyword";
        String transactionKeyword = "transactionKeyword";
        EStandard standard = EStandard.FHIR_JSON;
        String companyKeyword = "companyKeyword";
        MessageInstance request = new MessageInstance();
        MessageInstance response = new MessageInstance();

        TransactionInstanceDB transactionInstance = new TransactionInstanceDB(timestamp, visible,
                domainKeyword, simulatedActorKeyword, transactionKeyword, standard, companyKeyword, request, response);

        assertEquals(timestamp, transactionInstance.getTimestamp());
        assertEquals(visible, transactionInstance.getVisible());
        assertEquals(domainKeyword, transactionInstance.getDomain().getKeyword());
        assertEquals(simulatedActorKeyword, transactionInstance.getSimulatedActor().getKeyword());
        assertEquals(transactionKeyword, transactionInstance.getTransaction().getKeyword());
        assertEquals(standard, transactionInstance.getStandard());
        assertEquals(companyKeyword, transactionInstance.getCompanyKeyword());
        assertEquals(request, transactionInstance.getRequest());
        assertEquals(response, transactionInstance.getResponse());
        assertNull(transactionInstance.getId());
    }

}