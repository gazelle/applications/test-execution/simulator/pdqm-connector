package net.ihe.gazelle.app.pdqmconnectorservice.feature.fhir;

import net.ihe.gazelle.app.pdqmconnectorservice.feature.constants.SearchCriteriaName;
import net.ihe.gazelle.app.pdqmconnectorservice.feature.constants.SearchCriteriaVerb;

import static org.junit.Assert.assertNotNull;

/**
 * Builder for FHIR criteria based on .feature steps informations.
 *
 * @author wbars
 */
public class CriteriaBuilder {

    /**
     * Build a string criterion formated for FHIR Search requests.
     *
     * @param name  name of teh search criterion
     * @param verb  verb corresponding to the modfier to use
     * @param value value to search on
     * @return the literal representation of the FHIR Search parameter.
     */
    public static String buildCriterion(SearchCriteriaName name, SearchCriteriaVerb verb, String value) {
        StringBuilder requestParameter = new StringBuilder();
        requestParameter.append(name.getNameInRequest());
        requestParameter.append(verb.getRequestParameterModifier());
        requestParameter.append(value);
        return requestParameter.toString();
    }

    /**
     * Check a criterion can be build from .feature info.
     *
     * @param name  name of the criterion
     * @param verb  verb of the criterion
     * @param value value of the criterion
     */
    public static void checkCriterion(SearchCriteriaName name, SearchCriteriaVerb verb,
                                      String value) {
        assertNotNull("Criterion name shall not be null", name);
        assertNotNull("Criterion verb shall not be null", verb);
        assertNotNull("Criterion value shall not be null", value);
    }
}
