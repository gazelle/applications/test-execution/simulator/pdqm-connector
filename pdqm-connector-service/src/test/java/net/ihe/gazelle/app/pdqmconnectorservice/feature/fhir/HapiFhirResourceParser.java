package net.ihe.gazelle.app.pdqmconnectorservice.feature.fhir;

import ca.uhn.fhir.parser.DataFormatException;
import ca.uhn.fhir.parser.IParser;
import net.ihe.gazelle.fhir.constants.FhirParserProvider;
import org.apache.commons.io.IOUtils;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Patient;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

import static org.junit.Assert.fail;

/**
 * This class parses FHIR resources from strings using hapi-fhir library.
 */
public class HapiFhirResourceParser {

    /**
     * Parse a String representation of a FHIR Bundle to a {@link Bundle} object.
     *
     * @param bundleContent : String content of the Bundle
     * @param format        : format of the resource. Supported formats are XML and JSON.
     */
    public Bundle parseBundle(String bundleContent, FhirResourceFormat format) {
        IBaseResource resource = parseStringForFhirResource(bundleContent, format);
        if (resource instanceof Bundle) {
            return (Bundle) resource;
        } else {
            fail("Resource shall be a bundle.");
            return null;
        }
    }

    /**
     * Parse a String representation of a FHIR Patient to a {@link Patient} object.
     *
     * @param patientContent : String content of the Patient
     * @param format         : format of the resource. Supported formats are XML and JSON.
     */
    public Patient parsePatient(String patientContent, FhirResourceFormat format) {
        IBaseResource resource = parseStringForFhirResource(patientContent, format);
        if (resource instanceof Patient) {
            return (Patient) resource;
        } else {
            fail("Resource shall be a bundle.");
            return null;
        }
    }

    /**
     * Parse a String representation of a FHIR Resource to a {@link IBaseResource} object.
     *
     * @param resourceContent : String content of the FHIR resource.
     * @param format          : format of the resource. Supported formats are XML and JSON.
     * @return {@link IBaseResource} parsed resource from String content
     * @throws CannotParseResourceException : when the resource cannot be parsed.
     */
    private IBaseResource parseStringForFhirResource(String resourceContent, FhirResourceFormat format) throws CannotParseResourceException {
        Reader reader = null;
        IBaseResource resource = null;
        IParser parser = getParserForFormat(format);
        if (parser != null) {
            reader = new InputStreamReader(new ByteArrayInputStream(resourceContent.getBytes(StandardCharsets.UTF_8)));
            try {
                resource = parser.parseResource(reader);
            } catch (DataFormatException e) {
                throw new CannotParseResourceException("Error when parsing Fhir resource.", e);
            }
        }
        IOUtils.closeQuietly(reader);
        return resource;
    }

    /**
     * Get the correct parser based on the format to parse.
     *
     * @param format : format of the resource. Supported formats are XML and JSON.
     * @return {@link IParser} hapi-fhir parser for FHIR resources.
     */
    private IParser getParserForFormat(FhirResourceFormat format) {
        if (FhirResourceFormat.XML.equals(format)) {
            return FhirParserProvider.getXmlParser();
        } else if (FhirResourceFormat.JSON.equals(format)) {
            return FhirParserProvider.getJsonParser();
        }
        return null;
    }

    /**
     * Enumeration of all format that can be used.
     */
    public enum FhirResourceFormat {
        XML, JSON
    }
}
