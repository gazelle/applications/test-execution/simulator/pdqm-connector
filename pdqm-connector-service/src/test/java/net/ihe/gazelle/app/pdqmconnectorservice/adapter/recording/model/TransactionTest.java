package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


/**
 * Tests for {@link Transaction}
 */
public class TransactionTest {

    /**
     * Test for the ID getter
     */
    @Test
    public void testGetId() {
        Transaction transaction = new Transaction();
        assertNull(transaction.getId());
    }

    /**
     * Test for hashCode method
     */
    @Test
    public void testHashCode() {
        Transaction transaction = new Transaction();
        assertNotNull(transaction.hashCode());
    }

    /**
     * Test for equals method
     */
    @Test
    public void testEquals() {
        Transaction transaction = new Transaction();
        assertEquals(transaction, transaction);
    }
}
