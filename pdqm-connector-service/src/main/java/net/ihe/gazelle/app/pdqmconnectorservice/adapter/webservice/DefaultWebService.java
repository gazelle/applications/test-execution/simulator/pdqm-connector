package net.ihe.gazelle.app.pdqmconnectorservice.adapter.webservice;

import javax.ws.rs.*;
import javax.ws.rs.ext.Provider;

/**
 * default web service to keep track of wrong calls
 */
@Provider
@Path(value="/{var:.+}")
public class DefaultWebService {

    /**
     * Default constructor for the class.
     */
    public DefaultWebService(){
        //Empty Constructor
    }

    /**
     * default GET method
     * @return a simple string
     */
    @GET
    public String get() {
        return "get";
    }

    /**
     * default POST method
     * @return a simple string
     */
    @POST
    public String post() {
        return "post";
    }

    /**
     * default PUT method
     * @return a simple string
     */
    @PUT
    public String put() {
        return "put";
    }

    /**
     * default PATCH method
     * @return a simple string
     */
    @PATCH
    public String patch() {
        return "patch";
    }

    /**
     * default DELETE method
     * @return a simple string
     */
    @DELETE
    public String delete() {
        return "delete";
    }

    /**
     * default HEAD method
     * @return a simple string
     */
    @HEAD
    public String head() {
        return "head";
    }


}
