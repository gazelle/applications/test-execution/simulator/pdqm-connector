package net.ihe.gazelle.app.pdqmconnectorservice.business;

/**
 * Static representation of the interroperability model of the PDQm profile
 *
 * @author ceoche
 */
public class PDQm {

   /**
    * IHE Domain to which PDQm is related to.
    */
   public static final GazelleEntityReference DOMAIN = new GazelleEntityReference("ITI", "IT Infrastructure");

   /**
    * IHE Profile description of PDQm
    */
   public static final GazelleEntityReference PROFILE = new GazelleEntityReference("PDQm", "Patient Demographic Query for Mobile");

   /**
    * Static representation of transactions available for PDQm profile
    */
   public static class Transactions {
      public static final GazelleEntityReference ITI_78 = new GazelleEntityReference("ITI-78", "Mobile Patient Demogaphics Query");

      /**
       * Hidden constructor
       */
      private Transactions() {
      }
   }

   /**
    * static representation of actors available for PDQm profile
    */
   public static class Actors {
      public static final GazelleEntityReference PDS = new GazelleEntityReference("PDS", "Patient Demographics Supplier");
      public static final GazelleEntityReference PDC = new GazelleEntityReference("PDC", "Patient Demographics Consumer");

      /**
       * Hidden constructor
       */
      private Actors() {
      }
   }

   /**
    * Hidden constructor
    */
   private PDQm() {
   }

}
