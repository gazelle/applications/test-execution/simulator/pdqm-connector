package net.ihe.gazelle.app.pdqmconnectorservice.business.recording;

/**
 * Record intercepted transactions in Simulator logs
 */
public interface TransactionRecordingService {

   /**
    * Save the intercepted transaction instance
    *
    * @param transactionInstance transaction instance to record in Simulator logs
    */
   void recordTransaction(TransactionInstance transactionInstance) throws RecordingException;
}
