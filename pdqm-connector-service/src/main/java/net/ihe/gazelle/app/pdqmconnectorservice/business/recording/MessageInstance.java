package net.ihe.gazelle.app.pdqmconnectorservice.business.recording;

import java.util.ArrayList;
import java.util.List;

/**
 * Instance of a message to record as simulation log.
 */
public class MessageInstance {

   private String issuingActorKeyword;
   private byte[] content;
   private String type;
   private String issuer;
   private String issuerIpAddress;
   private List<MessageInstanceMetadata> metadataList = new ArrayList<>();

   /**
    * <p>Constructor for MessageInstance.</p>
    */
   public MessageInstance() {
      //Empty constructor.
   }

   /**
    * Constructor of a MessageInstance
    *
    * @param issuingActorKeyword keyword of the actor issuing this message
    * @param content             message payload
    * @param type                message type (name of the request or response)
    * @param issuer              issuer name or host
    * @param issuerIpAddress     issuer IP address
    */
   public MessageInstance(String issuingActorKeyword, byte[] content, String type, String issuer, String issuerIpAddress) {
      this.setIssuingActorKeyword(issuingActorKeyword);
      this.setContent(content);
      this.setType(type);
      this.setIssuer(issuer);
      this.setIssuerIpAddress(issuerIpAddress);
   }

   /**
    * Get the keyword of the actor issuing the message
    *
    * @return the keyword of the actor issuing the message
    */
   public String getIssuingActorKeyword() {
      return issuingActorKeyword;
   }

   /**
    * Set the keyword of the actor issuing the message
    *
    * @param issuingActorKeyword the keyword of the actor issuing the message
    */
   public void setIssuingActorKeyword(String issuingActorKeyword) {
      this.issuingActorKeyword = issuingActorKeyword;
   }

   /**
    * Get the message content
    *
    * @return the message content as byte array
    */
   public byte[] getContent() {
      return content;
   }

   /**
    * Set the message content
    *
    * @param content the message content as byte array
    */
   public void setContent(byte[] content) {
      if (content != null) {
         this.content = content.clone();
      } else {
         this.content = null;
      }
   }

   /**
    * Get the message type. This is the message name according to the IHE technical framework.
    *
    * @return the message type
    */
   public String getType() {
      return type;
   }

   /**
    * Set the message type, This is the message name according to the IHE technical framework
    *
    * @param type the messagetype
    */
   public void setType(String type) {
      this.type = type;
   }

   /**
    * Get the name or the host of the issuer of the message
    *
    * @return the name or the host of the issuer of the message
    */
   public String getIssuer() {
      return issuer;
   }

   /**
    * Set the name or the host of the issuer of the message
    *
    * @param issuer the name or the host of the issuer of the message
    */
   public void setIssuer(String issuer) {
      this.issuer = issuer;
   }

   /**
    * Get the IP address of the issuer of the message
    *
    * @return Get the IP address of the issuer of the message
    */
   public String getIssuerIpAddress() {
      return issuerIpAddress;
   }

   /**
    * Set the IP Address of the issuer of the message
    *
    * @param issuerIpAddress the IP Address of the issuer of the message
    */
   public void setIssuerIpAddress(String issuerIpAddress) {
      this.issuerIpAddress = issuerIpAddress;
   }

   /**
    * Access the list of Message Instance Metadata
    *
    * @return the list of message instance metadata
    */
   public List<MessageInstanceMetadata> getMetadataList() {
      return metadataList;
   }
}
