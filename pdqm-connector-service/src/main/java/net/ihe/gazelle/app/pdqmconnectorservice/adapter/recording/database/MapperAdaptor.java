package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.database;

import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import net.ihe.gazelle.lib.annotations.Package;

/**
 * Automatic mapper used to transform Business TransactionInstanceDB to TransactionInstanceDB JPA Entity
 */
@Package
class MapperAdaptor {
   MapperFactory mapperFactory;

   /**
    * Constructor
    */
   @Package
   MapperAdaptor() {
      mapperFactory = new DefaultMapperFactory.Builder().build();
   }

   /**
    * Map a source instance into a new instance of the destination class.
    *
    * @param source           Instance source
    * @param destinationClass destination class to instantiate
    * @param <S>              Source type
    * @param <D>              Destination type
    *
    * @return an Instance of the destination class with all possible field setup with the source instance values.
    */
   @Package
   <S, D> D map(S source, Class<D> destinationClass) {
      return this.mapperFactory.getMapperFacade().map(source, destinationClass);
   }
}
