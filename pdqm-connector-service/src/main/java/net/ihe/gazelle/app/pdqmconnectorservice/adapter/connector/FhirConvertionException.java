package net.ihe.gazelle.app.pdqmconnectorservice.adapter.connector;

/**
 * Exception to be thrown by the {@link BusinessToFhirConverter} if some values cannot be mapped from business model to FHIR.
 * Such exception should lead the user to ignore the Patient in its return value.
 *
 * @author wbars
 */
public class FhirConvertionException extends Exception{

    private static final long serialVersionUID = -700408346059144760L;

    /**
     * Constructs a new exception with null as its detail message. The cause is not initialized, and may subsequently be initialized by a call to
     * {@link Throwable#initCause(Throwable)}.
     */
    public FhirConvertionException() {
        super();
    }

    /**
     * Constructs a new exception with the specified detail message. The cause is not initialized, and may subsequently be initialized by a call to
     * {@link Throwable#initCause(Throwable)}.
     *
     * @param message the detail message. Can be retrieved by a later call of {@link Throwable#getMessage()} method.
     */
    public FhirConvertionException(String message) {
        super(message);
    }

    /**
     * Constructs a new exception with the specified detail message and cause. Note that the detail/TransactionRecordingDAO message associated with cause is not automatically
     * incorporated in this exception's detail message.
     *
     * @param message the detail message. Can be retrieved by a later call of {@link Throwable#getMessage()} method.
     * @param cause   the cause. Can be retrieved by a lter call to {@link Throwable#getCause()}. A null value is permitted, and indicates that the
     *                cause is nonexistent or unknown.
     */
    public FhirConvertionException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs a new exception with the specified cause and a detail message of (cause==null ? null : cause.toString()) (which typically contains
     * the class and detail message of cause). This constructor is useful for exceptions that are little more than wrappers for other throwables (for
     * example, {@link java.security.PrivilegedActionException}).
     *
     * @param cause the cause. Can be retrieved by a lter call to {@link Throwable#getCause()}. A null value is permitted, and indicates that the cause
     *              is nonexistent or unknown.
     */
    public FhirConvertionException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new exception with the specified detail message, cause, suppression enabled or disabled, and writable stack trace enabled or
     * disabled.
     *
     * @param message            the detail message. Can be retrieved by a later call of {@link Throwable#getMessage()} method.
     * @param cause              the cause. Can be retrieved by a lter call to {@link Throwable#getCause()}. A null value is permitted, and indicates
     *                           that the cause is nonexistent or unknown.
     * @param enableSuppression  whether or not suppression is enabled or disabled
     * @param writableStackTrace whether or not the stack trace should be writable
     */
    public FhirConvertionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
