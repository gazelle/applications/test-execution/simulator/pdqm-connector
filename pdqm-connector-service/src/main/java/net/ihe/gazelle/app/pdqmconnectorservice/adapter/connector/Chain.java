package net.ihe.gazelle.app.pdqmconnectorservice.adapter.connector;

import com.gitb.ps.ProcessingService;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;
import net.ihe.gazelle.sb.api.business.DecodingException;
import net.ihe.gazelle.sb.api.business.EncodedContent;
import net.ihe.gazelle.sb.api.business.EncodingException;
import net.ihe.gazelle.sb.api.business.StandardBlock;
import net.ihe.gazelle.sb.fhir.adapter.FhirStandardBlock;
import net.ihe.gazelle.sb.fhir.adapter.HttpContent;
import net.ihe.gazelle.sb.fhir.adapter.HttpResponse;
import net.ihe.gazelle.sb.fhir.business.FhirResources;
import net.ihe.gazelle.sb.fhir.business.FhirSearchParameters;
import net.ihe.gazelle.sb.pdqm.application.PDQmStandardBlock;
import org.hl7.fhir.r4.model.OperationOutcome;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

/**
 * This class defines the chain that will run all consecutive Standard Blocks in order to feed the simulation get and to return the correct answer
 * via the Outside Gate.
 */
public class Chain {

   private static final GazelleLogger logger = GazelleLoggerFactory.getInstance().getLogger(Chain.class);

   private SimulationGate simulationGate;
   private Map<String, StandardBlock> standardBlocks = new HashMap<>();

   /**
    * Default constructor for the class. Used for injection, it defines the value of the {@link SimulationGate} of the chain.
    * @param simulationGate      value of the {@link SimulationGate} to use.
    */
   @Inject
   public Chain(SimulationGate<FhirSearchParameters, FhirResources> simulationGate) {

      this.simulationGate = simulationGate;

      standardBlocks.put("FHIR", new FhirStandardBlock());
      standardBlocks.put("PDQm", new PDQmStandardBlock());
   }

   /**
    * Constructor withoutautomatic injection used for test purposes.
    * @param patientProcessingService     value of the {@link ProcessingService} to use.
    */
   public Chain(ProcessingService patientProcessingService) {
      this.simulationGate = new SimulationGateImpl(patientProcessingService);

      standardBlocks.put("FHIR", new FhirStandardBlock());
      standardBlocks.put("PDQm", new PDQmStandardBlock());
   }

   /**
    * Runs the chain.
    * @param content    the content to decode.
    * @return the encoded response.
    */
   public EncodedContent run(HttpContent content) {
      try {
         FhirSearchParameters decoded1 = ((FhirStandardBlock) standardBlocks.get("FHIR")).decode(content);
         FhirSearchParameters decoded2 = ((PDQmStandardBlock) standardBlocks.get("PDQm")).decode(decoded1);


         FhirResources simulationResponse = (FhirResources) simulationGate.process(decoded2);


          FhirResources pdqmResponse = ((PDQmStandardBlock) standardBlocks.get("PDQm")).encode(simulationResponse);
          return ((FhirStandardBlock) standardBlocks.get("FHIR")).encode(pdqmResponse);
      } catch (DecodingException | EncodingException | SearchException e) {
         logger.warn(e, "Unexpected error while processing PDQm chain");
         return returnOperationOutcome(e);
      }
   }

   private EncodedContent returnOperationOutcome(Exception e) {
      OperationOutcome.OperationOutcomeIssueComponent issue = new OperationOutcome.OperationOutcomeIssueComponent();
      issue.setSeverity(OperationOutcome.IssueSeverity.ERROR);
      issue.setCode(OperationOutcome.IssueType.EXCEPTION);
      issue.setDiagnostics(e.getMessage());
      OperationOutcome operationOutcome = new OperationOutcome();
      operationOutcome.getIssue().add(issue);
      FhirResources errorResourceResponse = new FhirResources();
      errorResourceResponse.addResource(operationOutcome);
      try{
         return ((FhirStandardBlock) standardBlocks.get("FHIR")).encode(errorResourceResponse);
      } catch (EncodingException ee) {
         logger.error(ee,"Unexpected error preventing to return the OperationOutcome");
         return returnHttpError();
      }
   }

   private static HttpResponse returnHttpError() {
      HttpResponse httpResponse = new HttpResponse();
      httpResponse.setResponse(Response.status(500).build());
      return httpResponse;
   }
}
