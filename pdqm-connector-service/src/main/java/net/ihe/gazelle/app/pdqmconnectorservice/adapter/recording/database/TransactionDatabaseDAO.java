package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.database;

import net.ihe.gazelle.app.pdqmconnectorservice.adapter.dependencyinjection.EntityManagerProducer;
import net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model.Transaction;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 * Database access to persisted Transactions
 */
public class TransactionDatabaseDAO {

   private final EntityManager entityManager;

   /**
    * Constructor
    *
    * @param entityManager an operationnal JPA {@link EntityManager} to access underlying storage space.
    */
   @Inject
   public TransactionDatabaseDAO(@EntityManagerProducer.InjectEntityManager EntityManager entityManager) {
      this.entityManager = entityManager;
   }

   /**
    * Verify if the given transaction already exist in the storage space
    *
    * @param transactionKeyword keyword of the transaction to verify
    *
    * @return true if it is already registered in the storage space, false otherwise.
    */
   public boolean isTransactionExisting(String transactionKeyword) {
      return (Boolean) entityManager
            .createNativeQuery("SELECT CASE WHEN EXISTS (" +
                  "    SELECT id" +
                  "    FROM tf_transaction" +
                  "    where keyword = ?" +
                  ") " +
                  "THEN CAST(1 AS BIT) " +
                  "ELSE CAST(0 AS BIT) END")
            .setParameter(1, transactionKeyword)
            .getSingleResult();
   }

   /**
    * load a Transaction from the storage space by its keyword
    *
    * @param keyword keyword of the transaction to load.
    *
    * @return the transaction with the given keyword or null if not found
    */
   public Transaction getTransactionByKeyword(String keyword) {
      try {
         return entityManager
               .createQuery("SELECT t from Transaction t where t.keyword = :keyword", Transaction.class)
               .setParameter("keyword", keyword)
               .getSingleResult();
      } catch (NoResultException e) {
         return null;
      }
   }

   /**
    * Create a transaction in the persistence storage with the specified keyword.
    *
    * @param keyword keyword of the transaction
    */
   public void createTransaction(String keyword) {
      Transaction transaction = new Transaction();
      transaction.setKeyword(keyword);
      transaction.setName(keyword);
      entityManager.persist(transaction);
   }

   /**
    * Get the number of transactions registered in the persistence storage
    *
    * @return the number of transactions registered
    */
   public Long getNumberOfRegisteredTransactions() {
      return (Long) entityManager
            .createQuery("select count(t) from Transaction t")
            .getSingleResult();
   }


}
