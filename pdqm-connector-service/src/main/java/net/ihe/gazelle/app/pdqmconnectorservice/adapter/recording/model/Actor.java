package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model;

import javax.persistence.*;


/**
 * <p>Actor class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Entity
@Table(name = "tf_actor", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "keyword"))
@SequenceGenerator(name = "tf_actor_sequence", sequenceName = "tf_actor_id_seq", allocationSize = 1)
public class Actor extends TFObject implements java.io.Serializable {


    /**
     * Serial ID version of this object
     */
    private static final long serialVersionUID = -450111131541283360L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tf_actor_sequence")
    private Integer id;

    /**
     * indicates whether a SUT can create a configuration for this actor or not
     */
    @Column(name = "can_act_as_responder")
    private Boolean canActAsResponder;

    /**
     * <p>Constructor for Actor.</p>
     */
    public Actor() {
        // empty
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * <p>Setter for the field <code>canActAsResponder</code>.</p>
     *
     * @param canActAsResponder a {@link java.lang.Boolean} object.
     */
    public void setCanActAsResponder(Boolean canActAsResponder) {
        this.canActAsResponder = canActAsResponder;
    }

    /**
     * <p>Getter for the field <code>canActAsResponder</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getCanActAsResponder() {
        return canActAsResponder;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (canActAsResponder != null ? canActAsResponder.hashCode() : 0);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Actor actor = (Actor) o;
        return canActAsResponder != null ? canActAsResponder.equals(actor.canActAsResponder) : actor.canActAsResponder == null;
    }
}
