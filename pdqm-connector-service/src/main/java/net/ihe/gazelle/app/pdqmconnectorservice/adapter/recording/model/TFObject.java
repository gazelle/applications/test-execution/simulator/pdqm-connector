package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * <b>Class Description :  </b>TFOject<br><br>
 * This abstract class is intended to be extended by others classes, for the purpose of implementing
 * a technical framework object . Here is the attributes
 * <ul>
 * <li><b>keyword</b> </li>
 * <li><b>name</b> :  </li>
 * <li><b>description</b> :  </li>
 * </ul></br>
 * and two associated columns in its table. <br>
 */

@MappedSuperclass
public abstract class TFObject {

    @Column(name = "keyword", unique = true, nullable = false, length = 128)
    @NotNull
    protected String keyword;

    @Column(name = "name", length = 128)
    protected String name;

    @Column(name = "description")
    @Size(max = 2048)
    protected String description;

    /**
     * <p>Getter for the field <code>keyword</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * <p>Setter for the field <code>keyword</code>.</p>
     *
     * @param keyword a {@link java.lang.String} object.
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    /**
     * <p>Getter for the field <code>name</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getName() {
        return name;
    }

    /**
     * <p>Setter for the field <code>name</code>.</p>
     *
     * @param name a {@link java.lang.String} object.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the field <code>description</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDescription() {
        return description;
    }

    /**
     * <p>Setter for the field <code>description</code>.</p>
     *
     * @param description a {@link java.lang.String} object.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((description == null) ? 0 : description.hashCode());
        result = prime * result + ((keyword == null) ? 0 : keyword.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TFObject other = (TFObject) obj;
        if (description == null) {
            if (other.description != null) {
                return false;
            }
        } else if (!description.equals(other.description)) {
            return false;
        }
        if (keyword == null) {
            if (other.keyword != null) {
                return false;
            }
        } else if (!keyword.equals(other.keyword)) {
            return false;
        }
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }
}
