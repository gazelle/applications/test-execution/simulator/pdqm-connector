package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.database;

import net.ihe.gazelle.app.pdqmconnectorservice.adapter.dependencyinjection.EntityManagerProducer;
import net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model.Domain;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 * Database access to persisted Domains
 */
public class DomainDatabaseDAO {

   private EntityManager entityManager;

   /**
    * Constructor
    *
    * @param entityManager An operational JPA {@link EntityManager} to reach the underlying storage space.
    */
   @Inject
   public DomainDatabaseDAO(@EntityManagerProducer.InjectEntityManager EntityManager entityManager) {
      this.entityManager = entityManager;
   }

   /**
    * Verify if the given domain already exist in the storage space
    *
    * @param domainKeyword keyword of the domain to verify
    *
    * @return true if it is already registered in the storage space, false otherwise.
    */
   public boolean isDomainExisting(String domainKeyword) {
      return (Boolean) entityManager
            .createNativeQuery("SELECT CASE WHEN EXISTS (" +
                  "    SELECT id" +
                  "    FROM tf_domain" +
                  "    where keyword = ?" +
                  ") " +
                  "THEN CAST(1 AS BIT) " +
                  "ELSE CAST(0 AS BIT) END")
            .setParameter(1, domainKeyword)
            .getSingleResult();
   }

   /**
    * load a Domain from the storage space by its keyword
    *
    * @param keyword keyword of the domain to load.
    *
    * @return the domain with the given keyword or null if not found
    */
   public Domain getDomainByKeyword(String keyword) {
      try {
         return entityManager.createQuery("select d from Domain d where d.keyword = :keyword", Domain.class).setParameter("keyword", keyword)
               .getSingleResult();
      } catch (NoResultException e) {
         return null;
      }
   }

   /**
    * Create an domain in the persistence storage with the specified keyword.
    *
    * @param keyword keyword of the domain
    */
   public void createDomain(String keyword) {
      Domain domain = new Domain();
      domain.setKeyword(keyword);
      domain.setName(keyword);
      entityManager.persist(domain);
   }

   /**
    * Get the number of domains registered in the persistence storage
    *
    * @return the number of domains registered
    */
   public Long getNumberOfRegisteredDomains() {
         return (Long) entityManager
               .createQuery("select count(d) from Domain d")
               .getSingleResult();
   }
}
