package net.ihe.gazelle.app.pdqmconnectorservice.adapter.webservice;

import net.ihe.gazelle.app.pdqmconnectorservice.adapter.connector.Chain;
import net.ihe.gazelle.app.pdqmconnectorservice.adapter.connector.OutsideGate;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import net.ihe.gazelle.sb.fhir.adapter.HttpContent;
import net.ihe.gazelle.sb.fhir.adapter.HttpResponse;

import javax.inject.Inject;
import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

/**
 * Web service of the PDQm Connector. Acts as a Patient Demographic Supplier.
 */
@Provider
@Path(value="/Patient")
public class PatientSimulatorWebService implements OutsideGate {

    private static final GazelleLogger LOGGER = GazelleLoggerFactory.getInstance().getLogger(PatientSimulatorWebService.class);

    @Context
    private ServletConfig config;

    private Chain chain;

    /**
     * Default constructor for the class
     */
    public PatientSimulatorWebService() {
        //Empty constructor
    }

    /**
     * Default constructor for the class. It is used to inject the {@link Chain} used to process requests.
     * @param chain     the {@link Chain} used to process PDQm requests.
     */
    @Inject
    public PatientSimulatorWebService(Chain chain){
        this.chain = chain;
    }

    /**
     * Setter for the config property, used for test purposes.
     * @param config    value to set to the config property.
     */
    public void setConfig(ServletConfig config) {
        this.config = config;
    }

    /**
     * Get patient list from registry that correspond to PDQm parameters found in the request.
     * @param request       {@link HttpServletRequest} of the GET request.
     * @param response      {@link HttpServletResponse} to the GET request.
     * @return a {@link Response} to the HTTP GET request.
     */
    @GET
    public Response getPatientList(@Context HttpServletRequest request, @Context HttpServletResponse response) {
        return runChain(request, response);
    }

    /**
     * Get a specific patient from registry from its identifier, as specified by PDQm.
     * @param request       {@link HttpServletRequest} of the GET request.
     * @param response      {@link HttpServletResponse} to the GET request.
     * @return a {@link Response} to the HTTP GET request.
     */
    @GET
    @Path("/{id}")
    public Response getPatientById(@PathParam("id") String id, @Context HttpServletRequest request, @Context HttpServletResponse response) {
        return runChain(request, response);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Chain initChain() {
        return this.chain;
    }

    /**
     * Launch the processing of any request arriving on this service by the dedicated chain.
     * @param request       {@link HttpServletRequest} of the GET request.
     * @param response      {@link HttpServletResponse} to the GET request.
     * @return a {@link Response} to the HTTP GET request.
     */
    private Response runChain(HttpServletRequest request, HttpServletResponse response){
        HttpContent content = new HttpContent(config, request, response);
        try {
            return ((HttpResponse)chain.run(content)).getResponse();
        } catch (Exception e) {
            LOGGER.error("Unexpected Error !", e);
        }
        return null;
    }
}
