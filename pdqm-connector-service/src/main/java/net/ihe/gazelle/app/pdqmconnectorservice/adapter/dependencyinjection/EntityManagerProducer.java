package net.ihe.gazelle.app.pdqmconnectorservice.adapter.dependencyinjection;

import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;
import javax.inject.Qualifier;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Producer for EntityManager used for CDI injection.
 */
@Default
public class EntityManagerProducer {

    @Qualifier
    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.FIELD, ElementType.METHOD,
            ElementType.TYPE, ElementType.PARAMETER})
    public @interface InjectEntityManager {}

    @Produces
    @PersistenceContext(unitName = "PDQmConnectorRecordingPU")
    @InjectEntityManager
    private EntityManager entityManager;

}
