package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model;

import javax.persistence.*;


/**
 *  <b>Class Description :  </b>IntegrationProfile<br><br>
 * This class describes the Integration profile object, used by the Gazelle application. This class belongs to the Technical Framework module.
 *
 * IntegrationProfile possesses the following attributes :
 * <ul>
 * <li><b>id</b> : id corresponding to the Integration profile</li>
 * <li><b>keyword</b> : keyword corresponding to the Integration profile</li>
 * <li><b>name</b> : name corresponding to the Integration profile</li>
 * <li><b>description</b> : description corresponding to the Integration profile</li>
 * <li><b>integrationProfileType</b> : type corresponding to the Integration profile</li>
 * </ul></br>
 * <b>Example of Integration profile</b> : "ARI : Access to Radiology Information" is an Integration profile<br>
 */


@Entity
@Table(name = "tf_integration_profile", schema = "public",
        uniqueConstraints = @UniqueConstraint(columnNames = "keyword"))
@SequenceGenerator(name = "tf_integration_profile_sequence", sequenceName = "tf_integration_profile_id_seq", allocationSize=1)
public class IntegrationProfile extends TFObject implements java.io.Serializable {


    /** Serial ID version of this object */
    private static final long serialVersionUID = -450911131561283760L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="tf_integration_profile_sequence")
    private Integer id;

    /**
     * <p>Constructor for IntegrationProfile.</p>
     */
    public IntegrationProfile() {
        // empty
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
}
