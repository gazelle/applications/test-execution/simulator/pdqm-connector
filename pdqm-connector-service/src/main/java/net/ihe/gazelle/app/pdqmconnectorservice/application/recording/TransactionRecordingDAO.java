package net.ihe.gazelle.app.pdqmconnectorservice.application.recording;

import net.ihe.gazelle.app.pdqmconnectorservice.business.recording.RecordingException;
import net.ihe.gazelle.app.pdqmconnectorservice.business.recording.TransactionInstance;

/**
 * Data Access Object to record transactions and messages.
 */
public interface TransactionRecordingDAO {

   /**
    * Save the intercepted transaction instance
    *
    * @param transactionInstance transaction instance to record in Simulator logs
    */
   void saveTransaction(TransactionInstance transactionInstance) throws RecordingException;

   /**
    * Is the given domain already existing in the storage space ?
    *
    * @param domainKeyword keyword of the domain to look for
    *
    * @return true if it is already registered, false otherwise
    */
   boolean isDomainExisting(String domainKeyword) throws RecordingException;

   /**
    * Is the given transaction already existing in the storage space ?
    *
    * @param transactionKeyword keyword of the transaction to look for
    *
    * @return true if it is already registered, false otherwise
    */
   boolean isTransactionExisting(String transactionKeyword) throws RecordingException;

   /**
    * Is the given actor already existing in the storage space ?
    *
    * @param actorKeyword keyword of the actor to look for
    *
    * @return true if it is already registered, false otherwise
    */
   boolean isActorExisting(String actorKeyword) throws RecordingException;

   /**
    * Create a new domain entry in the storage space
    *
    * @param domainKeyword Keyword of the domain
    */
   void createDomain(String domainKeyword) throws RecordingException;

   /**
    * Create a new transaction entry in the storage space
    *
    * @param transactionKeyword Keyword of the domain
    */
   void createTransaction(String transactionKeyword) throws RecordingException;

   /**
    * Create a new actor entry in the storage space
    *
    * @param actorKeyword keyword of the actor
    */
   void createActor(String actorKeyword) throws RecordingException;
}
