package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.interceptor;

import ca.uhn.fhir.rest.api.EncodingEnum;
import ca.uhn.fhir.rest.api.RestOperationTypeEnum;
import net.ihe.gazelle.app.pdqmconnectorservice.business.PDQm;
import net.ihe.gazelle.app.pdqmconnectorservice.business.recording.EStandardBusiness;
import net.ihe.gazelle.app.pdqmconnectorservice.business.recording.MessageInstance;
import net.ihe.gazelle.app.pdqmconnectorservice.business.recording.MessageInstanceMetadata;
import net.ihe.gazelle.app.pdqmconnectorservice.business.recording.TransactionInstance;
import net.ihe.gazelle.fhir.constants.FhirConstants;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;

/**
 * Transform RequestContext and ResponseContext as transaction instance for PDQm
 */
public class PDQmTransactionInstanceMapper {

   /**
    * Generate a Business object TransactionInstanceBusiness from what is given by the interceptor.
    *
    * @param requestContext  : context for the request, of type {@link ContainerRequestContext}
    * @param responseContext : context for the response, of type {@link ContainerResponseContext}
    * @param servletRequest  : servlet context of the transaction of type {@link HttpServletRequest}
    */
   public TransactionInstance getTransactionInstanceFromRequestResponseContext(ContainerRequestContext requestContext,
                                                                               ContainerResponseContext responseContext,
                                                                               HttpServletRequest servletRequest) {
      TransactionInstance transactionInstance = new TransactionInstance();
      transactionInstance.setDomainKeyword(PDQm.DOMAIN.getKeyword());
      transactionInstance.setSimulatedActorKeyword(PDQm.Actors.PDS.getKeyword());

      transactionInstance.setRequest(generateRequestMessageInstancefromContext(requestContext, servletRequest));
      transactionInstance.setResponse(generateResponseMessageInstancefromContext(responseContext, requestContext));
      transactionInstance.setStandard(getStandard(servletRequest));
      transactionInstance.setTimestamp(new Date());
      transactionInstance.setTransactionKeyword(PDQm.Transactions.ITI_78.getKeyword());
      return transactionInstance;
   }

   /**
    * Generates the MessageInstance object corresponding to the request recieved by the simulator.
    *
    * @param requestContext : context for the request, of type {@link ContainerRequestContext}
    * @param servletRequest : servlet context of the transaction of type {@link HttpServletRequest}
    */
   private MessageInstance generateRequestMessageInstancefromContext(ContainerRequestContext requestContext, HttpServletRequest servletRequest) {
      MessageInstance requestMessageInstance = new MessageInstance();
      requestMessageInstance.setIssuer(servletRequest.getRemoteHost());
      requestMessageInstance.setIssuerIpAddress(servletRequest.getRemoteAddr());
      requestMessageInstance.setIssuingActorKeyword(PDQm.Actors.PDC.getKeyword());

      requestMessageInstance.setContent(requestContext.getUriInfo().getRequestUri().toString().getBytes(StandardCharsets.UTF_8));

      requestMessageInstance.setType(getRequestType(requestContext));
      getRequestMetadata(servletRequest, requestMessageInstance.getMetadataList());
      return requestMessageInstance;
   }

   /**
    * Generates the MessageInstance object corresponding to the request recieved by the simulator.
    *
    * @param responseContext context for the response
    * @param requestContext  context for the request
    */
   private MessageInstance generateResponseMessageInstancefromContext(ContainerResponseContext responseContext,
                                                                      ContainerRequestContext requestContext) {
      MessageInstance responseMessageInstance = new MessageInstance();
      responseMessageInstance.setIssuer(PatientManagerConstants.ISSUER_DEFAULT_NAME);
      responseMessageInstance.setIssuingActorKeyword(PDQm.Actors.PDS.getKeyword());
      if (responseContext.getEntity() != null) {
         responseMessageInstance.setContent(responseContext.getEntity().toString().getBytes(StandardCharsets.UTF_8));
      } else {
         responseMessageInstance.setContent("Response is missing".getBytes(StandardCharsets.UTF_8));
      }
      responseMessageInstance.setType(getResponseType(requestContext));
      return responseMessageInstance;
   }

   /**
    * Extract the format of the transaction based on the servlet request. First tries to check _format parameter. If it does not exists, it will check
    * Accept header and try to extract excepted format.
    *
    * @param servletRequest : servlet context of the transaction of type {@link HttpServletRequest}
    */
   private EStandardBusiness getStandard(HttpServletRequest servletRequest) {
      EncodingEnum format = getFormatFromParameter(servletRequest);
      if (format == null) {
         format = getFormatFromHeader(servletRequest);
      }
       if (EncodingEnum.JSON.equals(format)) {
           return EStandardBusiness.FHIR_JSON;
       } else if (EncodingEnum.XML.equals(format)) {
           return EStandardBusiness.FHIR_XML;
       } else {
           return EStandardBusiness.FHIR_XML;
       }
   }

   /**
    * Get Message format from request parameters
    *
    * @param servletRequest the HTTP servlet request
    *
    * @return XML, JSON, RDF ({@link EncodingEnum}) or null if not defined nor known
    */
   private EncodingEnum getFormatFromParameter(HttpServletRequest servletRequest) {
      String[] parameterFormatValues = servletRequest.getParameterValues("_format");
      if (parameterFormatValues != null && parameterFormatValues.length > 0) {
         for (String format : parameterFormatValues) {
            if (format != null && EncodingEnum.JSON.equals(EncodingEnum.forContentType(format))) {
               return EncodingEnum.JSON;
            }
         }
         return EncodingEnum.XML;
      }
      return null;
   }

   /**
    * Get Message format from HTTP header of the request
    *
    * @param servletRequest the HTTP servlet request
    *
    * @return XML, JSON, RDF ({@link EncodingEnum}) or null if not defined nor known
    */
   private EncodingEnum getFormatFromHeader(HttpServletRequest servletRequest) {
      EncodingEnum encoding = null;
      String headerAcceptValue = servletRequest.getHeader("Accept");
      if (headerAcceptValue != null) {
         String[] acceptParts = headerAcceptValue.split(";");
         int i = 0;
         while ((encoding == null || EncodingEnum.RDF.equals(encoding)) && i < acceptParts.length) {
            encoding = EncodingEnum.forContentType(acceptParts[i].trim());
            i++;
         }
      }
      return encoding;
   }


   /**
    * Extract Metadata from request header.
    *
    * @param servletRequest : servlet context of the transaction of type {@link HttpServletRequest}
    */
   private void getRequestMetadata(HttpServletRequest servletRequest, List<MessageInstanceMetadata> metadataList) {
      String acceptHeader = servletRequest.getHeader("Accept");
      if (acceptHeader != null) {
         MessageInstanceMetadata requestMetadata = new MessageInstanceMetadata();
         requestMetadata.setLabel("Accept header");
         requestMetadata.setValue(acceptHeader);
         metadataList.add(requestMetadata);
      }
   }

   /**
    * Returns the type of the request performed based on what rest operation was performed
    *
    * @param requestContext : request context {@link RestOperationTypeEnum}
    */
   private String getRequestType(ContainerRequestContext requestContext) {
      if ("GET".equals(requestContext.getMethod()) && requestContext.getUriInfo().getPath().matches(".*Patient/[A-Za-z0-9\\-]+")) {
         return FhirConstants.PDQM_RETRIEVE_REQUEST_TYPE;
      } else if ("GET".equals(requestContext.getMethod()) && requestContext.getUriInfo().getPath().matches(".*Patient")) {
         return FhirConstants.PDQM_REQUEST_TYPE;
      }
      return "unknown";
   }

   /**
    * Returns the type of the response performed based on the request type.
    *
    * @param requestContext : request context {@link RestOperationTypeEnum}
    */
   private String getResponseType(ContainerRequestContext requestContext) {
      String requestType = getRequestType(requestContext);
      if (requestType.equals(FhirConstants.PDQM_RETRIEVE_REQUEST_TYPE)) {
         return FhirConstants.PDQM_RETRIEVE_RESPONSE;
      } else if (requestType.equals(FhirConstants.PDQM_REQUEST_TYPE)) {
         return FhirConstants.PDQM_RESPONSE;
      }
      return "unknown";
   }
}
