package net.ihe.gazelle.app.pdqmconnectorservice.application.recording;

import net.ihe.gazelle.app.pdqmconnectorservice.business.recording.RecordingException;
import net.ihe.gazelle.app.pdqmconnectorservice.business.recording.TransactionInstance;
import net.ihe.gazelle.app.pdqmconnectorservice.business.recording.TransactionRecordingService;

import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.transaction.Transactional;

/**
 * Implements a record transaction service. Create non existing domain, transaction and actors if necessary.
 */
@Default
public class TransactionRecordingServiceImpl implements TransactionRecordingService {

   private TransactionRecordingDAO transactionRecordingDAO;

   /**
    * Constructor
    *
    * @param transactionRecordingDAO underlying data storage access for TransactionRecording
    */
   @Inject
   public TransactionRecordingServiceImpl(TransactionRecordingDAO transactionRecordingDAO) {
      this.transactionRecordingDAO = transactionRecordingDAO;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   @Transactional
   public void recordTransaction(TransactionInstance transactionInstance) throws RecordingException {
      if (!transactionRecordingDAO.isDomainExisting(transactionInstance.getDomainKeyword())) {
         transactionRecordingDAO.createDomain(transactionInstance.getDomainKeyword());
      }

      if (!transactionRecordingDAO.isTransactionExisting(transactionInstance.getTransactionKeyword())) {
         transactionRecordingDAO.createTransaction(transactionInstance.getTransactionKeyword());
      }
      if (!transactionRecordingDAO.isActorExisting(transactionInstance.getRequest().getIssuingActorKeyword())) {
         transactionRecordingDAO.createActor(transactionInstance.getRequest().getIssuingActorKeyword());
      }
      if (!transactionRecordingDAO.isActorExisting(transactionInstance.getResponse().getIssuingActorKeyword())) {
         transactionRecordingDAO.createActor(transactionInstance.getResponse().getIssuingActorKeyword());
      }
      this.transactionRecordingDAO.saveTransaction(transactionInstance);
   }
}
