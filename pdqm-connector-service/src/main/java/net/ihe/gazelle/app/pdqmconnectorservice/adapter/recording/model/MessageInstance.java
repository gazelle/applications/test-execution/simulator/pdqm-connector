package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.model;

import javax.persistence.*;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * <b>Class description</b>: MessageInstance
 *
 * This class is used to store the request/response sent/received in an instance of a transaction between a SUT and the simulator
 *
 * @author 	Anne-Gaëlle Bergé / IHE Europe
 */

@Entity
@Table(name="cmn_message_instance", schema="public", uniqueConstraints=@UniqueConstraint(columnNames="id"))
@SequenceGenerator(name="cmn_message_instance_sequence", sequenceName="cmn_message_instance_id_seq", allocationSize=1)
public class MessageInstance implements Serializable{

    /**
     *
     */
    private static final long serialVersionUID = -8457580323530563536L;

    @Id
    @GeneratedValue(generator="cmn_message_instance_sequence", strategy=GenerationType.SEQUENCE)
    @Column(name="id", nullable=false, unique=true)
    private Integer id;

    @JoinColumn(name="issuing_actor")
    @ManyToOne(fetch=FetchType.EAGER)
    private Actor issuingActor;

    @Column(name="validation_status")
    private String validationStatus;

    @Column(name="validation_detailed_result")
    private byte[] validationDetailedResult;

    @Column(name="content")
    private byte[] content;

    @Column(name="type")
    private String type;

    @Column(name="issuer")
    private String issuer;

    @Column(name = "issuer_ip_address")
    private String issuerIpAddress;

    @OneToMany(targetEntity = MessageInstanceMetadata.class, mappedBy = "messageInstance")
    private List<MessageInstanceMetadata> metadataList;

    /**
     * <p>Constructor for MessageInstance.</p>
     */
    public MessageInstance() {
        // Empty Constructor.
    }

    public MessageInstance(String issuingActorKeyword, byte[] content, String type, String issuer, String issuerIpAddress) {
        Actor actor = new Actor();
        actor.setKeyword(issuingActorKeyword);
        this.setIssuingActor(actor);
        this.setContent(content);
        this.setType(type);
        this.setIssuer(issuer);
        this.setIssuerIpAddress(issuerIpAddress);
        this.setValidationDetailedResult(content);
        this.setMetadataList(new ArrayList<>());
        this.setValidationStatus("");
    }

    /**
     * <p>Getter for the field <code>metadataList</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<MessageInstanceMetadata> getMetadataList() {
        return metadataList;
    }

    /**
     * <p>Setter for the field <code>metadataList</code>.</p>
     *
     * @param metadataList a {@link java.util.List} object.
     */
    public void setMetadataList(List<MessageInstanceMetadata> metadataList) {
        this.metadataList = metadataList;
    }

    /**
     * <p>Getter for the field <code>issuingActor</code>.</p>
     *
     * @return a {@link Actor} object.
     */
    public Actor getIssuingActor() {
        return issuingActor;
    }

    /**
     * <p>Setter for the field <code>issuingActor</code>.</p>
     *
     * @param issuingActor a {@link Actor} object.
     */
    public void setIssuingActor(Actor issuingActor) {
        this.issuingActor = issuingActor;
    }

    /**
     * <p>Getter for the field <code>validationStatus</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getValidationStatus() {
        return validationStatus;
    }

    /**
     * <p>Setter for the field <code>validationStatus</code>.</p>
     *
     * @param validationStatus a {@link java.lang.String} object.
     */
    public void setValidationStatus(String validationStatus) {
        this.validationStatus = validationStatus;
    }

    /**
     * <p>Getter for the field <code>validationDetailedResult</code>.</p>
     *
     * @return an array of byte.
     */
    public byte[] getValidationDetailedResult() {
        return validationDetailedResult;
    }

    /**
     * <p>Setter for the field <code>validationDetailedResult</code>.</p>
     *
     * @param validationDetailedResult an array of byte.
     */
    public void setValidationDetailedResult(byte[] validationDetailedResult) {
        if (validationDetailedResult != null) {
            this.validationDetailedResult = validationDetailedResult.clone();
        } else {
            this.validationDetailedResult = null;
        }
    }

    /**
     * <p>Getter for the field <code>content</code>.</p>
     *
     * @return an array of byte.
     */
    public byte[] getContent()
    {
        return this.content;
    }


    /**
     * <p>Setter for the field <code>content</code>.</p>
     *
     * @param content an array of byte.
     */
    public void setContent(byte[] content) {
        if (content != null) {
            this.content = content.clone();
        } else {
            this.content = null;
        }
    }

    /**
     * <p>Setter for the field <code>content</code>.</p>
     *
     * @param content a {@link java.lang.String} object.
     */
    public void setContent(String content) {
        if (content == null) {
            this.content = null;
        }
        else {
            setContent(content.getBytes(StandardCharsets.UTF_8));
        }
    }

    /**
     * <p>Getter for the field <code>type</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getType() {
        return type;
    }

    /**
     * <p>Setter for the field <code>type</code>.</p>
     *
     * @param type a {@link java.lang.String} object.
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * <p>Getter for the field <code>issuer</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getIssuer() {
        return issuer;
    }

    /**
     * <p>Setter for the field <code>issuer</code>.</p>
     *
     * @param issuer a {@link java.lang.String} object.
     */
    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getId() {
        return id;
    }

    public String getIssuerIpAddress() {
        return issuerIpAddress;
    }

    public void setIssuerIpAddress(String issuerIpAddress) {
        this.issuerIpAddress = issuerIpAddress;
    }

}
