package net.ihe.gazelle.app.pdqmconnectorservice.adapter.connector;

import net.ihe.gazelle.app.patientregistryapi.business.*;
import net.ihe.gazelle.fhir.resources.PatientFhirIHE;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.r4.model.Enumerations;
import org.hl7.fhir.r4.model.HumanName;

import java.util.List;

/**
 * Converter that is able to transform {@link Patient} to {@link PatientFhirIHE}.
 */
public class BusinessToFhirConverter {

    private static final String URN_PREFIX = "urn:oid:";

    /**
     * Private constructor to override the implicit one.
     */
    private BusinessToFhirConverter() {
    }

    /**
     * Map a {@link Patient} to a {@link PatientFhirIHE}.
     *
     * @param patient {@link Patient} to map.
     * @return the mapped {@link PatientFhirIHE}.
     * @throws FhirConvertionException if cannot convert object to business.
     */
    public static PatientFhirIHE patientToFhirResource(Patient patient) throws FhirConvertionException {
        PatientFhirIHE fhirPatient = new PatientFhirIHE();
        fhirPatient.setId(patient.getUuid());
        setNames(fhirPatient, patient);
        fhirPatient.setGender(getFhirGenderCodeFromPatientDb(patient.getGender()));
        setCrossIdentifier(fhirPatient, patient);
        setBirthDate(fhirPatient, patient);
        setAddresses(fhirPatient, patient);
        setTelecom(fhirPatient, patient);
        fhirPatient.setActive(patient.isActive());
        return fhirPatient;
    }

    /**
     * Map {@link Patient#getContactPoints()} to {@link PatientFhirIHE#getTelecom()}.
     *
     * @param fhirPatient target {@link PatientFhirIHE}.
     * @param patient     {@link Patient} to map.
     * @throws FhirConvertionException if cannot convert object to business.
     */
    private static void setTelecom(PatientFhirIHE fhirPatient, Patient patient) throws FhirConvertionException {
        List<ContactPoint> contactPoints = patient.getContactPoints();
        if (contactPoints != null) {
            for (ContactPoint contactPoint : contactPoints) {
                if (contactPoint != null) {
                    org.hl7.fhir.r4.model.ContactPoint fhirContactPoint = new org.hl7.fhir.r4.model.ContactPoint();
                    fhirContactPoint.setSystem(getContactPointSystem(contactPoint.getType()));
                    fhirContactPoint.setValue(contactPoint.getValue());
                    fhirContactPoint.setUse(getContactPointUse(contactPoint.getUse()));
                    fhirPatient.addTelecom(fhirContactPoint);
                }
            }
        }
    }

    /**
     * Map {@link Patient#getAddresses()} to {@link PatientFhirIHE#getAddress()}.
     *
     * @param fhirPatient target {@link PatientFhirIHE}.
     * @param patient     {@link Patient} to map.
     * @throws FhirConvertionException if cannot convert object to business.
     */
    private static void setAddresses(PatientFhirIHE fhirPatient, Patient patient) throws FhirConvertionException {
        List<Address> addressList = patient.getAddresses();
        if (addressList != null) {
            for (Address address : addressList) {
                org.hl7.fhir.r4.model.Address fhirAddress = new org.hl7.fhir.r4.model.Address();
                for (String line : address.getLines()) {
                    fhirAddress.addLine(line);
                }
                fhirAddress.setCity(address.getCity());
                fhirAddress.setCountry(address.getCountryIso3());
                fhirAddress.setPostalCode(address.getPostalCode());
                fhirAddress.setState(address.getState());
                fhirAddress.setUse(getAddressUse(address.getUse()));
                fhirPatient.addAddress(fhirAddress);
            }
        }
    }

    /**
     * Map {@link Patient#getDateOfBirth()} to {@link PatientFhirIHE#getBirthDate()}.
     *
     * @param fhirPatient target {@link PatientFhirIHE}.
     * @param patient     {@link Patient} to map.
     */
    private static void setBirthDate(PatientFhirIHE fhirPatient, Patient patient) {
        if (patient.getDateOfBirth() != null) {
            fhirPatient.setBirthDate(patient.getDateOfBirth());
        }
    }

    /**
     * Map {@link Patient#getIdentifiers()} to {@link PatientFhirIHE}.
     *
     * @param fhirPatient target {@link PatientFhirIHE}.
     * @param patient     {@link Patient} to map.
     */
    private static void setCrossIdentifier(PatientFhirIHE fhirPatient, Patient patient) {
        if (patient.getIdentifiers() != null) {
            for (EntityIdentifier currentPatientIdentifier : patient.getIdentifiers()) {
                if (currentPatientIdentifier.getSystemIdentifier() != null) {
                    String fhirSystem = getUniversalIDAsUrn(currentPatientIdentifier.getSystemIdentifier());
                    fhirPatient.addIdentifier().setSystem(fhirSystem).setValue(currentPatientIdentifier.getValue());
                }
            }
        }
    }

    /**
     * Map {@link Patient#getNames()} to {@link PatientFhirIHE#getName()}.
     *
     * @param fhirPatient target {@link PatientFhirIHE}.
     * @param patient     {@link Patient} to map.
     * @throws FhirConvertionException if cannot convert object to business.
     */
    private static void setNames(PatientFhirIHE fhirPatient, Patient patient) throws FhirConvertionException {
        List<PersonName> patientNames = patient.getNames();
        if (patientNames != null) {
            for (PersonName personName : patientNames) {
                if (personName != null) {
                    HumanName name = new HumanName();
                    name.setFamily(personName.getFamily());
                    for (String given : personName.getGivens()) {
                        name.addGiven(given);
                    }
                    try {
                        name.setUse(HumanName.NameUse.fromCode(personName.getUse()));
                    } catch (FHIRException e) {
                        throw new FhirConvertionException(String.format("Cannot convert PersonName use : %s", personName.getUse()), e);
                    }
                    name.addPrefix(personName.getPrefix());
                    name.addSuffix(personName.getSuffix());
                    fhirPatient.addName(name);
                }
            }
        }
    }

    /**
     * Map Gazelle business {@link GenderCode} to FHIR {@link org.hl7.fhir.r4.model.Enumerations.AdministrativeGender}
     *
     * @param genderCode {@link GenderCode} to map.
     * @return mapped {@link org.hl7.fhir.r4.model.Enumerations.AdministrativeGender}
     * @throws FhirConvertionException if cannot convert object to business.
     */
    private static Enumerations.AdministrativeGender getFhirGenderCodeFromPatientDb(GenderCode genderCode) throws FhirConvertionException {
        if (genderCode != null) {
            switch (genderCode) {
                case MALE:
                    return Enumerations.AdministrativeGender.MALE;
                case FEMALE:
                    return Enumerations.AdministrativeGender.FEMALE;
                case UNDEFINED:
                    return Enumerations.AdministrativeGender.UNKNOWN;
                case OTHER:
                    return Enumerations.AdministrativeGender.OTHER;
                default:
                    throw new FhirConvertionException(String.format("Cannot map GenderCode : %s", genderCode));
            }
        }
        return null;
    }

    /**
     * Transform the literal value of a Universal ID to a valid URN.
     *
     * @param universalID Universal ID to transform to URN.
     * @return the literal value of the URN.
     */
    private static String getUniversalIDAsUrn(String universalID) {
        if (universalID == null) {
            return null;
        } else if (universalID.startsWith(URN_PREFIX)) {
            return universalID;
        } else {
            return URN_PREFIX + universalID;
        }
    }

    /**
     * Transform the Gazelle {@link AddressUse} to a FHIR {@link org.hl7.fhir.r4.model.Address.AddressUse}
     *
     * @param addressUse {@link AddressUse} to map.
     * @return mapped {@link org.hl7.fhir.r4.model.Address.AddressUse}
     * @throws FhirConvertionException if cannot convert object to business.
     */
    private static org.hl7.fhir.r4.model.Address.AddressUse getAddressUse(AddressUse addressUse) throws FhirConvertionException {
        if (addressUse != null) {
            switch (addressUse) {
                case HOME:
                case PRIMARY_HOME:
                    return org.hl7.fhir.r4.model.Address.AddressUse.HOME;
                case WORK:
                    return org.hl7.fhir.r4.model.Address.AddressUse.WORK;
                case VACATION_HOME:
                case TEMPORARY:
                    return org.hl7.fhir.r4.model.Address.AddressUse.TEMP;
                case BAD:
                    return org.hl7.fhir.r4.model.Address.AddressUse.OLD;
                case BILLING:
                    return org.hl7.fhir.r4.model.Address.AddressUse.BILLING;
                default:
                    throw new FhirConvertionException(String.format("Cannot map AddressUse : %s", addressUse));
            }
        }
        return null;
    }

    /**
     * Transform the Gazelle {@link ContactPointUse} to a FHIR {@link org.hl7.fhir.r4.model.ContactPoint.ContactPointUse}
     *
     * @param contactPointUse {@link ContactPointUse} to map.
     * @return mapped {@link org.hl7.fhir.r4.model.ContactPoint.ContactPointUse}
     */
    private static org.hl7.fhir.r4.model.ContactPoint.ContactPointUse getContactPointUse(ContactPointUse contactPointUse) {
        if (contactPointUse != null) {
            switch (contactPointUse) {
                case HOME:
                case PRIMARY_HOME:
                    return org.hl7.fhir.r4.model.ContactPoint.ContactPointUse.HOME;
                case MOBILE:
                    return org.hl7.fhir.r4.model.ContactPoint.ContactPointUse.MOBILE;
                case WORK:
                    return org.hl7.fhir.r4.model.ContactPoint.ContactPointUse.WORK;
                case TEMPORARY:
                    return org.hl7.fhir.r4.model.ContactPoint.ContactPointUse.TEMP;
                default:
                    return null;
            }
        }
        return null;
    }

    /**
     * Transform the Gazelle {@link ContactPointType} to a FHIR {@link org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem}
     *
     * @param contactPointType {@link ContactPointType} to map.
     * @return mapped {@link org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem}
     * @throws FhirConvertionException if cannot convert object to business.
     */
    private static org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem getContactPointSystem(ContactPointType contactPointType)
            throws FhirConvertionException {
        if (contactPointType != null) {
            switch (contactPointType) {
                case BEEPER:
                    return org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.PAGER;
                case PHONE:
                    return org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.PHONE;
                case FAX:
                    return org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.FAX;
                case URL:
                    return org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.URL;
                case EMAIL:
                    return org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.EMAIL;
                case SMS:
                    return org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.SMS;
                case OTHER:
                    return org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.OTHER;
                default:
                    throw new FhirConvertionException(String.format("Cannot map ContactPointType : %s", contactPointType));
            }
        }
        return null;
    }
}
