package net.ihe.gazelle.app.pdqmconnectorservice.business.recording;

/**
 * Key-Value structure to store metadata of message instances. As example, can be used to store HTTP header.
 *
 * @see MessageInstance
 */
public class MessageInstanceMetadata {
   private String label;
   private String value;

   /**
    * <p>Constructor for MessageInstanceMetadata.</p>
    */
   public MessageInstanceMetadata() {

   }

   /**
    * <p>Constructor for MessageInstanceMetadata.</p>
    *
    * @param label a {@link java.lang.String} object.
    * @param value a {@link java.lang.String} object.
    */
   public MessageInstanceMetadata(String label, String value) {
      this.setLabel(label);
      this.setValue(value);
   }

   /**
    * Get the label (key)
    *
    * @return the label
    */
   public String getLabel() {
      return label;
   }

   /**
    * Set the label (key)
    *
    * @param label the label
    */
   public void setLabel(String label) {
      this.label = label;
   }

   /**
    * Get the value
    *
    * @return the value as String
    */
   public String getValue() {
      return value;
   }

   /**
    * Set the value
    *
    * @param value the value as string
    */
   public void setValue(String value) {
      this.value = value;
   }
}
