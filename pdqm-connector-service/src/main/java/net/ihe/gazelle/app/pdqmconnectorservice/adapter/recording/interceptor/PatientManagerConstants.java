package net.ihe.gazelle.app.pdqmconnectorservice.adapter.recording.interceptor;

import net.ihe.gazelle.lib.annotations.Package;

/**
 * Constants used in Patient Manager for consistancy in the recording database.
 */
@Package
final class PatientManagerConstants {

   @Package
   static final String ISSUER_DEFAULT_NAME = "PatientManager";

   private PatientManagerConstants() {
   }
}
