# PDQm Stories and scenario

## IHE Use cases

2 general use cases
* A system must obtain patient demographics to populate a user interface via a known demographic field or a search parameters provided by a user.
* A prerequisite step whereby a system must obtain a identifier from another patient ID domain in order to complete another workflow.

### Use case #1 Patient Information for bed assigning

An admitted patient must be assigned to a bed (the patient is already registered in the PMIS). The patient may or may not be able to provide positive ID information.  The Nurse need to query for patient identity information to establish the relation between the bed and the patient using his/her device.

The Search criteria entered by the nurse might include one or more of the following:
* Partial or complete name (printed on the record)
* Patient ID (obtain from a bar code or record)
* Patient ID entry or scan
* Date of birth / age range

The system return a list of patient showing Patient ID, full name, age, sex etc. The nurse will select the appropriate entry to associate the patient with the bed equipement.

### Use case #2 Patient visits for the first time in Physician office

The nurse must register the patient in the local practice management information system (PMIS). The office is connected to a hospital enterprise's central patient registry (acting as PDS). The nurse will issue a query to the central patient registry using given patient basic demographics data. In the return patient list, she picks an appropriate record including the hospital's patient ID to enter in the PMIS. The local PMIs will uses a different Patient ID domain than the central patient registry.

### Uses cases #3 Patient Demographics Query with multiple PatientID domains

A lab technician enters some basic demographis data into lab application to query a patient demographics supplier to identify a patient for his lab exams. As the application also needs the patient identifier in another Patient ID Domain in the enterprise for results delivery, the application is configured to query for Patient IDs from other domains in the query response.

## Translated Epics for Gherkins input

PDQm connector Feature value : High

| feature           | Relative feature value  |
|-------------------|-------------------------|
| Patient Search    | High                    |
| Patient Retrieve  | Essential               |

PDQm Search criteria Attributes


| criteria           | feature value    |
|--------------------|------------------|
| id                 | low              |
| active             | medium           |
| identifier         | Essential        |
| family             | Essential        |
| given              | Essential        |
| birthdate          | high             |
| telecom            | low              |
| address            | medium           |
| address-city       | medium           |
| address-country    | medium           |
| address-postalcode | medium           |
| address-state      | medium           |
| gender             | high             |


| technical aspect              | value                                                     |
|-------------------------------|-----------------------------------------------------------|
| Single citeria                | Essential, higher than multiple                           |
| multiple criteria             | lower than single                                         |
| multiple AND                  | higher than OR                                            |
| multiple OR                   | lower than AND                                            |
| modifiers                     | higher than multiple criteria, lower than single criteria |
| modifier String startswith    | essential, same as single criteria                        |
| modifier String exact         | lower than startswith                                     |
| modifier String contains      | lower than exact                                          |









