package net.ihe.gazelle.sb.fhir.adapter;

import net.ihe.gazelle.sb.fhir.adapter.hapi.HapiCorrectorService;
import net.ihe.gazelle.sb.fhir.business.FhirResources;
import net.ihe.gazelle.sb.fhir.business.FhirSearchParameters;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class HapiCorrectorServiceTestModel extends HapiCorrectorService {

    FhirSearchParameters parameters = new FhirSearchParameters();

    Response response = Response.status(Response.Status.OK).build();

    HapiCorrectorServiceTestModel(FhirSearchParameters parameters) {
        this.parameters = parameters;
    }

    HapiCorrectorServiceTestModel(Response response) {
        this.response = response;
    }

    @Override
    public Response correctHapi(HttpServletRequest request, HttpServletResponse response, ServletConfig config, CompletableFuture<FhirSearchParameters> completableFutureParams, CompletableFuture<FhirResources> completableFutureResources) {
        completableFutureParams.complete(this.parameters);
        try {
            completableFutureResources.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return this.response;
    }
}
