package net.ihe.gazelle.sb.fhir.adapter.hapi;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;

public class CustomHttpServletResponseWrapper extends HttpServletResponseWrapper {

    ByteArrayOutputStream outputstream = new ByteArrayOutputStream(1024);
    ServletOutputStreamAdapter servletOutputstream = new ServletOutputStreamAdapter(outputstream);
    PrintWriter writer = new PrintWriter(new OutputStreamWriter(outputstream, StandardCharsets.UTF_8));

    public CustomHttpServletResponseWrapper(HttpServletResponse response) {
        super(response);
    }

    @Override
    public ServletOutputStream getOutputStream() throws IOException {
        return this.servletOutputstream;
    }

    public ByteArrayOutputStream getCustomOutputStream() {
        return this.outputstream;
    }

    @Override
    public PrintWriter getWriter() {
        return this.writer;
    }

    private static class ServletOutputStreamAdapter extends ServletOutputStream {
        private OutputStream delegate;

        ServletOutputStreamAdapter(OutputStream delegate) {
            this.delegate = delegate;
        }

        public void write(int b) throws IOException {
            this.delegate.write(b);
        }

        @Override
        public void flush() throws IOException {
            this.delegate.flush();
        }

        public boolean isReady() {
            throw new UnsupportedOperationException();
        }

        public void setWriteListener(WriteListener arg0) {
            throw new UnsupportedOperationException();
        }
    }

}
