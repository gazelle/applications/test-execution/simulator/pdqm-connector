package net.ihe.gazelle.sb.fhir.adapter.hapi;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.api.EncodingEnum;
import ca.uhn.fhir.rest.server.FifoMemoryPagingProvider;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.IncomingRequestAddressStrategy;
import ca.uhn.fhir.rest.server.RestfulServer;
import net.ihe.gazelle.sb.fhir.business.FhirResources;
import net.ihe.gazelle.sb.fhir.business.FhirSearchParameters;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Created by xfs on 20/04/16.
 *
 * @author aberge
 * @version $Id: $Id
 */
@WebServlet(urlPatterns = "/hapi/*", name = "uppercaseServlet")
public class PatientManagerFhirServer extends RestfulServer {

    private static final long serialVersionUID = 1L;
    /**
     * Constant <code>DEFAULT_PAGE_SIZE=10</code>
     */
    private static final int DEFAULT_PAGE_SIZE = 10;
    /**{
			method = RequestTypeEnum.valueOf(theReq.getMethod());
		}
     * Constant <code>MAXIMUM_PAGE_SIZE=100</code>
     */
    private static final int MAXIMUM_PAGE_SIZE = 100;

    private CompletableFuture<FhirSearchParameters> completableFutureParams;

    private CompletableFuture<FhirResources> completableFutureResources;

    /**
     * constructor to deal with async
     * @param config the servlet config
     * @param completableFutureParams the param to send
     * @param completableFutureResources the resources to receive
     * @throws ServletException if there is an initialization problem
     */
    public PatientManagerFhirServer(ServletConfig config, CompletableFuture<FhirSearchParameters> completableFutureParams,
                                    CompletableFuture<FhirResources> completableFutureResources) throws ServletException {
        super(FhirContext.forR4());
        this.completableFutureParams = completableFutureParams;
        this.completableFutureResources = completableFutureResources;
        this.init(config);

        // Paging
        FifoMemoryPagingProvider pp = new FifoMemoryPagingProvider(DEFAULT_PAGE_SIZE);
        pp.setDefaultPageSize(DEFAULT_PAGE_SIZE);
        pp.setMaximumPageSize(MAXIMUM_PAGE_SIZE);
        setPagingProvider(pp);


    }


    /**
     * {@inheritDoc}
     * The initialize method is automatically called when the servlet is starting up, so it can
     * be used to configure the servlet to define resource providers, or set up
     * configuration, interceptors, etc.
     */
    @Override
    public void initialize() {
        // Address
        setServerAddressStrategy(new IncomingRequestAddressStrategy());

        // Encoding
        setDefaultResponseEncoding(EncodingEnum.XML);

        // Description
        setImplementationDescription("IHE PDQm Patient Demographics Supplier and IHE PIXm Patient Identifier Cross-Reference Manager");

        // Resources
        List<IResourceProvider> resourceProviders = new ArrayList<>();
        resourceProviders.add(new RestfulPatientResourceProvider(completableFutureParams, completableFutureResources));
        setResourceProviders(resourceProviders);
    }

    /**
     * <p>Constructor for PatientManagerFhirServer.</p>
     * @throws ServletException if init is impossible
     */
    public PatientManagerFhirServer() throws ServletException {
        super(FhirContext.forR4());

        this.init(this);

        // Paging
        FifoMemoryPagingProvider pp = new FifoMemoryPagingProvider(DEFAULT_PAGE_SIZE);
        pp.setDefaultPageSize(DEFAULT_PAGE_SIZE);
        pp.setMaximumPageSize(MAXIMUM_PAGE_SIZE);
        setPagingProvider(pp);

    }

}
