package net.ihe.gazelle.sb.fhir.business;

import net.ihe.gazelle.sb.api.business.DecodedContent;
import net.ihe.gazelle.sb.api.business.EncodedContent;
import org.hl7.fhir.r4.model.Resource;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class FhirResources implements DecodedContent, EncodedContent {

    private List<Resource> resources = new ArrayList<>();

    /**
     * add new resource
     * @param resource the resource to add
     */
    public void addResource(Resource resource) {
        resources.add(resource);
    }

    /**
     * add all resource
     * @param resource the resources to add
     */
    public < T extends Resource> void addAllResource(Collection<T> resource) {
        resources.addAll(resource);
    }

    /**
     * resource list accessor
     * @return a copy of the resource list
     */
    public List<Resource> getResources() {
        return new ArrayList<>(resources);
    }
}
