package net.ihe.gazelle.sb.fhir.adapter;

import net.ihe.gazelle.sb.api.business.DecodedContent;
import net.ihe.gazelle.sb.api.business.EncodedContent;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * HttpContent, content from webservice
 */
public class HttpContent implements DecodedContent, EncodedContent {


    private ServletConfig config;
    private HttpServletRequest request;
    private HttpServletResponse response;

    /**
     * complete constructor
     * @param config the servlet config
     * @param request the http request
     * @param response the http response
     */
    public HttpContent(ServletConfig config, HttpServletRequest request, HttpServletResponse response) {
        this.setConfig(config);
        this.setRequest(request);
        this.setResponse(response);
    }

    /**
     * default constructor
     */
    public HttpContent() {
    }

    /**
     * get servlet config
     * @return servlet configuration
     */
    public ServletConfig getConfig() {
        return config;
    }

    /**
     * set servlet config
     * @param config servlet configuration
     */
    public void setConfig(ServletConfig config) {
        this.config = config;
    }

    /**
     * get http request
     * @return the http request
     */
    public HttpServletRequest getRequest() {
        return request;
    }

    /**
     * set http request
     * @param request http request
     */
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    /**
     * get http response
     * @return http response
     */
    public HttpServletResponse getResponse() {
        return response;
    }

    /**
     * set http response
     * @param response http response
     */
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }
}
