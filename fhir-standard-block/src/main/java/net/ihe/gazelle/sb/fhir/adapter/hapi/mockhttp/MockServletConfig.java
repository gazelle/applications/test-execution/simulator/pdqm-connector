package net.ihe.gazelle.sb.fhir.adapter.hapi.mockhttp;



import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;


/**
 * Mock implementation of the {@link javax.servlet.ServletConfig} interface.
 *
 * @author Rod Johnson
 * @author Juergen Hoeller
 * @since 1.0.2
 */
public class MockServletConfig implements ServletConfig {

    private final ServletContext servletContext;

    private final String servletName;

    private final Map<String, String> initParameters = new LinkedHashMap<>();


    /**
     * Create a new MockServletConfig with a default .
     */
    public MockServletConfig() {
        this(null, "");
    }

    /**
     * Create a new MockServletConfig with a default.
     * @param servletName the name of the servlet
     */
    public MockServletConfig(String servletName) {
        this(null, servletName);
    }

    /**
     * Create a new MockServletConfig.
     * @param servletContext the ServletContext that the servlet runs in
     */
    public MockServletConfig(ServletContext servletContext) {
        this(servletContext, "");
    }

    /**
     * Create a new MockServletConfig.
     * @param servletContext the ServletContext that the servlet runs in
     * @param servletName the name of the servlet
     */
    public MockServletConfig(ServletContext servletContext, String servletName) {
        this.servletContext = (servletContext != null ? servletContext : null);
        this.servletName = servletName;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public String getServletName() {
        return this.servletName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ServletContext getServletContext() {
        return this.servletContext;
    }

    /**
     * add param
     * @param name name
     * @param value value
     */
    public void addInitParameter(String name, String value) {
        this.initParameters.put(name, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getInitParameter(String name) {
        return this.initParameters.get(name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Enumeration<String> getInitParameterNames() {
        return Collections.enumeration(this.initParameters.keySet());
    }

}
