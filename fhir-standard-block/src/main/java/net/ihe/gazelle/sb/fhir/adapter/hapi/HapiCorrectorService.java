package net.ihe.gazelle.sb.fhir.adapter.hapi;

import net.ihe.gazelle.lib.annotations.Package;
import net.ihe.gazelle.sb.fhir.business.FhirResources;
import net.ihe.gazelle.sb.fhir.business.FhirSearchParameters;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.CompletableFuture;
import java.util.zip.GZIPInputStream;

public class HapiCorrectorService {

    /**
     * Prevent hapi from responding directly to the caller and recreate response that will continue the normal way
     * @param request the http request
     * @param response the http response
     * @param config the servlet config
     * @param completableFutureParams async param
     * @param completableFutureResources async resources
     * @return corrected Response
     */
    @Package
    public Response correctHapi(HttpServletRequest request, HttpServletResponse response, ServletConfig config, CompletableFuture<FhirSearchParameters> completableFutureParams, CompletableFuture<FhirResources> completableFutureResources) {
        try {
            PatientManagerFhirServer patientManagerFhirServer = new PatientManagerFhirServer(config, completableFutureParams, completableFutureResources);
            patientManagerFhirServer.initialize();
            CustomHttpServletResponseWrapper wrapper = new CustomHttpServletResponseWrapper(response);
            patientManagerFhirServer.service(request, wrapper);
            byte[] fhirResponse = wrapper.getCustomOutputStream().toByteArray();
            return parseResponse(fhirResponse, response);
        } catch (Exception e) {
            return Response.status(500).build();
        }

    }

    /**
     * Recreate correct Response from hapi intercepted response and the servlet response corrupted by hapi
     * @param fhirResponse hapi intercepted response
     * @param response servlet response corrupted by hapi
     * @return correct response
     * @throws IOException if unable to read hapi response
     */
    @Package
    Response parseResponse(byte[] fhirResponse, HttpServletResponse response) throws IOException {
        InputStream stream;
        if ("gzip".equals(response.getHeader("Content-Encoding"))) {
            stream = new GZIPInputStream(new ByteArrayInputStream(fhirResponse));
        } else {
            stream = new ByteArrayInputStream(fhirResponse);
        }

        String newFhirResponse = new String(stream.readAllBytes(), StandardCharsets.UTF_8);


        Response.ResponseBuilder responseBuilder = Response.status(response.getStatus()).entity(newFhirResponse);
        response.getHeaderNames().forEach(headerName -> {
            if (!"Content-Encoding".equals(headerName)) {
                responseBuilder.header(headerName, response.getHeader(headerName));
            }
        });
        if (newFhirResponse.startsWith("<")) {
            responseBuilder.header("Content-Type", "application/fhir+xml");
        } else {
            responseBuilder.header("Content-Type", "application/fhir+json");
        }
        Response ourResponse = responseBuilder.build();
        response.reset();
        return ourResponse;
    }
}
