package net.ihe.gazelle.sb.fhir.adapter.hapi.mockhttp;

import org.apache.commons.lang3.ObjectUtils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.stream.Collectors;

import javax.servlet.AsyncContext;
import javax.servlet.DispatcherType;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpUpgradeHandler;
import javax.servlet.http.Part;
import javax.ws.rs.core.HttpHeaders;

/**
 * Mock implementation of the {@link javax.servlet.http.HttpServletRequest} interface.
 *
 * <p>The default, preferred {@link Locale} for the <em>server</em> mocked by this request
 * is {@link Locale#ENGLISH}. This value can be changed via {@link #addPreferredLocale}
 * or {@link #setPreferredLocales}.
 *
 * <p>As of Spring Framework 5.0, this set of mocks is designed on a Servlet 4.0 baseline.
 *
 * @author Juergen Hoeller
 * @author Rod Johnson
 * @author Rick Evans
 * @author Mark Fisher
 * @author Chris Beams
 * @author Sam Brannen
 * @author Brian Clozel
 * @since 1.0.2
 */
public class MockHttpServletRequest implements HttpServletRequest {

    private static final String HTTP = "http";

    private static final String HTTPS = "https";

    private static final String CHARSET_PREFIX = "charset=";

    private static final TimeZone GMT = TimeZone.getTimeZone("GMT");

    private static final BufferedReader EMPTY_BUFFERED_READER =
            new BufferedReader(new StringReader(""));

    /**
     * Date formats as specified in the HTTP RFC.
     *
     * @see <a href="https://tools.ietf.org/html/rfc7231#section-7.1.1.1">Section 7.1.1.1 of RFC 7231</a>
     */
    private static final String[] DATE_FORMATS = new String[]{
            "EEE, dd MMM yyyy HH:mm:ss zzz",
            "EEE, dd-MMM-yy HH:mm:ss zzz",
            "EEE MMM dd HH:mm:ss yyyy"
    };


    // ---------------------------------------------------------------------
    // Public constants
    // ---------------------------------------------------------------------

    /**
     * The default protocol: 'HTTP/1.1'.
     *
     * @since 4.3.7
     */
    public static final String DEFAULT_PROTOCOL = "HTTP/1.1";

    /**
     * The default scheme: 'http'.
     *
     * @since 4.3.7
     */
    public static final String DEFAULT_SCHEME = HTTP;

    /**
     * The default server address: '127.0.0.1'.
     */
    public static final String DEFAULT_SERVER_ADDR = "127.0.0.1";

    /**
     * The default server name: 'localhost'.
     */
    public static final String DEFAULT_SERVER_NAME = "localhost";

    /**
     * The default server port: '80'.
     */
    public static final int DEFAULT_SERVER_PORT = 80;

    /**
     * The default remote address: '127.0.0.1'.
     */
    public static final String DEFAULT_REMOTE_ADDR = "127.0.0.1";

    /**
     * The default remote host: 'localhost'.
     */
    public static final String DEFAULT_REMOTE_HOST = "localhost";


    // ---------------------------------------------------------------------
    // Lifecycle properties
    // ---------------------------------------------------------------------

    private final ServletContext servletContext;

    private boolean active = true;


    // ---------------------------------------------------------------------
    // ServletRequest properties
    // ---------------------------------------------------------------------

    private final Map<String, Object> attributes = new LinkedHashMap<>();

    private String characterEncoding;

    private byte[] content;

    private String contentType;

    private ServletInputStream inputStream;

    private BufferedReader reader;

    private final Map<String, String[]> parameters = new LinkedHashMap<>(16);

    private String protocol = DEFAULT_PROTOCOL;

    private String scheme = DEFAULT_SCHEME;

    private String serverName = DEFAULT_SERVER_NAME;

    private int serverPort = DEFAULT_SERVER_PORT;

    private String remoteAddr = DEFAULT_REMOTE_ADDR;

    private String remoteHost = DEFAULT_REMOTE_HOST;

    /**
     * List of locales in descending order.
     */
    private final LinkedList<Locale> locales = new LinkedList<>();

    private boolean secure = false;

    private int remotePort = DEFAULT_SERVER_PORT;

    private String localName = DEFAULT_SERVER_NAME;

    private String localAddr = DEFAULT_SERVER_ADDR;

    private int localPort = DEFAULT_SERVER_PORT;

    private boolean asyncStarted = false;

    private boolean asyncSupported = false;


    private DispatcherType dispatcherType = DispatcherType.REQUEST;


    // ---------------------------------------------------------------------
    // HttpServletRequest properties
    // ---------------------------------------------------------------------


    private String authType;


    private Cookie[] cookies;


    private String method;


    private String pathInfo;

    private String contextPath = "";


    private String queryString;


    private String remoteUser;

    private final Set<String> userRoles = new HashSet<>();


    private Principal userPrincipal;


    private String requestedSessionId;


    private String requestURI;

    private String servletPath = "";


    private HttpSession session;

    private boolean requestedSessionIdValid = true;

    private boolean requestedSessionIdFromCookie = true;

    private boolean requestedSessionIdFromURL = false;


    // ---------------------------------------------------------------------
    // Constructors
    // ---------------------------------------------------------------------

    /**
     * Create a new {@code MockHttpServletRequest} with a default.
     *
     * @see #MockHttpServletRequest(ServletContext, String, String)
     */
    public MockHttpServletRequest() {
        this(null, "", "");
    }

    /**
     * Create a new {@code MockHttpServletRequest} with a default
     * .
     *
     * @param method     the request method (may be {@code null})
     * @param requestURI the request URI (may be {@code null})
     * @see #setMethod
     * @see #setRequestURI
     * @see #MockHttpServletRequest(ServletContext, String, String)
     */
    public MockHttpServletRequest(String method, String requestURI) {
        this(null, method, requestURI);
    }

    /**
     * Create a new {@code MockHttpServletRequest} with the supplied {@link ServletContext}.
     *
     * @param servletContext the ServletContext that the request runs in
     *                       (may be {@code null} to use a default  )
     * @see #MockHttpServletRequest(ServletContext, String, String)
     */
    public MockHttpServletRequest(ServletContext servletContext) {
        this(servletContext, "", "");
    }

    /**
     * Create a new {@code MockHttpServletRequest} with the supplied {@link ServletContext},
     * {@code method}, and {@code requestURI}.
     * <p>The preferred locale will be set to {@link Locale#ENGLISH}.
     *
     * @param servletContext the ServletContext that the request runs in (may be
     *                       {@code null} to use a default  )
     * @param method         the request method (may be {@code null})
     * @param requestURI     the request URI (may be {@code null})
     * @see #setMethod
     * @see #setRequestURI
     * @see #setPreferredLocales
     */
    public MockHttpServletRequest(ServletContext servletContext, String method, String requestURI) {
        this.servletContext = (servletContext != null ? servletContext : null);
        this.method = method;
        this.requestURI = requestURI;
        this.locales.add(Locale.ENGLISH);
    }


    // ---------------------------------------------------------------------
    // Lifecycle methods
    // ---------------------------------------------------------------------

    /**
     * Return the ServletContext that this request is associated with. (Not
     * available in the standard HttpServletRequest interface for some reason.)
     */
    @Override
    public ServletContext getServletContext() {
        return this.servletContext;
    }

    /**
     * Return whether this request is still active (that is, not completed yet).
     */
    public boolean isActive() {
        return this.active;
    }

    /**
     * Mark this request as completed, keeping its state.
     */
    public void close() {
        this.active = false;
    }

    /**
     * Invalidate this request, clearing its state.
     */
    public void invalidate() {
        close();
        clearAttributes();
    }


    // ---------------------------------------------------------------------
    // ServletRequest interface
    // ---------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getAttribute(String name) {
        return this.attributes.get(name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Enumeration<String> getAttributeNames() {
        return Collections.enumeration(new LinkedHashSet<>(this.attributes.keySet()));
    }

    /**
     * {@inheritDoc}
     */
    @Override

    public String getCharacterEncoding() {
        return this.characterEncoding;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCharacterEncoding(String characterEncoding) {
        this.characterEncoding = characterEncoding;
        updateContentTypeHeader();
    }

    /**
     * update
     */
    private void updateContentTypeHeader() {

    }

    /**
     * Set the content of the request body as a byte array.
     * <p>If the supplied byte array represents text such as XML or JSON, the
     * {@link #setCharacterEncoding character encoding} should typically be
     * set as well.
     *
     * @see #setCharacterEncoding(String)
     * @see #getContentAsByteArray()
     * @see #getContentAsString()
     */
    public void setContent(byte[] content) {
        this.content = content;
        this.inputStream = null;
        this.reader = null;
    }

    /**
     * Get the content of the request body as a byte array.
     *
     * @return the content as a byte array (potentially {@code null})
     * @see #setContent(byte[])
     * @see #getContentAsString()
     * @since 5.0
     */

    public byte[] getContentAsByteArray() {
        return this.content;
    }

    /**
     * Get the content of the request body as a {@code String}, using the configured
     * {@linkplain #getCharacterEncoding character encoding}.
     *
     * @return the content as a {@code String}, potentially {@code null}
     * @throws IllegalStateException        if the character encoding has not been set
     * @throws UnsupportedEncodingException if the character encoding is not supported
     * @see #setContent(byte[])
     * @see #setCharacterEncoding(String)
     * @see #getContentAsByteArray()
     * @since 5.0
     */

    public String getContentAsString() throws IllegalStateException, UnsupportedEncodingException {

        if (this.content == null) {
            return null;
        }
        return new String(this.content, this.characterEncoding);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getContentLength() {
        return (this.content != null ? this.content.length : -1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getContentLengthLong() {
        return getContentLength();
    }

    /**
     * set content type
     *
     * @param contentType content type
     */
    public void setContentType(String contentType) {
        this.contentType = contentType;
        if (contentType != null) {

            updateContentTypeHeader();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getContentType() {
        return this.contentType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ServletInputStream getInputStream() {
        if (this.inputStream != null) {
            return this.inputStream;
        } else if (this.reader != null) {
            throw new IllegalStateException(
                    "Cannot call getInputStream() after getReader() has already been called for the current request");
        }

        return this.inputStream;
    }

    /**
     * Set a single value for the specified HTTP parameter.
     * <p>If there are already one or more values registered for the given
     * parameter name, they will be replaced.
     */
    public void setParameter(String name, String value) {
        setParameter(name, new String[]{value});
    }

    /**
     * Set an array of values for the specified HTTP parameter.
     * <p>If there are already one or more values registered for the given
     * parameter name, they will be replaced.
     */
    public void setParameter(String name, String... values) {
        this.parameters.put(name, values);
    }

    /**
     * Set all provided parameters <strong>replacing</strong> any existing
     * values for the provided parameter names. To add without replacing
     * existing values, use {@link #addParameters(java.util.Map)}.
     */
    public void setParameters(Map<String, ?> params) {
        params.forEach((key, value) -> {
            if (value instanceof String) {
                setParameter(key, (String) value);
            } else if (value instanceof String[]) {
                setParameter(key, (String[]) value);
            } else {
                throw new IllegalArgumentException(
                        "Parameter map value must be single value " + " or array of type [" + String.class.getName() + "]");
            }
        });
    }

    /**
     * Add a single value for the specified HTTP parameter.
     * <p>If there are already one or more values registered for the given
     * parameter name, the given value will be added to the end of the list.
     *
     * @param name  name
     * @param value value
     */
    public void addParameter(String name, String value) {
        addParameter(name, new String[]{value});
    }

    /**
     * Add an array of values for the specified HTTP parameter.
     * <p>If there are already one or more values registered for the given
     * parameter name, the given values will be added to the end of the list.
     *
     * @param name   name
     * @param values values
     */
    public void addParameter(String name, String... values) {
        String[] oldArr = this.parameters.get(name);
        if (oldArr != null) {
            String[] newArr = new String[oldArr.length + values.length];
            System.arraycopy(oldArr, 0, newArr, 0, oldArr.length);
            System.arraycopy(values, 0, newArr, oldArr.length, values.length);
            this.parameters.put(name, newArr);
        } else {
            this.parameters.put(name, values);
        }
    }

    /**
     * Add all provided parameters <strong>without</strong> replacing any
     * existing values. To replace existing values, use
     * {@link #setParameters(java.util.Map)}.
     *
     * @param params params
     */
    public void addParameters(Map<String, ?> params) {
        params.forEach((key, value) -> {
            if (value instanceof String) {
                addParameter(key, (String) value);
            } else if (value instanceof String[]) {
                addParameter(key, (String[]) value);
            } else {
                throw new IllegalArgumentException("Parameter map value must be single value " +
                        " or array of type [" + String.class.getName() + "]");
            }
        });
    }

    /**
     * Remove already registered values for the specified HTTP parameter, if any.
     *
     * @param name name
     */
    public void removeParameter(String name) {
        this.parameters.remove(name);
    }

    /**
     * Remove all existing parameters.
     */
    public void removeAllParameters() {
        this.parameters.clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getParameter(String name) {

        String[] arr = this.parameters.get(name);
        return (arr != null && arr.length > 0 ? arr[0] : null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Enumeration<String> getParameterNames() {
        return Collections.enumeration(this.parameters.keySet());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String[] getParameterValues(String name) {

        return this.parameters.get(name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, String[]> getParameterMap() {
        return Collections.unmodifiableMap(this.parameters);
    }

    /**
     * set protocol
     *
     * @param protocol protocol
     */
    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getProtocol() {
        return this.protocol;
    }

    /**
     * set scheme
     *
     * @param scheme scheme
     */
    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getScheme() {
        return this.scheme;
    }

    /**
     * set server name
     *
     * @param serverName server name
     */
    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getServerName() {
        String host = getHeader(HttpHeaders.HOST);
        if (host != null) {
            host = host.trim();
            if (host.startsWith("[")) {
                host = host.substring(1, host.indexOf(']'));
            } else if (host.contains(":")) {
                host = host.substring(0, host.indexOf(':'));
            }
            return host;
        }

        // else
        return this.serverName;
    }

    /**
     * set server port
     *
     * @param serverPort port
     */
    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getServerPort() {
        String host = getHeader(HttpHeaders.HOST);
        if (host != null) {
            host = host.trim();
            int idx;
            if (host.startsWith("[")) {
                idx = host.indexOf(':', host.indexOf(']'));
            } else {
                idx = host.indexOf(':');
            }
            if (idx != -1) {
                return Integer.parseInt(host.substring(idx + 1));
            }
        }

        // else
        return this.serverPort;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BufferedReader getReader() throws UnsupportedEncodingException {
        if (this.reader != null) {
            return this.reader;
        } else if (this.inputStream != null) {
            throw new IllegalStateException(
                    "Cannot call getReader() after getInputStream() has already been called for the current request");
        }

        if (this.content != null) {
            InputStream sourceStream = new ByteArrayInputStream(this.content);
            Reader sourceReader = (this.characterEncoding != null) ?
                    new InputStreamReader(sourceStream, this.characterEncoding) :
                    new InputStreamReader(sourceStream);
            this.reader = new BufferedReader(sourceReader);
        } else {
            this.reader = EMPTY_BUFFERED_READER;
        }
        return this.reader;
    }

    /**
     * set remote add
     *
     * @param remoteAddr addr
     */
    public void setRemoteAddr(String remoteAddr) {
        this.remoteAddr = remoteAddr;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRemoteAddr() {
        return this.remoteAddr;
    }

    /**
     * set remote host
     *
     * @param remoteHost host
     */
    public void setRemoteHost(String remoteHost) {
        this.remoteHost = remoteHost;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRemoteHost() {
        return this.remoteHost;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAttribute(String name, Object value) {


        if (value != null) {
            this.attributes.put(name, value);
        } else {
            this.attributes.remove(name);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeAttribute(String name) {


        this.attributes.remove(name);
    }

    /**
     * Clear all of this request's attributes.
     */
    public void clearAttributes() {
        this.attributes.clear();
    }

    /**
     * Add a new preferred locale, before any existing locales.
     *
     * @param locale locale
     * @see #setPreferredLocales
     */
    public void addPreferredLocale(Locale locale) {
        this.locales.addFirst(locale);
        updateAcceptLanguageHeader();
    }

    /**
     * Set the list of preferred locales, in descending order, effectively replacing
     * any existing locales.
     *
     * @param locales locales
     * @see #addPreferredLocale
     * @since 3.2
     */
    public void setPreferredLocales(List<Locale> locales) {
        this.locales.clear();
        this.locales.addAll(locales);
        updateAcceptLanguageHeader();
    }

    /**
     * update
     */
    private void updateAcceptLanguageHeader() {
    }

    /**
     * Return the first preferred {@linkplain Locale locale} configured
     * in this mock request.
     * <p>If no locales have been explicitly configured, the default,
     * preferred {@link Locale} for the <em>server</em> mocked by this
     * request is {@link Locale#ENGLISH}.
     * <p>In contrast to the Servlet specification, this mock implementation
     * does <strong>not</strong> take into consideration any locales
     * specified via the {@code Accept-Language} header.
     *
     * @see javax.servlet.ServletRequest#getLocale()
     * @see #addPreferredLocale(Locale)
     * @see #setPreferredLocales(List)
     */
    @Override
    public Locale getLocale() {
        return this.locales.getFirst();
    }

    /**
     * Return an {@linkplain Enumeration enumeration} of the preferred
     * {@linkplain Locale locales} configured in this mock request.
     * <p>If no locales have been explicitly configured, the default,
     * preferred {@link Locale} for the <em>server</em> mocked by this
     * request is {@link Locale#ENGLISH}.
     * <p>In contrast to the Servlet specification, this mock implementation
     * does <strong>not</strong> take into consideration any locales
     * specified via the {@code Accept-Language} header.
     *
     * @see javax.servlet.ServletRequest#getLocales()
     * @see #addPreferredLocale(Locale)
     * @see #setPreferredLocales(List)
     */
    @Override
    public Enumeration<Locale> getLocales() {
        return Collections.enumeration(this.locales);
    }

    /**
     * Set the boolean {@code secure} flag indicating whether the mock request
     * was made using a secure channel, such as HTTPS.
     *
     * @param secure secure?
     * @see #isSecure()
     * @see #getScheme()
     * @see #setScheme(String)
     */
    public void setSecure(boolean secure) {
        this.secure = secure;
    }

    /**
     * Return {@code true} if the {@link #setSecure secure} flag has been set
     * to {@code true} or if the {@link #getScheme scheme} is {@code https}.
     *
     * @see javax.servlet.ServletRequest#isSecure()
     */
    @Override
    public boolean isSecure() {
        return (this.secure || HTTPS.equalsIgnoreCase(this.scheme));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RequestDispatcher getRequestDispatcher(String path) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Deprecated
    public String getRealPath(String path) {
        return this.servletContext.getRealPath(path);
    }


    /**
     * set remote port
     *
     * @param remotePort port
     */
    public void setRemotePort(int remotePort) {
        this.remotePort = remotePort;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getRemotePort() {
        return this.remotePort;
    }

    /**
     * set local name
     *
     * @param localName name
     */
    public void setLocalName(String localName) {
        this.localName = localName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLocalName() {
        return this.localName;
    }

    /**
     * Set localAddr
     *
     * @param localAddr value for the localAddr property
     */
    public void setLocalAddr(String localAddr) {
        this.localAddr = localAddr;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLocalAddr() {
        return this.localAddr;
    }

    /**
     * Set localPort
     *
     * @param localPort value for localPort
     */
    public void setLocalPort(int localPort) {
        this.localPort = localPort;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getLocalPort() {
        return this.localPort;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AsyncContext startAsync() {
        return startAsync(this, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AsyncContext startAsync(ServletRequest request, ServletResponse response) {
        return null;
    }

    /**
     * Set asyncStarted
     *
     * @param asyncStarted value for asyncStarted property
     */
    public void setAsyncStarted(boolean asyncStarted) {
        this.asyncStarted = asyncStarted;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAsyncStarted() {
        return this.asyncStarted;
    }

    /**
     * Set asyncSupported
     *
     * @param asyncSupported value for asyncSupported property
     */
    public void setAsyncSupported(boolean asyncSupported) {
        this.asyncSupported = asyncSupported;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isAsyncSupported() {
        return this.asyncSupported;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AsyncContext getAsyncContext() {
        return null;
    }

    /**
     * Set dispatcherType
     *
     * @param dispatcherType value for the property
     */
    public void setDispatcherType(DispatcherType dispatcherType) {
        this.dispatcherType = dispatcherType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DispatcherType getDispatcherType() {
        return this.dispatcherType;
    }

    /**
     * Set authType
     *
     * @param authType value for the property
     */
    public void setAuthType(String authType) {
        this.authType = authType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAuthType() {
        return this.authType;
    }

    /**
     * Set cookies
     *
     * @param cookies values for the property
     */
    public void setCookies(Cookie... cookies) {
        this.cookies = (ObjectUtils.isEmpty(cookies) ? null : cookies);
        if (this.cookies == null) {
            removeHeader(HttpHeaders.COOKIE);
        } else {
            doAddHeaderValue(HttpHeaders.COOKIE, encodeCookies(this.cookies), true);
        }
    }

    /**
     * .
     * Encode cookies
     *
     * @param cookies to be encoding
     * @return String value of encoded cookies.
     */
    private static String encodeCookies(Cookie... cookies) {
        return Arrays.stream(cookies)
                .map(c -> c.getName() + '=' + (c.getValue() == null ? "" : c.getValue()))
                .collect(Collectors.joining("; "));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Cookie[] getCookies() {
        return this.cookies;
    }

    /**
     * Add an HTTP header entry for the given name.
     * <p>While this method can take any {@code Object} as a parameter,
     * it is recommended to use the following types:
     * <ul>
     * <li>String or any Object to be converted using {@code toString()}; see {@link #getHeader}.</li>
     * <li>String, Number, or Date for date headers; see {@link #getDateHeader}.</li>
     * <li>String or Number for integer headers; see {@link #getIntHeader}.</li>
     * <li>{@code String[]} or {@code Collection<String>} for multiple values; see {@link #getHeaders}.</li>
     * </ul>
     *
     * @see #getHeaderNames
     * @see #getHeaders
     * @see #getHeader
     * @see #getDateHeader
     */
    public void addHeader(String name, Object value) {
        if (HttpHeaders.CONTENT_TYPE.equalsIgnoreCase(name)) {
            setContentType(value.toString());
        } else if (HttpHeaders.ACCEPT_LANGUAGE.equalsIgnoreCase(name)) {
            try {

            } catch (IllegalArgumentException ex) {
                // Invalid Accept-Language format -> just store plain header
            }
            doAddHeaderValue(name, value, true);
        } else {
            doAddHeaderValue(name, value, false);
        }
    }

    /**
     * Add header value
     *
     * @param name    nom du header
     * @param value   valeur associée
     * @param replace true if the header shall be replaced
     */
    private void doAddHeaderValue(String name, Object value, boolean replace) {
        //Empty method
    }

    /**
     * Remove already registered entries for the specified HTTP header, if any.
     *
     * @since 4.3.20
     */
    public void removeHeader(String name) {
    }

    /**
     * Return the long timestamp for the date header with the given {@code name}.
     * <p>If the internal value representation is a String, this method will try
     * to parse it as a date using the supported date formats:
     * <ul>
     * <li>"EEE, dd MMM yyyy HH:mm:ss zzz"</li>
     * <li>"EEE, dd-MMM-yy HH:mm:ss zzz"</li>
     * <li>"EEE MMM dd HH:mm:ss yyyy"</li>
     * </ul>
     *
     * @param name the header name
     * @see <a href="https://tools.ietf.org/html/rfc7231#section-7.1.1.1">Section 7.1.1.1 of RFC 7231</a>
     */
    @Override
    public long getDateHeader(String name) {
        return -1;
    }

    /**
     * Parse Date Header
     *
     * @param name  nom du header
     * @param value value of the header
     * @return the long value for the date time.
     */
    private long parseDateHeader(String name, String value) {
        for (String dateFormat : DATE_FORMATS) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat, Locale.US);
            simpleDateFormat.setTimeZone(GMT);
            try {
                return simpleDateFormat.parse(value).getTime();
            } catch (ParseException ex) {
                // ignore
            }
        }
        throw new IllegalArgumentException("Cannot parse date value '" + value + "' for '" + name + "' header");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getHeader(String name) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Enumeration<String> getHeaders(String name) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Enumeration<String> getHeaderNames() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getIntHeader(String name) {

        return -1;
    }

    /**
     * Set method
     *
     * @param method value
     */
    public void setMethod(String method) {
        this.method = method;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMethod() {
        return this.method;
    }

    /**
     * Set pathInfo
     *
     * @param pathInfo value
     */
    public void setPathInfo(String pathInfo) {
        this.pathInfo = pathInfo;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPathInfo() {
        return this.pathInfo;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPathTranslated() {
        return (this.pathInfo != null ? getRealPath(this.pathInfo) : null);
    }

    /**
     * Set contextPath
     *
     * @param contextPath value
     */
    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getContextPath() {
        return this.contextPath;
    }

    /**
     * Set queryString
     *
     * @param queryString value
     */
    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    @Override
    public String getQueryString() {
        return this.queryString;
    }

    /**
     * Set remoteUser
     *
     * @param remoteUser value
     */
    public void setRemoteUser(String remoteUser) {
        this.remoteUser = remoteUser;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRemoteUser() {
        return this.remoteUser;
    }

    /**
     * Add user role
     *
     * @param role to add
     */
    public void addUserRole(String role) {
        this.userRoles.add(role);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isUserInRole(String role) {
        return false;
    }

    /**
     * Set userPrincipal
     *
     * @param userPrincipal principal to set
     */
    public void setUserPrincipal(Principal userPrincipal) {
        this.userPrincipal = userPrincipal;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Principal getUserPrincipal() {
        return this.userPrincipal;
    }

    /**
     * Set requestSessionId
     * @param requestedSessionId
     */
    public void setRequestedSessionId(String requestedSessionId) {
        this.requestedSessionId = requestedSessionId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRequestedSessionId() {
        return this.requestedSessionId;
    }

    /**
     * Set requestURI
     * @param requestURI value
     */
    public void setRequestURI(String requestURI) {
        this.requestURI = requestURI;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getRequestURI() {
        return this.requestURI;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StringBuffer getRequestURL() {
        String scheme = getScheme();
        String server = getServerName();
        int port = getServerPort();
        String uri = getRequestURI();

        StringBuffer url = new StringBuffer(scheme).append("://").append(server);
        if (port > 0 && ((HTTP.equalsIgnoreCase(scheme) && port != 80) ||
                (HTTPS.equalsIgnoreCase(scheme) && port != 443))) {
            url.append(':').append(port);
        }
        if (uri != null && !uri.isEmpty()) {
            url.append(uri);
        }
        return url;
    }

    /**
     * Set servletPath
     * @param servletPath value
     */
    public void setServletPath(String servletPath) {
        this.servletPath = servletPath;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getServletPath() {
        return this.servletPath;
    }

    /**
     * Set session
     * @param session value
     */
    public void setSession(HttpSession session) {
        this.session = session;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HttpSession getSession(boolean create) {

        return this.session;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HttpSession getSession() {
        return getSession(true);
    }

    /**
     * The implementation of this (Servlet 3.1+) method calls
     * Otherwise it simply returns the current session id.
     *
     * @since 4.0.3
     */
    @Override
    public String changeSessionId() {

        return this.session.getId();
    }

    /**
     * Set requestSessionIdValid
     * @param requestedSessionIdValid value
     */
    public void setRequestedSessionIdValid(boolean requestedSessionIdValid) {
        this.requestedSessionIdValid = requestedSessionIdValid;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isRequestedSessionIdValid() {
        return this.requestedSessionIdValid;
    }

    /**
     * Set requestedSessionIdFromCookie
     * @param requestedSessionIdFromCookie value
     */
    public void setRequestedSessionIdFromCookie(boolean requestedSessionIdFromCookie) {
        this.requestedSessionIdFromCookie = requestedSessionIdFromCookie;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isRequestedSessionIdFromCookie() {
        return this.requestedSessionIdFromCookie;
    }

    /**
     * Set requestedSessionIdFromURL
     * @param requestedSessionIdFromURL value
     */
    public void setRequestedSessionIdFromURL(boolean requestedSessionIdFromURL) {
        this.requestedSessionIdFromURL = requestedSessionIdFromURL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isRequestedSessionIdFromURL() {
        return this.requestedSessionIdFromURL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Deprecated
    public boolean isRequestedSessionIdFromUrl() {
        return isRequestedSessionIdFromURL();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean authenticate(HttpServletResponse response) throws IOException, ServletException {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void login(String username, String password) throws ServletException {
        throw new UnsupportedOperationException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void logout() throws ServletException {
        this.userPrincipal = null;
        this.remoteUser = null;
        this.authType = null;
    }

    /**
     * Add part
     * @param part value
     */
    public void addPart(Part part) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Part getPart(String name) throws IOException, ServletException {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<Part> getParts() throws IOException, ServletException {
        List<Part> result = new LinkedList<>();

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T extends HttpUpgradeHandler> T upgrade(Class<T> handlerClass) throws IOException, ServletException {
        throw new UnsupportedOperationException();
    }
}
