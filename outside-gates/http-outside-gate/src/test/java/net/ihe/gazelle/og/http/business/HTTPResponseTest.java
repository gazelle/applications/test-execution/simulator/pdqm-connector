package net.ihe.gazelle.og.http.business;

import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.og.tcp.business.TCPMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Context;

import static org.junit.jupiter.api.Assertions.*;

class HTTPResponseTest {

    /**
     * tested class
     */
    private HTTPResponse httpResponse;

    private HTTPResponse httpResponse1;

    @Covers(requirements = {"HTTPOG-10"})
    @BeforeEach
    public void init() {
        String uuid = "7d16f712-bcff-11ea-b3de-0242ac130004";
        String standardKeyword = "HTTP";
        byte[] httpBody = new byte[] {1,2,3};
        HTTPVersion httpVersion = HTTPVersion.V1_1;
        TCPMessage tcpMessage = new TCPMessage(uuid,standardKeyword,httpBody);
        httpResponse = new HTTPResponse(uuid,standardKeyword,httpBody, tcpMessage, httpVersion, 200,"");
        httpResponse1 = new HTTPResponse(uuid,standardKeyword,httpBody, tcpMessage, httpVersion, 200,"");
    }

    @Covers(requirements = {"HTTPOG-13"})
    @Test
    public void httpStatusPhraseTest() {
        String myPhrase = "myPhrase";
        httpResponse.setHttpStatusPhrase(myPhrase);

        assertEquals(myPhrase, httpResponse.getHttpStatusPhrase(), "http status phrase should be the one set");
    }

    @Test
    public void phraseNullTest() {
        assertThrows(IllegalArgumentException.class, ()-> httpResponse.setHttpStatusPhrase(null), "http status phrase must not be null");
    }

    @Covers(requirements = {"HTTPOG-11"})
    @Test
    public void httpStatusCodeTest() {
        int myCode = 230;
        httpResponse.setHttpStatusCode(myCode);

        assertEquals(myCode, httpResponse.getHttpStatusCode(), "http status code should be the one set");
    }

    @Test
    public void equalsSameTest() {

        assertEquals(httpResponse, httpResponse1, "identical response shall be equal");
        assertEquals(httpResponse.hashCode(), httpResponse1.hashCode(), "same for hashcode");
    }

    @Test
    public void equalsidentityTest() {

        assertEquals(httpResponse, httpResponse, "response shall be equal to itself");
    }

    @Test
    public void equalsDifferentTest() {
        httpResponse.setHttpStatusCode(1);
        httpResponse1.setHttpStatusCode(2);

        assertNotEquals(httpResponse, httpResponse1, "different response shall not be equal");
        assertNotEquals(httpResponse.hashCode(), httpResponse1.hashCode(), "same for hashcode");
    }

}