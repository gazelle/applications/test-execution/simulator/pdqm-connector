package net.ihe.gazelle.og.http.business;

import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.modelapi.messaging.business.MessageMetadataInstance;
import net.ihe.gazelle.og.tcp.business.TCPMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class AbstractHTTPMessageTest {

    /**
     * tested class
     */
    private AbstractHTTPMessage abstractHTTPMessage;
    private AbstractHTTPMessage abstractHTTPMessage2;

    @BeforeEach
    public void init() {
        String uuid = "c4657530-bc64-11ea-b3de-0242ac130004";
        String standardKeyword = "HTTP";
        TCPMessage rawTCPMessage = new TCPMessage(uuid, standardKeyword, new byte[] {1});
        byte[] httpBody = new byte[] {1,2,3};
        HTTPVersion httpVersion = HTTPVersion.V1_1;
        abstractHTTPMessage = new AbstractHTTPMessage(uuid, standardKeyword, httpBody, rawTCPMessage, httpVersion);
        abstractHTTPMessage2 = new AbstractHTTPMessage(uuid, standardKeyword, httpBody, rawTCPMessage, httpVersion);
        abstractHTTPMessage.setHttpHeader(new ArrayList<>());
    }

    @Covers(requirements = {"HTTPOG-4"})
    @Test
    public void httpBodyTest() {
        byte[] expectedHttpBody = new byte[] {4,5,6};
        abstractHTTPMessage.setHttpBody(expectedHttpBody);
        assertEquals(expectedHttpBody,abstractHTTPMessage.getHttpBody(),"The expectedHttpBody must equal to " + Arrays.toString(expectedHttpBody));
    }

    @Covers(requirements = {"HTTPOG-3"})
    @Test
    public void httpVersionTest() {
        HTTPVersion expectedHttpVersion = HTTPVersion.V2_0;
        abstractHTTPMessage.setHttpVersion(expectedHttpVersion);
        assertEquals(expectedHttpVersion,abstractHTTPMessage.getHttpVersion(),"The expectedHttpVersion must equal to " + expectedHttpVersion);
    }

    @Covers(requirements = {"HTTPOG-5"})
    @Test
    public void httpFieldTest() {
        List<MessageMetadataInstance> expectedHttpHeaderList = new ArrayList<>();
        expectedHttpHeaderList.add(new MessageMetadataInstance("name","value"));
        abstractHTTPMessage.setHttpHeader(expectedHttpHeaderList);
        assertEquals(expectedHttpHeaderList,abstractHTTPMessage.getHttpHeader(),"The expectedHttpHeaderList must equal to " + expectedHttpHeaderList);
    }

    @Test
    public void addHttpHeaderTest() {
        MessageMetadataInstance messageMetadataInstance = new MessageMetadataInstance("name","value");
        abstractHTTPMessage.addHttpHeader(messageMetadataInstance);
        assertTrue(abstractHTTPMessage.getHttpHeader().contains(messageMetadataInstance),"The headerField must contain after addition " + messageMetadataInstance);
    }

    @Test
    public void removeHttpHeaderTest() {
        MessageMetadataInstance messageMetadataInstance = new MessageMetadataInstance("name","value");
        abstractHTTPMessage.addHttpHeader(messageMetadataInstance);
        abstractHTTPMessage.removeHttpHeader(messageMetadataInstance);
        assertFalse(abstractHTTPMessage.getHttpHeader().contains(messageMetadataInstance),"The headerField must not contain after removing " + messageMetadataInstance);
    }

    @Covers(requirements = {"HTTPOG-2"})
    @Test
    public void rawTCPMessageTest() {
        TCPMessage expectedTCPMessage = new TCPMessage("29e9608e-bcff-11ea-b3de-0242ac130004","TCP",new byte[]{1,2,3});
        abstractHTTPMessage.setRawTCPMessage(expectedTCPMessage);
        assertEquals(expectedTCPMessage, abstractHTTPMessage.getRawTCPMessage(), "The expectedTCPMessage must equal to " + expectedTCPMessage);
    }

    @Test
    public void objectEqualsTest() {
        assertEquals(abstractHTTPMessage, abstractHTTPMessage,"object shall be equal to itself");
    }

    @Test
    public void equalsTest() {
        assertEquals(abstractHTTPMessage, abstractHTTPMessage2,"identical objects shall be equal");
        assertEquals(abstractHTTPMessage.hashCode(), abstractHTTPMessage2.hashCode(), "hashcode shall be equal too");
    }

    @Test
    public void notEqualsTest() {
        abstractHTTPMessage2.setHttpVersion(HTTPVersion.V2_0);
        assertNotEquals(abstractHTTPMessage, abstractHTTPMessage2,"identical objects shall be not equal");
    }
}

