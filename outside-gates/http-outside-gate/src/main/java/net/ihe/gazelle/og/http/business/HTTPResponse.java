package net.ihe.gazelle.og.http.business;

import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.og.tcp.business.TCPMessage;

import java.util.Objects;

@Covers(requirements = {"HTTPOG-10"})
public class HTTPResponse extends AbstractHTTPMessage {

    private int httpStatusCode;

    private String httpStatusPhrase;

    /**
     * Default constructor for the class taking as input all mandatory elements for the object to be created.
     *
     * @param uuid       value of the entity UUID.
     * @param standardKeyword Keyword of the used standard
     * @param httpBody   body of the http message
     * @param rawTCPMessage TCP Message
     * @param httpVersion HTTP Version
     * @throws IllegalArgumentException if any mandatory element is null.
     */
    public HTTPResponse(String uuid, String standardKeyword, byte[] httpBody, TCPMessage rawTCPMessage, HTTPVersion httpVersion, int httpStatusCode, String httpStatusPhrase) {
        super(uuid, standardKeyword, httpBody, rawTCPMessage, httpVersion);
        this.setHttpStatusCode(httpStatusCode);
        this.setHttpStatusPhrase(httpStatusPhrase);
    }

    /**
     * get http status code
     * @return http status code
     */
    public int getHttpStatusCode() {
        return httpStatusCode;
    }

    /**
     * set http status code
     * @param httpStatusCode status code to set
     */
    @Covers(requirements = {"HTTPOG-11"})
    public void setHttpStatusCode(int httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    /**
     * get http status phrase
     * @return http status phrase
     */
    public String getHttpStatusPhrase() {
        return httpStatusPhrase;
    }

    /**
     * set http status phrase
     * @param httpStatusPhrase, the http status phrase
     */
    @Covers(requirements = {"HTTPOG-13"})
    public void setHttpStatusPhrase(String httpStatusPhrase) {
        if (httpStatusPhrase == null) {
            throw new IllegalArgumentException("http status phrase shall not be null");
        }
        this.httpStatusPhrase = httpStatusPhrase;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HTTPResponse)) return false;
        if (!super.equals(o)) return false;
        HTTPResponse that = (HTTPResponse) o;
        return httpStatusCode == that.httpStatusCode &&
                httpStatusPhrase.equals(that.httpStatusPhrase);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), httpStatusCode, httpStatusPhrase);
    }
}
