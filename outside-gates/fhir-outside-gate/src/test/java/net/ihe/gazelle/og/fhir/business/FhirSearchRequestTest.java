package net.ihe.gazelle.og.fhir.business;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link FhirSearchRequest}
 */
class FhirSearchRequestTest {

    /**
     * tested class
     */
    private FhirSearchRequest fhirSearchRequest;

    /**
     * Create a test {@link FhirSearchRequest}
     */
    @BeforeEach
    public void init() {
        String uuid = "40e11188-bc69-11ea-b3de-0242ac130004";
        String standardKeyword = "FHIR";
        FhirVersion fhirVersion = FhirVersion.R4;
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.TEXT, true);
        String base = "base";
        String mimeType = "mimeType";
        String id = "2";
        String vid = "3";
        String compartment = "compartment";
        fhirSearchRequest = new FhirSearchRequest(uuid, standardKeyword, fhirVersion, generalParam, base,
                mimeType, id, vid, compartment, "type");
    }

    /**
     * Test getter for searchParameters property.
     */
    @Test
    @Covers(requirements = "FHIROG-22")
    public void searchParametersTest() {
        assertNotNull(fhirSearchRequest.getSearchParameters(), "Search Parameters shall never be null.");
    }

    /**
     * Test adding a search parameter
     */
    @Test
    public void addSearchParameter() {
        SearchParamImpl searchParam = new SearchParamImpl("VALUE");

        fhirSearchRequest.addSearchParameter(searchParam);

        assertEquals(1, fhirSearchRequest.getSearchParameters().size(), "Search parameter shall have been added !");
        assertEquals(searchParam, fhirSearchRequest.getSearchParameters().get(0), "Search parameter shall be the one added !");
    }

    /**
     * Test removing a search parameter
     */
    @Test
    public void removeSearchParameter() {
        SearchParamImpl searchParam = new SearchParamImpl("VALUE");
        fhirSearchRequest.addSearchParameter(searchParam);

        fhirSearchRequest.removeSearchParameter(searchParam);

        assertEquals(0, fhirSearchRequest.getSearchParameters().size(), "Search parameter shall have been removed !");
    }

    /**
     * Test identityEqual with two entity with same identity.
     */
    @Test
    void identityEqualTrue() {
        String uuid = "40e11188-bc69-11ea-b3de-0242ac130004";
        String standardKeyword = "FHIR";
        FhirVersion fhirVersion = FhirVersion.R4;
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.TEXT, true);
        String base = "base";
        String mimeType = "mimeType";
        String id = "2";
        String vid = "3";
        String compartment = "compartment";
        FhirSearchRequest fhirSearchRequest1 = new FhirSearchRequest(uuid, standardKeyword, fhirVersion, generalParam, base,
                mimeType, id, vid, compartment, "type");

        assertTrue(fhirSearchRequest.identityEqual(fhirSearchRequest1), "Two FhirRequests with same UUID shall be " +
                "identityEqual.");
    }

    /**
     * Test identityEqual with same object
     */
    @Test
    void identityEqualSameObject() {
        assertTrue(fhirSearchRequest.identityEqual(fhirSearchRequest), "FhirSearchRequest shall be identityEqual to himself.");
    }

    /**
     * Test identityEqual with instance of another class
     */
    @Test
    void identityEqualOtherClass() {
        assertFalse(fhirSearchRequest.identityEqual(13), "FhirSearchRequest shall not be identityEqual to an instance of another class.");
    }

    /**
     * Test identityEqual with two entity with same identity but different value.
     */
    @Test
    void identityEqualDifferentValue() {
        String uuid = "40e11188-bc69-11ea-b3de-0242ac130004";
        String standardKeyword = "FHIR";
        FhirVersion fhirVersion = FhirVersion.R4;
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.TEXT, true);
        String base = "base";
        String mimeType = "mimeType";
        String id = "3";
        String vid = "4";
        String compartment = "compartment";
        FhirSearchRequest fhirSearchRequest1 = new FhirSearchRequest(uuid, standardKeyword, fhirVersion, generalParam, base,
                mimeType, id, vid, compartment, "tarte");

        assertTrue(fhirSearchRequest.identityEqual(fhirSearchRequest1), "Two FhirSearchRequest with same UUID shall be " +
                "identityEqual.");
    }

    /**
     * Test identityEqual with two entity with different UUID
     */
    @Test
    void identityEqualDifferentUUID() {
        String uuid = "UUID";
        String standardKeyword = "FHIR";
        FhirVersion fhirVersion = FhirVersion.R4;
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.TEXT, true);
        String base = "base";
        String mimeType = "mimeType";
        String id = "2";
        String vid = "3";
        String compartment = "compartment";
        FhirSearchRequest fhirSearchRequest1 = new FhirSearchRequest(uuid, standardKeyword, fhirVersion, generalParam, base,
                mimeType, id, vid, compartment, "type");

        assertFalse(fhirSearchRequest.identityEqual(fhirSearchRequest1), "Two FhirSearchRequest with different UUID shall" +
                " not be identityEqual.");
    }

    /**
     * Test object is equal to itself.
     */
    @Test
    void equalsSameObject() {
        assertTrue(fhirSearchRequest.equals(fhirSearchRequest), "Object shall be equal to itself.");
    }

    /**
     * Test object is not equal with an instance of another class
     */
    @Test
    void equalsOtherClass() {
        assertFalse(fhirSearchRequest.equals(13), "Object shall not be equal to an instance of another class !");
    }

    /**
     * Test object is not equal with null
     */
    @Test
    void equalsNull() {
        assertFalse(fhirSearchRequest.equals(null), "Object shall not be equal to null.");
    }

    /**
     * Test object is not equal if super is not equal.
     */
    @Test
    void equalsSuper() {
        String uuid = "40e11188-bc69-11ea-b3de-0242ac130004";
        String standardKeyword = "FHIRR5";
        FhirVersion fhirVersion = FhirVersion.R4;
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.TEXT, true);
        String base = "base";
        String mimeType = "mimeType";
        String id = "2";
        String vid = "3";
        String compartment = "compartment";
        FhirSearchRequest fhirSearchRequest1 = new FhirSearchRequest(uuid, standardKeyword, fhirVersion, generalParam, base,
                mimeType, id, vid, compartment, "type");

        assertFalse(fhirSearchRequest.equals(fhirSearchRequest1), "Object shall not be equal if super is not equal.");
    }

    /**
     * Test objects are equal if they have the same value
     */
    @Test
    void equalsSameValue() {
        String uuid = "40e11188-bc69-11ea-b3de-0242ac130004";
        String standardKeyword = "FHIR";
        FhirVersion fhirVersion = FhirVersion.R4;
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.TEXT, true);
        String base = "base";
        String mimeType = "mimeType";
        String id = "2";
        String vid = "3";
        String compartment = "compartment";
        FhirSearchRequest fhirSearchRequest1 = new FhirSearchRequest(uuid, standardKeyword, fhirVersion, generalParam, base,
                mimeType, id, vid, compartment, "type");

        assertTrue(fhirSearchRequest1.equals(fhirSearchRequest1), "Object shall be equal if they have the same value !");
    }

    /**
     * Test objects are not equal if they have not the same parameters
     */
    @Test
    void equalsDifferentParameters() {
        String uuid = "40e11188-bc69-11ea-b3de-0242ac130004";
        String standardKeyword = "FHIR";
        FhirVersion fhirVersion = FhirVersion.R4;
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.TEXT, true);
        String base = "base";
        String mimeType = "mimeType";
        String id = "2";
        String vid = "3";
        String compartment = "compartment";
        FhirSearchRequest fhirSearchRequest1 = new FhirSearchRequest(uuid, standardKeyword, fhirVersion, generalParam, base,
                mimeType, id, vid, compartment, "type");
        fhirSearchRequest1.addSearchParameter(new SearchParamImpl("VALUE"));

        assertFalse(fhirSearchRequest.equals(fhirSearchRequest1), "Object shall be equal if they have the same value !");
    }

    /**
     * Test hashCode.
     */
    @Test
    void testHashCode() {
        assertNotNull(fhirSearchRequest.hashCode(), "Hash Code shall not be null.");
    }
}
