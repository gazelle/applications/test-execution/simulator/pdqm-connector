package net.ihe.gazelle.og.fhir.business;

import net.ihe.gazelle.lib.annotations.Covers;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Resource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link FhirResponse}
 */
class FhirResponseTest {
    /**
     * tested class
     */
    private FhirResponse fhirResponse;

    /**
     * Initiate a test {@link FhirResponse}
     */
    @BeforeEach
    public void init() {
        String uuid = "d306d3c4-bc67-11ea-b3de-0242ac130004";
        String standardKeyword = "FHIR";
        FhirVersion fhirVersion = FhirVersion.R4;

        fhirResponse = new FhirResponseConcrete(uuid, standardKeyword, fhirVersion);
    }

    /**
     * Test for the resource property getter and setter.
     */
    @Test
    @Covers(requirements = "FHIROG-7")
    public void resourceTest() {
        Resource expectedResource = new Bundle();

        fhirResponse.setResource(expectedResource);

        assertEquals(expectedResource, fhirResponse.getResource(),"The expectedResource must equal to " + expectedResource);
    }

    /**
     * Test identityEqual with two entity with same identity.
     */
    @Test
    void identityEqualTrue() {
        String uuid = "d306d3c4-bc67-11ea-b3de-0242ac130004";
        String standardKeyword = "FHIR";
        FhirVersion fhirVersion = FhirVersion.R4;
        FhirResponse fhirResponse1 = new FhirResponseConcrete(uuid, standardKeyword, fhirVersion);

        assertTrue(fhirResponse1.identityEqual(fhirResponse), "Two FhirResponse with same UUID shall be identityEqual.");
    }

    /**
     * Test identityEqual with same object
     */
    @Test
    void identityEqualSameObject() {
        assertTrue(fhirResponse.identityEqual(fhirResponse), "FhirResponse shall be identityEqual to himself.");
    }

    /**
     * Test identityEqual with instance of another class
     */
    @Test
    void identityEqualOtherClass() {
        assertFalse(fhirResponse.identityEqual(13), "FhirResponse shall not be identityEqual to an instance of another class.");
    }

    /**
     * Test identityEqual with two entity with same identity but different value.
     */
    @Test
    void identityEqualDifferentValue() {
        String uuid = "d306d3c4-bc67-11ea-b3de-0242ac130004";
        String standardKeyword = "FHIRR5";
        FhirVersion fhirVersion = FhirVersion.R4;
        FhirResponse fhirResponse1 = new FhirResponseConcrete(uuid, standardKeyword, fhirVersion);

        assertTrue(fhirResponse.identityEqual(fhirResponse1), "Two FhirResponse with same UUID shall be identityEqual.");
    }

    /**
     * Test identityEqual with two entity with different uuid.
     */
    @Test
    void identityEqualDifferentUUID() {
        String uuid = "uuid";
        String standardKeyword = "FHIR";
        FhirVersion fhirVersion = FhirVersion.R4;
        FhirResponse fhirResponse1 = new FhirResponseConcrete(uuid, standardKeyword, fhirVersion);

        assertFalse(fhirResponse.identityEqual(fhirResponse1), "Two TCPMessage with different UUID shall not be identityEqual.");
    }

    /**
     * Test object is equal to itself.
     */
    @Test
    void equalsSameObject() {
        assertTrue(fhirResponse.equals(fhirResponse), "Object shall be equal to itself.");
    }

    /**
     * Test object is not equal with an instance of another class
     */
    @Test
    void equalsOtherClass() {
        assertFalse(fhirResponse.equals(13), "Object shall not be equal to an instance of another class !");
    }

    /**
     * Test object is not equal with null
     */
    @Test
    void equalsNull() {
        assertFalse(fhirResponse.equals(null), "Object shall not be equal to null.");
    }

    /**
     * Test object is not equal if super is not equal.
     */
    @Test
    void equalsSuper() {
        String uuid = "d306d3c4-bc67-11ea-b3de-0242ac130004";
        String standardKeyword = "FHIRR5";
        FhirVersion fhirVersion = FhirVersion.R4;
        FhirResponse fhirResponse1 = new FhirResponseConcrete(uuid, standardKeyword, fhirVersion);

        assertFalse(fhirResponse1.equals(fhirResponse), "Object shall not be equal if super is not equal.");
    }

    /**
     * Test objects are equal if they have the same value
     */
    @Test
    void equalsSameValue() {
        String uuid = "d306d3c4-bc67-11ea-b3de-0242ac130004";
        String standardKeyword = "FHIR";
        FhirVersion fhirVersion = FhirVersion.R4;
        FhirResponse fhirResponse1 = new FhirResponseConcrete(uuid, standardKeyword, fhirVersion);

        assertTrue(fhirResponse.equals(fhirResponse1), "Object shall be equal if they have the same value !");
    }

    /**
     * Test object is not equal if they have different resources
     */
    @Test
    void equalsDifferentResources() {
        String uuid = "d306d3c4-bc67-11ea-b3de-0242ac130004";
        String standardKeyword = "FHIR";
        FhirVersion fhirVersion = FhirVersion.R4;
        FhirResponse fhirResponse1 = new FhirResponseConcrete(uuid, standardKeyword, fhirVersion);
        fhirResponse1.setResource(new Bundle());

        assertFalse(fhirResponse1.equals(fhirResponse), "Object shall not be equal if they have different resources.");
    }

    /**
     * Test hashCode.
     */
    @Test
    void testHashCode() {
        assertNotNull(fhirResponse.hashCode(), "Hash Code shall not be null.");
    }
}
