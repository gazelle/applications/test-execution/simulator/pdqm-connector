package net.ihe.gazelle.og.fhir.business;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FhirRequestTest {

    /**
     * tested class
     */
    private FhirRequest fhirRequest;
    private FhirRequest fhirRequest2;

    @BeforeEach
    public void init() {
        String uuid = "c4657530-bc64-11ea-b3de-0242ac130004";
        String standardKeyword = "FHIR";
        FhirVersion fhirVersion = FhirVersion.R4;
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.TEXT, true);
        String base = "base";
        String mimeType = "mimeType";
        String id = "1";
        String vid = "2";
        String compartment = "compartment";
        fhirRequest = new FhirRequestConcrete(uuid, standardKeyword, fhirVersion, generalParam, base,
                mimeType, id, vid, compartment, "type");
        fhirRequest2 = new FhirRequestConcrete(uuid, standardKeyword, fhirVersion, generalParam, base,
                mimeType, id, vid, compartment, "type");
    }

    @Test
    @Covers(requirements = "FHIROG-16")
    public void generalParamTest() {
        GeneralParam expectedGeneralParam = new GeneralParam(Format.JSON, Summary.DATA, true);
        fhirRequest.setGeneralParam(expectedGeneralParam);
        assertEquals(expectedGeneralParam, fhirRequest.getGeneralParam(),"The expectedGeneralParam must equal to " + expectedGeneralParam);
    }

    @Test
    public void baseTest() {
        String expectedBase = "expectedBase";
        fhirRequest.setBase(expectedBase);
        assertEquals(expectedBase, fhirRequest.getBase(),"The expectedBase must equal to " + expectedBase);
    }

    @Test
    @Covers(requirements = "FHIROG-10")
    public void baseNullTest() {
        assertThrows(IllegalArgumentException.class,() -> fhirRequest.setBase(null),"The expectedBase must not be null");
    }

    @Test
    @Covers(requirements = "FHIROG-11")
    public void mimeTypeTest() {
        String expectedMimeType = "expectedMimeType";
        fhirRequest.setMimeType(expectedMimeType);
        assertEquals(expectedMimeType, fhirRequest.getMimeType(),"The expectedMimeType must equal to " + expectedMimeType);
    }

    @Test
    @Covers(requirements = "FHIROG-12")
    public void idTest() {
        String expectedId = "expectedId";
        fhirRequest.setId(expectedId);
        assertEquals(expectedId, fhirRequest.getId(),"The expectedId must equal to " + expectedId);
    }

    @Test
    @Covers(requirements = "FHIROG-13")
    public void vidTest() {
        String expectedvId = "expectedvId";
        fhirRequest.setVid(expectedvId);
        assertEquals(expectedvId, fhirRequest.getVid(),"The expectedvId must equal to " + expectedvId);
    }

    @Test
    @Covers(requirements = "FHIROG-14")
    public void compartmentTest() {
        String expectedCompartment = "expectedCompartment";
        fhirRequest.setCompartment(expectedCompartment);
        assertEquals(expectedCompartment, fhirRequest.getCompartment(),"The expectedCompartment must equal to " + expectedCompartment);
    }

    @Test
    @Covers(requirements = "FHIROG-15")
    public void typeTest() {
        String expectedType = "expectedType";
        fhirRequest.setType(expectedType);
        assertEquals(expectedType, fhirRequest.getType(), "The expectedType must equal to " + expectedType);
    }

    @Test
    public void equalsDifferentVIdTest() {
        String expectedvId = "expectedvId";
        fhirRequest2.setVid(expectedvId);
        assertNotEquals(fhirRequest, fhirRequest2,"If field is different, objects should not be equal");
        assertNotEquals(fhirRequest.hashCode(), fhirRequest2.hashCode(), "hashcode shall be different too");
    }

    @Test
    public void equalsTest() {
        assertEquals(fhirRequest, fhirRequest2,"identical objects shall be equal");
        assertEquals(fhirRequest.hashCode(), fhirRequest2.hashCode(), "hashcode shall be equal too");
    }

    @Test
    public void equalsIdentityTest() {
        assertEquals(fhirRequest, fhirRequest,"object shall be equal to itself");
    }
}
