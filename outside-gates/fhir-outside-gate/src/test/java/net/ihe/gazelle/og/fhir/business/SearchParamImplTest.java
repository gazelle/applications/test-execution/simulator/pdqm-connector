package net.ihe.gazelle.og.fhir.business;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SearchParamImplTest {

    @Test
    @Covers(requirements = "FHIROG-25")
    public void valueTest() {
        String value = "value";
        SearchParamImpl searchParam = new SearchParamImpl(value);

        assertEquals(value, searchParam.getValue(), "value shall be setted correctly");
    }

    @Test
    public void valueNullTest() {
         assertThrows(IllegalArgumentException.class,() -> new SearchParamImpl(null), "value shall not be null");
    }

    @Test
    public void equalsSameTest() {
        String value = "value";
        SearchParamImpl searchParam = new SearchParamImpl(value);

        assertEquals(searchParam, searchParam, "parameter shall be equal to itself");
    }

    @Test
    public void equalsDifferentTest() {
        SearchParamImpl searchParam1 = new SearchParamImpl("value1");
        SearchParamImpl searchParam2 = new SearchParamImpl("value2");

        assertNotEquals(searchParam1, searchParam2, "parameter with different value shall not be equal");
        assertNotEquals(searchParam1.hashCode(), searchParam2.hashCode(), "hashcode also shall not be equal");
    }

    @Test
    public void equalsTest() {
        SearchParamImpl searchParam1 = new SearchParamImpl("value1");
        SearchParamImpl searchParam2 = new SearchParamImpl("value1");

        assertEquals(searchParam1, searchParam2, "parameter with same value shall be equal");
        assertEquals(searchParam1.hashCode(), searchParam2.hashCode(), "hashcode also shall be equal");
    }
}
