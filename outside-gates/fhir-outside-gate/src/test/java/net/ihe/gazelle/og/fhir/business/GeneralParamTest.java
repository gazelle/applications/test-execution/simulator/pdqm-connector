package net.ihe.gazelle.og.fhir.business;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link GeneralParam}
 */
class GeneralParamTest {

    /**
     * Test for format property getter and setter.
     */
    @Test
    @Covers(requirements = "FHIROG-18")
    void getFormat() {
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.DATA, true);

        generalParam.setFormat(Format.JSON);

        assertEquals(Format.JSON, generalParam.getFormat(), "Format shall be JSON !");
    }

    /**
     * Test for summary property getter and setter.
     */
    @Test
    @Covers(requirements = "FHIROG-20")
    void getSummary() {
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.DATA, true);

        generalParam.setSummary(Summary.TEXT);

        assertEquals(Summary.TEXT, generalParam.getSummary(), "Summary shall be TEST !");
    }

    /**
     * Test for pretty property getter and setter.
     */
    @Test
    @Covers(requirements = "FHIROG-19")
    void isPretty() {
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.DATA, true);

        generalParam.setPretty(false);

        assertFalse(generalParam.isPretty(), "GeneralParam shall be ugly !");
    }

    /**
     * Getter for elements
     */
    @Test
    @Covers(requirements = "FHIROG-21")
    void getElements() {
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.DATA, true);

        assertNotNull(generalParam.getElements(), "Element list shall never be null !");
    }

    /**
     * Test for addElement
     */
    @Test
    void addElement() {
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.DATA, true);
        String test = "test";

        generalParam.addElement(test);

        assertEquals(1, generalParam.getElements().size(), "Element shall be added to the list !");
        assertEquals(test, generalParam.getElements().get(0), "Element shall be the one added !");
    }

    /**
     * Test for removeElement
     */
    @Test
    void removeElement() {
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.DATA, true);
        String test = "test";

        generalParam.addElement(test);
        generalParam.removeElement(test);

        assertEquals(0, generalParam.getElements().size(), "Element shall be removed from the list !");
    }

    /**
     * Test object is equal to itself.
     */
    @Test
    void equalsSameObject() {
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.DATA, true);

        assertTrue(generalParam.equals(generalParam), "Object shall be equal to itself.");
    }

    /**
     * Test object is not equal with an instance of another class
     */
    @Test
    void equalsOtherClass() {
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.DATA, true);

        assertFalse(generalParam.equals(13), "Object shall not be equal to an instance of another class !");
    }

    /**
     * Test object is not equal with null
     */
    @Test
    void equalsNull() {
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.DATA, true);

        assertFalse(generalParam.equals(null), "Object shall not be equal to null.");
    }

    /**
     * Test objects are not equal if they have different formats
     */
    @Test
    void equalsDifferentFormat() {
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.DATA, true);
        GeneralParam generalParam1 = new GeneralParam(Format.JSON, Summary.DATA, true);

        assertFalse(generalParam.equals(generalParam1), "Object shall not be equal if they have different formats !");
    }

    /**
     * Test objects are not equal if they have different summary
     */
    @Test
    void equalsDifferentSummary() {
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.DATA, true);
        GeneralParam generalParam1 = new GeneralParam(Format.XML, Summary.TEXT, true);

        assertFalse(generalParam.equals(generalParam1), "Object shall not be equal if they have different summary !");
    }

    /**
     * Test objects are not equal if they have different pretty
     */
    @Test
    void equalsDifferentPretty() {
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.DATA, true);
        GeneralParam generalParam1 = new GeneralParam(Format.XML, Summary.DATA, false);

        assertFalse(generalParam.equals(generalParam1), "Object shall not be equal if they have different pretty !");
    }

    /**
     * Test objects are not equal if they have different elements
     */
    @Test
    void equalsDifferentElements() {
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.DATA, true);
        GeneralParam generalParam1 = new GeneralParam(Format.XML, Summary.TEXT, true);
        generalParam1.addElement("Test");

        assertFalse(generalParam.equals(generalParam1), "Object shall not be equal if they have different elements !");
    }

    /**
     * Test objects are equal if they have the same value
     */
    @Test
    void equalsSameValue() {
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.DATA, true);
        GeneralParam generalParam1 = new GeneralParam(Format.XML, Summary.DATA, true);

        assertTrue(generalParam.equals(generalParam1), "Object shall be equal if they have the same value !");
    }

    @Test
    void testHashCode() {
        GeneralParam generalParam = new GeneralParam(Format.XML, Summary.DATA, true);

        assertNotNull(generalParam.hashCode(), "Hash code shall not be null.");
    }
}