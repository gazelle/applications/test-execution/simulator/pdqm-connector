package net.ihe.gazelle.og.fhir.business;

public class FhirRequestConcrete extends FhirRequest {
    /**
     * Default constructor for the class taking as input all mandatory elements for the object to be created.
     *
     * @param uuid            value of the entity UUID.
     * @param standardKeyword Keyword of the used standard
     * @param fhirVersion     version of Fhir
     * @param generalParam
     * @param base
     * @param mimeType
     * @param id
     * @param vid
     * @param compartment
     * @throws IllegalArgumentException if any mandatory element is null.
     */
    public FhirRequestConcrete(String uuid, String standardKeyword, FhirVersion fhirVersion, GeneralParam generalParam, String base,
                               String mimeType, String id, String vid, String compartment, String type) {
        super(uuid, standardKeyword, fhirVersion, generalParam, base, mimeType, id, vid, compartment, type);
    }
}
