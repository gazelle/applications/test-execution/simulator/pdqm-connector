package net.ihe.gazelle.og.fhir.business;

public class FhirResponseConcrete extends FhirResponse {
    /**
     * Default constructor for the class taking as input all mandatory elements for the object to be created.
     *
     * @param uuid            value of the entity UUID.
     * @param standardKeyword Keyword of the used standard
     * @param fhirVersion     version of Fhir
     * @throws IllegalArgumentException if any mandatory element is null.
     */
    public FhirResponseConcrete(String uuid, String standardKeyword, FhirVersion fhirVersion) {
        super(uuid, standardKeyword, fhirVersion);
    }
}
