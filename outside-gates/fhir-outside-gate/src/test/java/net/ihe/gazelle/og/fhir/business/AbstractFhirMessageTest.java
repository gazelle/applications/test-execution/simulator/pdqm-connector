package net.ihe.gazelle.og.fhir.business;

import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.og.http.business.AbstractHTTPMessage;
import net.ihe.gazelle.og.http.business.HTTPMethod;
import net.ihe.gazelle.og.http.business.HTTPVersion;
import net.ihe.gazelle.og.tcp.business.TCPMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test for {@link AbstractFhirMessage}
 */
class AbstractFhirMessageTest {

    /**
     * tested concrete class
     */
    private AbstractFhirMessageConcrete abstractFhirMessageConcrete;

    /**
     * Initialize a test {@link AbstractFhirMessageConcrete}
     */
    @BeforeEach
    void init() {
        FhirVersion fhirVersion = FhirVersion.R4;
        String uuid = "2206a8f2-bc5c-11ea-b3de-0242ac130004";
        String standardKeyword = "FHIR";
        abstractFhirMessageConcrete = new AbstractFhirMessageConcrete(uuid, standardKeyword, fhirVersion);
    }

    /**
     * Test for fhirFormat property getter and setter.
     */
    @Test
    void fhirVersionTest() {
        FhirVersion expectedFhirVersion = FhirVersion.R4;

        abstractFhirMessageConcrete.setFhirVersion(expectedFhirVersion);

        assertEquals(expectedFhirVersion, abstractFhirMessageConcrete.getFhirVersion(), "The FhirVersion must equal to " + expectedFhirVersion);
    }

    /**
     * Test for fhirFormat property setter does not accept null.
     */
    @Test
    @Covers(requirements = "FHIROG-4")
    void fhirVersionSetNull() {
        assertThrows(IllegalArgumentException.class, () -> abstractFhirMessageConcrete.setFhirVersion(null));
    }

    /**
     * Test for httpMessage property getter and setter.
     */
    @Test
    @Covers(requirements = "FHIROG-3")
    void httpMessageTest() {

        TCPMessage rawTCPMessage = new TCPMessage("uuid", "TCP",new byte[]{1});
        AbstractHTTPMessage httpMessage = new AbstractHTTPMessage("uuid", "sk", "body".getBytes(StandardCharsets.UTF_8), rawTCPMessage, HTTPVersion.V2_0);

        abstractFhirMessageConcrete.setHttpMessage(httpMessage);

        assertEquals(httpMessage, abstractFhirMessageConcrete.getHttpMessage(), "The HTTPMessage must equal to the one set !");
    }

    /**
     * Test identityEqual with two entity with same identity.
     */
    @Test
    void identityEqualTrue() {
        FhirVersion fhirVersion = FhirVersion.R4;
        String uuid = "2206a8f2-bc5c-11ea-b3de-0242ac130004";
        String standardKeyword = "FHIR";
        AbstractFhirMessageConcrete abstractFhirMessageConcrete1 = new AbstractFhirMessageConcrete(uuid, standardKeyword, fhirVersion);

        assertTrue(abstractFhirMessageConcrete.identityEqual(abstractFhirMessageConcrete1), "Two AbstractFhirMessage with same UUID shall " +
                "be " +
                "identityEqual.");
    }

    /**
     * Test identityEqual with same object
     */
    @Test
    void identityEqualSameObject() {
        assertTrue(abstractFhirMessageConcrete.identityEqual(abstractFhirMessageConcrete), "AbstractFhirMessage shall be identityEqual to himself.");
    }

    /**
     * Test identityEqual with instance of another class
     */
    @Test
    void identityEqualOtherClass() {
        assertFalse(abstractFhirMessageConcrete.identityEqual(13), "AbstractFhirMessage shall not be identityEqual to an instance of another class.");
    }

    /**
     * Test identityEqual with two entity with same identity but different value.
     */
    @Test
    void identityEqualDifferentValue() {
        FhirVersion fhirVersion = FhirVersion.R5;
        String uuid = "2206a8f2-bc5c-11ea-b3de-0242ac130004";
        String standardKeyword = "FHIR";
        AbstractFhirMessageConcrete abstractFhirMessageConcrete1 = new AbstractFhirMessageConcrete(uuid, standardKeyword, fhirVersion);

        assertTrue(abstractFhirMessageConcrete.identityEqual(abstractFhirMessageConcrete1), "Two AbstractFhirMessage with same UUID shall be " +
                "identityEqual.");
    }

    /**
     * Test identityEqual with two entity with different UUID
     */
    @Test
    void identityEqualDifferentUUID() {
        FhirVersion fhirVersion = FhirVersion.R4;
        String uuid = "UUID";
        String standardKeyword = "FHIR";
        AbstractFhirMessageConcrete abstractFhirMessageConcrete1 = new AbstractFhirMessageConcrete(uuid, standardKeyword, fhirVersion);

        assertFalse(abstractFhirMessageConcrete.identityEqual(abstractFhirMessageConcrete1), "Two AbstractFhirMessage with different UUID shall" +
                " not be identityEqual.");
    }

    /**
     * Test object is equal to itself.
     */
    @Test
    void equalsSameObject() {
        assertTrue(abstractFhirMessageConcrete.equals(abstractFhirMessageConcrete), "Object shall be equal to itself.");
    }

    /**
     * Test object is not equal with an instance of another class
     */
    @Test
    void equalsOtherClass() {
        assertFalse(abstractFhirMessageConcrete.equals(13), "Object shall not be equal to an instance of another class !");
    }

    /**
     * Test object is not equal with null
     */
    @Test
    void equalsNull() {
        assertFalse(abstractFhirMessageConcrete.equals(null), "Object shall not be equal to null.");
    }

    /**
     * Test object is not equal if super is not equal.
     */
    @Test
    void equalsSuper() {
        FhirVersion fhirVersion = FhirVersion.R4;
        String uuid = "2206a8f2-bc5c-11ea-b3de-0242ac130004";
        String standardKeyword = "FHIRR5";
        AbstractFhirMessageConcrete abstractFhirMessageConcrete1 = new AbstractFhirMessageConcrete(uuid, standardKeyword, fhirVersion);

        assertFalse(abstractFhirMessageConcrete.equals(abstractFhirMessageConcrete1), "Object shall not be equal if super is not equal.");
    }

    /**
     * Test objects are equal if they have the same value
     */
    @Test
    void equalsSameValue() {
        FhirVersion fhirVersion = FhirVersion.R4;
        String uuid = "UUID";
        String standardKeyword = "FHIR";
        AbstractFhirMessageConcrete abstractFhirMessageConcrete1 = new AbstractFhirMessageConcrete(uuid, standardKeyword, fhirVersion);

        assertTrue(abstractFhirMessageConcrete.equals(abstractFhirMessageConcrete1), "Object shall be equal if they have the same value !");
    }

    /**
     * Test hashCode.
     */
    @Test
    void testHashCode() {
        assertNotNull(abstractFhirMessageConcrete.hashCode(), "Hash Code shall not be null.");
    }
}
