package net.ihe.gazelle.og.fhir.business;

/**
 * accepted formats for a FHIR transaction
 */
public enum Format {
    XML,
    JSON
}
